/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: process.h
 * Description: Header file for process, including some hardware settings
 */ 


#ifndef PROCESS_H_
#define PROCESS_H_

// Timer1 (MEASURE and CHIRP) Prescale
#define TIMER1_PRESCALE()            TCCR1B  = (1<<CS10)

// Timer4 (TIMER and RF) Prescale
#define TIMER4_PRESCALE()            TCCR4B  = (1<<CS41)

// Run V91
#define TIMER_NEXT()                 OCR4B  +=  (F_CPU/(8UL * TICKS_PER_SECOND))
#define TIMER_MASK_ON()              TIMSK4 |=  (1<<OCIE4B)
#define TIMER_MASK_OFF()             TIMSK4 &= ~(1<<OCIE4B)
#define TIMER_ISR                    ISR(TIMER4_COMPB_vect)

// RTC
#define RTC_POWER_OFF()
#define RTC_POWER_ON()
#define RTC_INT_MSK_SET()            PCMSK1 |=  (_BV(PCINT15))
#define RTC_INT                      _BV(PJ6)

// keyboard
#define KEY1_INT_MSK_SET()           PCMSK1 |=  (_BV(PCINT14))
#define KEY2_INT_MSK_SET()           PCMSK1 |=  (_BV(PCINT13))
#define KEY3_INT_MSK_SET()           PCMSK1 |=  (_BV(PCINT12))
#define KEY4_INT_MSK_SET()           PCMSK1 |=  (_BV(PCINT11))

#define KEYS_INT_MSK_SET()           PCMSK1 |=  (0x3C)
#define KEYS_INT_MSK_CLR()           PCMSK1 &= ~(0x3C)

#define PCINT0_INT_CLR()             PCIFR  |=  (_BV(PCIF0))
#define PCINT0_INT_OFF()             PCICR  &= ~(_BV(PCIE0))
#define PCINT0_INT_ON()              PCICR  |=  (_BV(PCIE0))

#define PCINT1_INT_CLR()             PCIFR  |=  (_BV(PCIF1))
#define PCINT1_INT_OFF()             PCICR  &= ~(_BV(PCIE1))
#define PCINT1_INT_ON()              PCICR  |=  (_BV(PCIE1))

#define PCINT2_INT_CLR()             PCIFR  |=  (_BV(PCIF2))
#define PCINT2_INT_OFF()             PCICR  &= ~(_BV(PCIE2))
#define PCINT2_INT_ON()              PCICR  |=  (_BV(PCIE2))

#define INPS_INT_MSK_CLR()           PCMSK2 &= ~(0x0F)
#define INPS_INT_MSK_REG()           PCMSK2
#define INPS()                       PINK   &   (0x0F)

// Prototype
void ProcessInit(void);

void ProcessRun(void);

void ProcessWalk(void);

void ProcessSecondTick(void);

void SetTickEnable(uint8_t enable);

void set_run_enable(uint8_t enable);

#endif /* PROCESS_H_ */