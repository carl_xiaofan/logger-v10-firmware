/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: main.c
 * Description: Main loop for the logger
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include "Core/Setting/setting.h"
#include "core/Serial/radio.h"
#include "core/ports.h"
#include "main.h"
#include "process.h"

static uint16_t firmwareVersion = 121;
static uint16_t buildYear = 2020;
static uint8_t buildMonth = 7;
static uint8_t buildDay = 7;


int main(void) {
	cli();
	InitPorts();
	ProcessInit();
	sei();
	
	SettingSetRTM(0);
	SettingsSetFWversion(firmwareVersion);
	DateTime_t t;
	t.Year = buildYear;
	t.Month = buildMonth;
	t.Date = buildDay;
	t.Hour = 0;
	t.Minute = 0;
	t.Second = 0;
	SettingsSetBuildDate(t);
	
	
	while (TRUE) {
		ProcessWalk();
	}
	
	return 0;
}

