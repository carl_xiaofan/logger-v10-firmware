/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: sram.h
 * Description: Header file for sram.c, assign ram address for each buffer
 */ 


#ifndef SCRAM_H_
#define SCRAM_H_

#include "../../main.h"

// Firmware
#define SRAM_CS_LO()                 PORTL  &= ~(_BV(PL6))
#define SRAM_CS_HI()                 PORTL  |=  (_BV(PL6))
#define SRAM_SCK_LO()                PORTL  &= ~(_BV(PL5))
#define SRAM_SCK_HI()                PORTL  |=  (_BV(PL5))
#define SRAM_MOSI_LO()               PORTL  &= ~(_BV(PL4))
#define SRAM_MOSI_HI()               PORTL  |=  (_BV(PL4))
#define SRAM_MISO()                  PINL   &   (_BV(PL7))

// Static RAM address allocator
#define LOGGING_BUFFER               0     // 0    - 513
#define RECORD_BUFFER				 520   // 520  - 560
#define GENERAL_BUFFER               600   // 600  - 1113
#define LCD_TEXT_BUFFER              1200  // 1200 - 1519
#define HOLDING_REGISTER_BASE        1600  // 1600 - xxxx

#define RAM_WRITE_STATUS			 0x01
#define RAM_WRITE_COMMAND			 0x02
#define RAM_READ_COMMAND			 0x03
#define RAM_SEQUENTIAL_MODE			 0x40
#define RAM_READ_STATUS			     0x05
// prototypes

void SramInit(void);

void SramSequentialEnd(void);

uint8_t SramReadStatus(void);

void SramWriteStatus(uint8_t data);

uint8_t SramSequentialReadByte(void);

uint16_t SramSequentialRead16bits(void);

uint32_t SramSequentialRead32bits(void);

void SramSequentialReadStart(unsigned int address);

void SramSequentialWriteStart(unsigned int address);

void SramSequentialWriteByte(uint8_t data);


#endif /* SCRAM_H_ */