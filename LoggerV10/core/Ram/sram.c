/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: sram.c
 * Description: 23K256 Microchip serial SRAM, Handle for read and write into SRAM
 */ 

#include <avr/io.h>
#include "../../common/util.h"
#include "sram.h"
 
// prototypes
static void SendAddress(unsigned int address);
static void SendByte(uint8_t data);
static uint8_t ReadByte(void);

/*******************************************************************************
* Function Name		: SramInit
* Description		: Init Scram to sequential mode
* Input				: void
* Return			: void
*******************************************************************************/
void SramInit(void) {
	SramWriteStatus(RAM_SEQUENTIAL_MODE);
}

/*******************************************************************************
* Function Name		: SramSequentialReadStart
* Description		: start read at specific address
* Input				: address to be start to read
* Return			: void
*******************************************************************************/
void SramSequentialReadStart(unsigned int address) {

	SRAM_SCK_LO();
	SRAM_CS_LO();

	SendByte(RAM_READ_COMMAND);
	// send address
	SendAddress(address);
}

/*******************************************************************************
* Function Name		: SramSequentialReadByte
* Description		: read byte after sequential read start
* Input				: void
* Return			: byte read
*******************************************************************************/
uint8_t SramSequentialReadByte(void) {
	return ReadByte();
}

uint16_t SramSequentialRead16bits(void) {
	uint8_t t1 = ReadByte();
	uint8_t t2 = ReadByte();
	
	return U8ToU16(t1, t2);
}

uint32_t SramSequentialRead32bits(void) {
	uint8_t t1 = ReadByte();
	uint8_t t2 = ReadByte();
	uint8_t t3 = ReadByte();
	uint8_t t4 = ReadByte();
	
	return U8ToU32(t1, t2, t3, t4);
}

/*******************************************************************************
* Function Name		: SramSequentialWriteStart
* Description		: start write at specific address
* Input				: address to be start to write
* Return			: void
*******************************************************************************/
void SramSequentialWriteStart(unsigned int address) {	 
	SRAM_SCK_LO();
	SRAM_CS_LO();

	SendByte(RAM_WRITE_COMMAND);
	// send address
	SendAddress(address);
}

/*******************************************************************************
* Function Name		: SramSequentialWriteByte
* Description		: write byte after sequential write start
* Input				: void
* Return			: void
*******************************************************************************/
void SramSequentialWriteByte(uint8_t data) {
	// send data byte
	SendByte(data);
}

/*******************************************************************************
* Function Name		: SramSequentialEnd
* Description		: end sequential write/read
* Input				: void
* Return			: void
*******************************************************************************/
void SramSequentialEnd(void) {
	SRAM_CS_HI();
}

/*******************************************************************************
* Function Name		: SramReadStatus
* Description		: read the state of sram
* Input				: void
* Return			: status
*******************************************************************************/
uint8_t SramReadStatus(void) {
	uint8_t data;

	SRAM_SCK_LO();
	SRAM_CS_LO();

	SendByte(RAM_READ_STATUS);
	// read byte
	data = ReadByte();

	SRAM_CS_HI();
	return data;
}

/*******************************************************************************
* Function Name		: SramWriteStatus
* Description		: write the state of sram
* Input				: data status to be write
* Return			: void
*******************************************************************************/
void SramWriteStatus(uint8_t data) {
	SRAM_SCK_LO();
	SRAM_CS_LO();

	SendByte(RAM_WRITE_STATUS);
	// send data byte
	SendByte(data);

	SRAM_CS_HI();
}
 
 /*******************************************************************************
 * Function Name	: SendAddress
 * Description		: send address to sram
 * Input			: void
 * Return			: void
 *******************************************************************************/
static void SendAddress(unsigned int address)
{
	uint8_t i = 16;

	while (i--) {
		if (address & 0x8000) {
			SRAM_MOSI_HI(); 
		} else {
			SRAM_MOSI_LO();
		}
		
		address <<= 1;
		
		SRAM_SCK_HI();
		SRAM_SCK_LO();
	}
}

 /*******************************************************************************
 * Function Name	: SendByte
 * Description		: send byte to sram
 * Input			: data byte to be send
 * Return			: void
 *******************************************************************************/
static void SendByte(uint8_t data) {
	uint8_t i = 8;

	while (i--) {
		if (data & 0x80) {
			SRAM_MOSI_HI(); 
		} else {
			SRAM_MOSI_LO();
		}
		
		data <<= 1;
		
		SRAM_SCK_HI();
		SRAM_SCK_LO();
	}
}

 /*******************************************************************************
 * Function Name	: ReadByte
 * Description		: read byte from sram
 * Input			: void
 * Return			: data byte to be read
 *******************************************************************************/
static uint8_t ReadByte(void) {
	uint8_t data = 0, i = 8;

	SRAM_MOSI_LO();
	while (i--) {
		data <<= 1;
		if (SRAM_MISO()) {
			data |= 1;
		}
		
		SRAM_SCK_HI();
		SRAM_SCK_LO();
	}
	return data;
}