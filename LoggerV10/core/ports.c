/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: port.c
 * Description: Initial the ports output/input
 */ 

#include <avr/io.h>
#include "ports.h"

/*******************************************************************************
* Function Name		: init_ports
* Description		: Initialize Ports for Atmega2560
* Input				: void
* Return			: void
*******************************************************************************/
void InitPorts()
{
	/*
	Pin 78  PA0    NC            INPUT  1
	Pin 77  PA1    AUX-PWR-E     OUTPUT 0
	Pin 76  PA2    ANA-PWR-E     OUTPUT 0
	Pin 75  PA3    LCD-BL-E      OUTPUT 0
	Pin 74  PA4    RF-CTS        INPUT  0
	Pin 73  PA5    RF-SLEEP-3V3  INPUT  0
	Pin 72  PA6    RF-PWR-E      OUTPUT 0
	Pin 71  PA7    RF-CTRL1      OUTPUT 0
	*/
	DDRA  = 0xCE;
	PORTA = 0x01;
	/*
	Pin 19  PB0    TEST1         OUTPUT 0 SS never use as input
	Pin 20  PB1    SCK           OUTPUT 0 PCINT1
	Pin 21  PB2    MOSI          OUTPUT 0 PCINT2
	Pin 22  PB3    MISO          INPUT  1 PCINT3
	Pin 23  PB4    FLASH-CS      OUTPUT 1 PCINT4
	Pin 24  PB5    NC            INPUT  1 PCINT5
	Pin 25  PB6    RS232-RX      INPUT  0 PCINT6
	Pin 26  PB7    USB-RX        INPUT  0 PCINT7
	*/
	DDRB  = 0x17;
	PORTB = 0x38;
	/*
	Pin 53  PC0    NC            INPUT  1 
	Pin 54  PC1    RF-RESET      OUTPUT 0 
	Pin 55  PC2    RF-SLEEP-CON  OUTPUT 0 
	Pin 56  PC3    RF-RTS        OUTPUT 0 
	Pin 57  PC4    LCD-D4        OUTPUT 0 
	Pin 58  PC5    LCD-D5        OUTPUT 0 
	Pin 59  PC6    LCD-D6        OUTPUT 0 
	Pin 60  PC7    LCD-D7        OUTPUT 0 
	*/
	DDRC  = 0xFE;
	PORTC = 0x01;
	/*
	Pin 43  PD0  SCL             INPUT  0 
	Pin 44  PD1  SDA             INPUT  0 
	Pin 45  PD2  USB-RX          INPUT  0
	Pin 46  PD3  USB-TX          OUTPUT 1
	Pin 47  PD4  CAPTURE         INPUT  0 
	Pin 48  PD5  ADC-SCK         OUTPUT 0 
	Pin 49  PD6  ADC-MISO        INPUT  1 
	Pin 50  PD7  NC              INPUT  1
	*/
	DDRD  = 0x28;
	PORTD = 0xC8;
	/*
	Pin 2   PE0  RS485-RX        INPUT  0 PCINT8
	Pin 3   PE1  RS485-TX        OUTPUT 1 
	Pin 4   PE2  MUX-B           OUTPUT 0 
	Pin 5   PE3  MUX-C           OUTPUT 0 
	Pin 6   PE4  IOPS2           OUTPUT 1 
	Pin 7   PE5  IOPE2           OUTPUT 1 
	Pin 8   PE6  IOPS1           OUTPUT 1 
	Pin 9   PE7  IOPE1           OUTPUT 1 
	*/
	DDRE  = 0xFE;
	PORTE = 0xF2;
	DIDR1 = 0xFE; // COULD BE 00 DOES IT MAKE ANY DIFFERENCE ??
	/*
	Pin 97  PF0  BATT-LEVEL      INPUT  0 
	Pin 96  PF1  MUX-A           OUTPUT 0 
	Pin 95  PF2  MUX-INH         OUTPUT 0
	Pin 94  PF3  TEST2           OUTPUT 0 
	Pin 93  PF4  NC              INPUT  1
	Pin 92  PF5  NC              INPUT  1
	Pin 91  PF6  NC              INPUT  1
	Pin 90  PF7  NC              INPUT  1
	*/
	DDRF  = 0x0E;
	PORTF = 0xF0;
	DIDR0 = 0x01; // Disable digital input on analog line
	/*
	Pin 51  PG0  NC              INPUT  1 
	Pin 52  PG1  NC              INPUT  1 
	Pin 70  PG2  RF-CNTRL0       OUTPUT 0 
	Pin 28  PG3  RS485-RE        OUTPUT 0 
	Pin 29  PG4  NC              INPUT  1 
	Pin 01  PG5  RESISTOR-E      OUTPUT 0
			PG6  NC              INPUT  1
			PG7  NC              INPUT  1
	*/
	DDRG  = 0x2C;
	PORTG = 0xD3;
	/*
	Pin 12  PH0  RS232-RX        INPUT  0 
	Pin 13  PH1  RS232-TX        OUTPUT 1 
	Pin 14  PH2  VWLOAD-E        OUTPUT 0 
	Pin 15  PH3  ADC-CS          OUTPUT 1 
	Pin 16  PH4  LCD-E           OUTPUT 0 
	Pin 17  PH5  LCD-PWR-E       OUTPUT 0
	Pin 18  PH6  LCD-RS          OUTPUT 0
	Pin 27  PH7  RS485-DE        OUTPUT 0 
	*/
	DDRH  = 0xFE;
	PORTH = 0x0A;
	/*
	Pin 63  PJ0  RF-RX-3V3       INPUT  0 PCINT9
	Pin 64  PJ1  RF-TX           OUTPUT 1 PCINT10
	Pin 65  PJ2  SW4             INPUT  1 PCINT11
	Pin 66  PJ3  SW3             INPUT  1 PCINT12
	Pin 67  PJ4  SW2             INPUT  1 PCINT13
	Pin 68  PJ5  SW1             INPUT  1 PCINT14
	Pin 69  PJ6  RTC-INT         INPUT  0 PCINT15
	Pin 79  PJ7  NC              INPUT  1 
	*/
	DDRJ  = 0x02;
	PORTJ = 0xBE;
	/*
	Pin 63  PK0  INP8            INPUT  0 PCINT16
	Pin 64  PK1  INP7            INPUT  0 PCINT17
	Pin 65  PK2  INP6            INPUT  0 PCINT18
	Pin 66  PK3  INP5            INPUT  0 PCINT19
	Pin 67  PK4  IOPE3           OUTPUT 1 PCINT20
	Pin 68  PK5  IOPS3           OUTPUT 1 PCINT21
	Pin 69  PK6  IOPE4           OUTPUT 1 PCINT22
	Pin 79  PK7  IOPS4           OUTPUT 1 PCINT23
	*/
	DDRK  = 0xF0;
	PORTK = 0xF0;
	DIDR2 = 0x0F; // Disable on logger inputs (enable if required for switch use)
	/*
	Pin 63  PL0  NC              INPUT  1
	Pin 64  PL1  NC              INPUT  1
	Pin 65  PL2  NC              INPUT  1 
	Pin 66  PL3  RS232-EN        OUTPUT 0 // Disabled initially
	Pin 67  PL4  RAM-MOSI        OUTPUT 0 
	Pin 68  PL5  RAM-SCK         OUTPUT 0
	Pin 69  PL6  RAM-CS          OUTPUT 1
	Pin 79  PL7  RAM-MISO-3V3    INPUT  0
	*/
	DDRL  = 0x78;
	PORTL = 0x47;
}