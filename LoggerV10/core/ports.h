/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: port.h
 * Description: Header file for port.c
 */ 

#ifndef PORTS_H_
#define PORTS_H_

void InitPorts(void);

#endif /* PORTS_H_ */