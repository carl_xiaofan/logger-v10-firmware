/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: key.h
 * Description: Header file for key.c
 */ 

#ifndef KEY_H_
#define KEY_H_

#define KEYS()                   (~PINJ &   (0x3C)) >> 2

 // variables
 extern uint8_t pressed;
 extern uint8_t released;
 extern uint8_t key_store;
 extern uint8_t keys_pressed;

 // prototypes
 void KeyInit(void);
 
 void KeyRun(void);

 unsigned char IsKeySet(unsigned char *flag);

#endif /* KEY_H_ */