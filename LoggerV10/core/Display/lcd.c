/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: lcd.c
 * Description: The version LCD is NHD-0420H1Z-FSW-GBW, data sheet is stored in Logger/V10
 * Use SRAM to store buffer of LCD Text
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include "../../common/timer.h"
#include "../../common/state.h"
#include "../Ram/sram.h"
#include "lcd.h" 

// Prototype
static void goto_position(uint8_t x, uint8_t y);
static void put_char(char data);
static void command(uint8_t data);
static void half_command(uint8_t data);
static void enable(void);

// Variables
static const uint8_t line[] = {0x80,0xC0,0x94,0xD4};
static volatile uint8_t lcd_level;
static volatile uint8_t next_state;
static volatile uint8_t lcd_state;
static volatile uint8_t text_changed;
static volatile uint8_t settle_time;

/*******************************************************************************
* Function Name		: lcd_init
* Description		: Initialize the port, but do not turn on the screen, 
*					  also put space char in ram
* Input				: void
* Return			: void
*******************************************************************************/
void lcd_init(void)
{
	LCD_DATA_PORT() &= ~(LCD_DATA_MASK());
	LCD_OFF();
	lcd_state = OFF;
	next_state = SETUP;
	text_changed = FALSE;
	settle_time = Milliseconds(80);
	
	SramSequentialWriteStart(LCD_TEXT_BUFFER);
	
	for (unsigned int i = 0; i < COLS * ROWS * PAGES; i++)
	{
		SramSequentialWriteByte(WHITE_SPACE); // fill pages with spaces
	}

	SramSequentialEnd();
}

/*******************************************************************************
* Function Name		: lcd_walk
* Description		: LCD FSM walk
* Input				: void
* Return			: void
*******************************************************************************/
void lcd_walk(void)
{
	switch (lcd_state)
	{
		case WAIT:
			lcd_write();
			break;
		case SETUP : 
			lcd_start(); 
			break;
	}
}

/*******************************************************************************
* Function Name		: lcd_run
* Description		: Count down time
* Input				: void
* Return			: void
*******************************************************************************/
void lcd_run(void)
{
	if (settle_time) 
	{
		if (!(--settle_time)) 
		{
			lcd_state = next_state;
		}
	}
}

/*******************************************************************************
* Function Name		: lcd_start
* Description		: Turn on the screen and back light, also initialize the LCD
* Input				: void
* Return			: void
*******************************************************************************/
void lcd_start(void)
{
	LCD_ON();
	LCD_BL_ON();
	
	_delay_ms(50);	// Delay for 50ms when trying to initialize LCD
	
	RS_LO();
	half_command(0x20);		// Command for LCD, see detail at datasheet
	half_command(0x20);
	half_command(0xC0);
	_delay_ms(0.200);
	half_command(0x00);
	half_command(0xC0);
	_delay_ms(0.200);
	half_command(0x00);
	half_command(0x10);
	_delay_ms(3.500);
	half_command(0x00);
	half_command(0x60);
	
	SramSequentialReadStart(LCD_TEXT_BUFFER);

	for (uint8_t k = 0; k < ROWS; k++)
	{
		goto_position(0,k);
		for (uint8_t i = 0; i < COLS; i++) 
		{
			put_char(SramSequentialReadByte());
		}
	}

	SramSequentialEnd();
	
	lcd_state = ON;
}

/*******************************************************************************
* Function Name		: lcd_text_set
* Description		: Set the text in SRAM by level and x y position
* Input				: level which level be written on
*					: x coordinate
*					: y coordinate
*					: *msg message being written
* Return			: void
*******************************************************************************/
void lcd_text_set(uint8_t level, uint8_t x, uint8_t y, char *msg)
{
	uint8_t ch;
	  
	SramSequentialWriteStart(LCD_TEXT_BUFFER + level * PAGE_SIZE + y * COL_SIZE + x);
	
	while ((ch = *msg++)) {
		SramSequentialWriteByte(ch);
	}
	
	SramSequentialEnd();
}

/*******************************************************************************
* Function Name		: lcd_text_clr
* Description		: Clear the text in SRAM
* Input				: level which level be clear
*					: x coordinate
*					: y coordinate
*					: len how many bytes being clear
* Return			: void
*******************************************************************************/
void lcd_text_clr(uint8_t level, uint8_t x, uint8_t y, uint8_t len)
{
	SramSequentialWriteStart(LCD_TEXT_BUFFER + level * PAGE_SIZE + y * COL_SIZE + x);
	
	for (uint8_t i = 0; i < len; ++i)
	{
		SramSequentialWriteByte(WHITE_SPACE);
	}
	
	SramSequentialEnd();
}

/*******************************************************************************
* Function Name		: lcd_text_show
* Description		: Set the state to wait and set which level will be showing
* Input				: level which level be shown
* Return			: void
*******************************************************************************/
void lcd_text_show(uint8_t level) {
	lcd_level = level;
	lcd_state = WAIT;
}

/*******************************************************************************
* Function Name		: lcd_write
* Description		: Read from SRAM and print it on the LCD
* Input				: void
* Return			: void
*******************************************************************************/
void lcd_write(void)
{
	uint8_t i, k;

	text_changed = FALSE; // clear the flag
	
	SramSequentialReadStart(LCD_TEXT_BUFFER + lcd_level * PAGE_SIZE);

	for (k = 0; k < ROWS; k++)
	{
		goto_position(0,k);
		for (i = 0; i < COLS; i++) 
		{
			put_char(SramSequentialReadByte());
		}
	}

	SramSequentialEnd();
	
	lcd_state = ON;
}

/*******************************************************************************
* Function Name		: goto_position
* Description		: Move the cursor to certain position
* Input				: x coordinate
*					: y coordinate
* Return			: void
*******************************************************************************/
static void goto_position(uint8_t x, uint8_t y)
{
	if ((x < COLS) && (y < ROWS))
	{
		command(line[y] + x);
	} 
}

/*******************************************************************************
* Function Name		: put_char
* Description		: Put a char to cursor
* Input				: data char being put
* Return			: void
*******************************************************************************/
static void put_char(char data)
{
	RS_HI();
	command(data);
	RS_LO();
}

/*******************************************************************************
* Function Name		: half_command
* Description		: Set the low four bit of data port
* Input				: data command
* Return			: void
*******************************************************************************/
static void half_command(uint8_t data)
{
	LCD_DATA_PORT() &= ~(LCD_DATA_MASK());        // clear old data
	LCD_DATA_PORT() |= (data & LCD_DATA_MASK()); // set new data
	enable();
}

/*******************************************************************************
* Function Name		: command
* Description		: Set all 8 bit of data port
* Input				: data command
* Return			: void
*******************************************************************************/
static void command(uint8_t data)
{
	LCD_DATA_PORT() &= ~(LCD_DATA_MASK());        // clear old data
	LCD_DATA_PORT() |= (data & LCD_DATA_MASK()); // set new data // high nibble
	enable();
	
	LCD_DATA_PORT() &= ~(LCD_DATA_MASK());        // clear old data
	LCD_DATA_PORT() |= ((data << 4) & LCD_DATA_MASK()); // set new data // low nibble
	enable();
}

/*******************************************************************************
* Function Name		: enable
* Description		: Enable data transfer
* Input				: void
* Return			: void
*******************************************************************************/
static void enable(void)
{
	E_HI();
	_delay_us(2);
	E_LO();
	_delay_ms(0.2);
}