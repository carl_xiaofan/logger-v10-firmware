/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: lcd.h
 * Description: Header file for lcd.c
 */ 


#ifndef LCD_H_
#define LCD_H_

// Firmware
#define LCD_ON()                     PORTH  |=  (_BV(PH5))
#define LCD_OFF()                    PORTH  &= ~(_BV(PH5))
#define LCD_BL_ON()                  PORTA  |=  (_BV(PA3))
#define LCD_BL_OFF()                 PORTA  &= ~(_BV(PA3))
#define E_HI()                       PORTH  |=  (_BV(PH4))	// Enable Pin
#define E_LO()                       PORTH  &= ~(_BV(PH4))
#define RS_HI()                      PORTH  |=  (_BV(PH6))	// RS Pin, Command for 0, data for 1
#define RS_LO()                      PORTH  &= ~(_BV(PH6))
#define LCD_DATA_PORT()              PORTC
#define LCD_DATA_MASK()				 0xF0

// Variables
#define ROWS                         4    // number of lines on the LCD
#define COLS                         20   // number of characters per line
#define PAGES                        4
#define PAGE_SIZE					 80
#define COL_SIZE					 20

#define WHITE_SPACE					 0x20

// Pages
#define INFO_PAGE					 0
#define MENU_PAGE					 1
#define READ_PAGE					 2
#define SETTING_PAGE			     3
#define BLANK_PAGE                   4

// Prototypes
void lcd_init(void);

void lcd_start(void);

void lcd_walk(void);

void lcd_run(void);

void lcd_write(void);

void lcd_text_set(uint8_t level, uint8_t x, uint8_t y, char *msg);

void lcd_text_clr(uint8_t level, uint8_t x, uint8_t y, uint8_t len);

void lcd_text_show(uint8_t level);

#endif /* LCD_H_ */