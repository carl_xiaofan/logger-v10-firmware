/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: menu.h
 * Description: Header file for menu.c
 */ 

#ifndef MENU_H_
#define MENU_H_

// Variable
#define KEY_UP			1
#define KEY_RIGHT		2
#define KEY_DOWN		3
#define KEY_LEFT		4

//Prototype
void MenuInit(void);

void MenuWalk(void);

void UploadRead(void);

void MenuSecondTick(void);

void MenuBusyUpload(uint8_t set);

#endif /* MENU_H_ */