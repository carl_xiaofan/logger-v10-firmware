/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: key.c
 * Description: Handle for button clicked
 */ 

#include <avr/io.h>
#include "key.h"
#include "../../main.h"

// variables
uint8_t keys_pressed;       // keys currently pressed - valid throughout run
uint8_t prev_keys_pressed;	// keys pressed last time
uint8_t key_store;			// last key pressed (valid even after all keys released)
uint8_t pressed;			// true if a key was pressed, cleared on access
uint8_t released;			// true if a key was released, cleared on access

/*******************************************************************************
* Function Name		: KeyInit
* Description		: Initialize key variable
* Input				: void
* Return			: void
*******************************************************************************/
void KeyInit(void) {
	prev_keys_pressed = 0;
	keys_pressed = 0;
	key_store = 0;
}

/*******************************************************************************
* Function Name		: KeyRun
* Description		: Detect button press event
* Input				: void
* Return			: void
*******************************************************************************/
void KeyRun(void) {
	keys_pressed = KEYS();    // read keyboard
	 
	// set or clear key_active flag
	// see if keys currently pressed are different to those previously pressed
	if (keys_pressed != prev_keys_pressed) {
		if (keys_pressed > prev_keys_pressed) {
			key_store = keys_pressed;	// store last key press
			pressed   = TRUE;
			released  = FALSE;
		} else {
			released  = TRUE;
			pressed   = FALSE;
		}
		prev_keys_pressed = keys_pressed;
	}
}

/*******************************************************************************
* Function Name		: IsKeySet
* Description		: clears flag, returns TRUE if flag WAS set (it is now clear)
* Input				: void
* Return			: true/false
*******************************************************************************/
uint8_t IsKeySet(uint8_t *flag)
{
	uint8_t ok;

	if (*flag) {
		ok = TRUE; 
	} else {
		ok = FALSE;
	}
	
	*flag = FALSE;
	
	return ok;
}