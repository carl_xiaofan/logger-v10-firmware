/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: menu.c
 * Description: Control the menu display, including navigation
 */ 

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>

#include "../../common/state.h"
#include "../../common/util.h"
#include "../RTC/DS3232.h"
#include "../Setting/setting.h"
#include "../Log/log.h"
#include "../Power/battery.h"
#include "../Read/read.h"
#include "../serial/serial_packet.h"
#include "../serial/uart.h"
#include "../../main.h"
#include "key.h"
#include "lcd.h"
#include "menu.h"

static uint8_t page_status;
static uint8_t lcd_showing;	// Which page is showing
static uint8_t pressed_key;	// Which key has been pressed

// Menu page
static uint8_t menu_selected;	// Which option is selected in menu page
static char* menu_options[] = {"Channel 1", "Channel 2", "Channel 3", 
	"Channel 4", "Channel 5", "Channel 6", "Channel 7", 
	"Channel 8", "SETTINGS"};
static const uint8_t menu_count = 9;

// Menu page
static uint8_t setting_selected;	// Which option is selected in menu page
static char* setting_options[] = {"Mode", "Role", "Slave Number", "Slave Mode"};
static const uint8_t setting_count = 3;

// Prototype
static void InfoPage(void);
static void MenuPage(void);
static void ReadPage(void);
static void SetPage(void);
static void ChangeSet(void);

static void RunChan(void);
static void RunDisp(void);
static void RunUpdate(void);

static void SetChan(void);
static uint8_t ChannelFromKey(uint8_t code);
  
void MenuInit(void) {
	page_status     = INIT;
	lcd_showing     = BLANK_PAGE;
	menu_selected	= 0;
	setting_selected = 0;
}

/*******************************************************************************
* Function Name		: MenuWalk
* Description		: Menu walk for the process
*					  INIT: initialize the screen with info page
*					  DISP: nothing happened, IDLE state
*					  CHAN: after button event detected
*					  UPDATE: update the reading if in READ_PAGE
* Input				: void
* Return			: void
*******************************************************************************/
void MenuWalk(void) {
	switch (page_status) {
		case INIT     : InfoPage();   break;
		case DISP     : RunDisp();    break;
		case CHAN     : RunChan();    break;
		case UPDATE	  : RunUpdate();  break;
	}
}

/*******************************************************************************
* Function Name		: MenuSecondTick
* Description		: Handle update every one second, only for INFO PAGE
* Input				: void
* Return			: void
*******************************************************************************/
void MenuSecondTick(void) {
	if (lcd_showing == INFO_PAGE) {
		if (has_set()) {
			InfoPage();
		}
		
		char buf_string[6];
		DateTime_t now = RTC_Get();
		char* date = Get_Date(now);
		char* time = Get_Time(now);
		lcd_text_set(INFO_PAGE, 0, 3, date);
		lcd_text_set(INFO_PAGE, 11, 3, time);
		lcd_text_set(INFO_PAGE, 0, 2, dtostrf(((float)BatteryGetVolts())/1000.0,4,1,buf_string));
		lcd_text_show(INFO_PAGE);
		
		free(date);
		free(time);
	}
}

/*******************************************************************************
* Function Name		: UploadRead
* Description		: Update the reading if in READ_PAGE
* Input				: void
* Return			: void
*******************************************************************************/
void UploadRead(void) {
	if (lcd_showing == READ_PAGE && menu_selected < 8) {
		page_status = UPDATE;
	}
}

/*******************************************************************************
* Function Name		: MenuBusyUpload
* Description		: Show busy uploading on screen
* Input				: void
* Return			: void
*******************************************************************************/
void MenuBusyUpload(uint8_t set) {
	if (set) {
		lcd_text_clr(BLANK_PAGE, 0, 0, 80);
		lcd_text_set(BLANK_PAGE, 1, 1, "UPLOADING.......");
		lcd_text_set(BLANK_PAGE, 1, 2, "DO NOT POWER OFF");
		lcd_text_show(BLANK_PAGE);
		lcd_write();
		page_status = DISP;
		lcd_showing = BLANK_PAGE;
	} else {
		InfoPage();
	}
}

/*******************************************************************************
* Function Name		: InfoPage
* Description		: Update the info page
* Input				: void
* Return			: void
*******************************************************************************/
static void InfoPage(void) {
	char buf_string[6];
	lcd_text_clr(INFO_PAGE, 0, 0, PAGE_SIZE);	// Clear whole page
	lcd_text_set(INFO_PAGE, 0, 0, "SIGRA LOGGER");
	
	sprintf(buf_string, "%05u", SettingsGetSerial());
	lcd_text_set(INFO_PAGE, 13, 0, buf_string);
	
	sprintf(buf_string, "%03u", SettingsGetFWversion());
	lcd_text_set(INFO_PAGE, 0, 1, "FW     - ");
	lcd_text_set(INFO_PAGE, 3, 1, buf_string);
	lcd_text_set(INFO_PAGE, 9, 1, Get_Date(SettingsGetBuildDate()));  // BD = Build Date

	lcd_text_set(INFO_PAGE, 0, 2, dtostrf(((float)BatteryGetVolts())/1000,4,1,buf_string));
	lcd_text_set(INFO_PAGE, 0, 2, "     Volts");
	
	lcd_text_set(INFO_PAGE, 0, 3, Get_Date(RTC_Get()));
	lcd_text_set(INFO_PAGE, 11, 3, Get_Time(RTC_Get()));
	
	if (GetLogState() == STOP) {
		lcd_text_set(INFO_PAGE, 11, 2, "SD Fail");
	}
	
	if (SettingGetRole() == ROLE_MASTER) {
		lcd_text_set(INFO_PAGE, 19, 0, "*");
	}
	
	lcd_text_show(INFO_PAGE);
	
	page_status = DISP;
	
	lcd_showing = INFO_PAGE;
}

/*******************************************************************************
* Function Name		: MenuPage
* Description		: Show the menu page
* Input				: void
* Return			: void
*******************************************************************************/
static void MenuPage(void) {
	lcd_text_clr(MENU_PAGE, 0, 0, 80);
	if (menu_selected < 4) {
		lcd_text_set(MENU_PAGE, 0, 0, menu_options[0]);
		lcd_text_set(MENU_PAGE, 0, 1, menu_options[1]);
		lcd_text_set(MENU_PAGE, 0, 2, menu_options[2]);
		lcd_text_set(MENU_PAGE, 0, 3, menu_options[3]);
		
		lcd_text_set(MENU_PAGE, 18, menu_selected, "<");
	} else {
		lcd_text_set(MENU_PAGE, 0, 0, menu_options[menu_selected - 3]);
		lcd_text_set(MENU_PAGE, 0, 1, menu_options[menu_selected - 2]);
		lcd_text_set(MENU_PAGE, 0, 2, menu_options[menu_selected - 1]);
		lcd_text_set(MENU_PAGE, 0, 3, menu_options[menu_selected]);
		
		lcd_text_set(MENU_PAGE, 18, 3, "<");
	}
	
	lcd_text_show(MENU_PAGE);
	
	page_status = DISP;
	
	lcd_showing = MENU_PAGE;
}

/*******************************************************************************
* Function Name		: ReadPage
* Description		: Show outline of read page
* Input				: void
* Return			: void
*******************************************************************************/
static void ReadPage(void) {
	lcd_text_clr(READ_PAGE, 0, 0, PAGE_SIZE);
	lcd_text_set(READ_PAGE, 0, 0, "Data Type:");
	lcd_text_set(READ_PAGE, 0, 1, "Reading:");
	lcd_text_set(READ_PAGE, 0, 2, "Unit:");
	
	page_status = DISP;
	
	lcd_text_show(READ_PAGE);
	
	lcd_showing = READ_PAGE;
}

/*******************************************************************************
* Function Name		: SetPage
* Description		: Show outline of set page
* Input				: void
* Return			: void
*******************************************************************************/
static void SetPage(void) {
	lcd_text_clr(SETTING_PAGE, 0, 0, PAGE_SIZE);
	lcd_text_set(SETTING_PAGE, 0, 0, setting_options[0]);
	switch (SettingGetSerialMode()) {
		case MODE_USB:
			lcd_text_set(SETTING_PAGE, 13, 0, "USB");
			break;
		case MODE_RS232:
			lcd_text_set(SETTING_PAGE, 13, 0, "RS232");
			break;
		case MODE_RS485:
			lcd_text_set(SETTING_PAGE, 13, 0, "RS485");
			break;
		case MODE_RADIO:
			lcd_text_set(SETTING_PAGE, 13, 0, "RADIO");
			break;
	}
	lcd_text_set(SETTING_PAGE, 0, 1, setting_options[1]);
	
	switch (SettingGetRole()) {
		case ROLE_MASTER:
			lcd_text_set(SETTING_PAGE, 13, 1, "Master");
			lcd_text_set(SETTING_PAGE, 0, 2, setting_options[3]);
			switch (SettingGetSlaveSerialMode()) {
				case MODE_RS485:
					lcd_text_set(SETTING_PAGE, 13, 2, "RS485");
					break;
				case MODE_RADIO:
					lcd_text_set(SETTING_PAGE, 13, 2, "RADIO");
					break;
				case MODE_DISABLE:
					lcd_text_set(SETTING_PAGE, 13, 2, "NO");
					break;
			}
			break;
		case ROLE_SLAVE:
			lcd_text_set(SETTING_PAGE, 13, 1, "Slave");
			lcd_text_set(SETTING_PAGE, 0, 2, setting_options[2]);
			char buf_string[1];
			sprintf(buf_string, "%u", (SettingGetSlaveNumber() + 1));
			lcd_text_set(SETTING_PAGE, 13, 2, buf_string);
			break;
	}
	
	lcd_text_set(SETTING_PAGE, 19, setting_selected, "<");
	
	page_status = DISP;
	lcd_text_show(SETTING_PAGE);
	lcd_showing = SETTING_PAGE;
}

/*******************************************************************************
* Function Name		: ChangeSet
* Description		: Handle setting change in LCD
* Input				: void
* Return			: void
*******************************************************************************/
static void ChangeSet(void) {
	uint8_t value;
	switch (setting_selected) {
		case 0:	// Mode
			value = SettingGetSerialMode();
			value = (value + 1) % 4;
			UartSetSerialMode(value);
			break;
		case 1:	// Role
			value = SettingGetRole();
			value = (value + 1) % 2;
			UartSetRole(value);
			break;
		case 2:
			if (SettingGetRole() == ROLE_SLAVE) {
				value = SettingGetSlaveNumber();
				value = (value + 1) % 5;
				SettingSetSlaveNumber(value);
			} else {
				value = SettingGetSlaveSerialMode();
				if (value == MODE_RS485) {
					SettingSetSlaveSerialMode(MODE_RADIO);
				} else if(value == MODE_RADIO) {
					SettingSetSlaveSerialMode(MODE_DISABLE);
				} else {
					SettingSetSlaveSerialMode(MODE_RS485);
				}
			}
			break;
	}
}

/*******************************************************************************
* Function Name		: RunDisp
* Description		: Every time LCD is IDLE, it will enter this,
*					  to check for button event
* Input				: void
* Return			: void
*******************************************************************************/
static void RunDisp(void) {
	if (IsKeySet(&pressed)) {
		SetChan(); // another key pressed
	}
}

/*******************************************************************************
* Function Name		: RunChan
* Description		: Button event detect, handle the event
* Input				: void
* Return			: void
*******************************************************************************/
static void RunChan(void) {
	if (IsKeySet(&pressed)) {
		SetChan();
	}

	if (IsKeySet(&released)) {
		if (lcd_showing == INFO_PAGE) {
			if (pressed_key == KEY_RIGHT) {
				MenuPage();
			}
		} else if (lcd_showing == MENU_PAGE) {
			switch (pressed_key) {
				case KEY_UP:
					if (menu_selected == 0) {
						menu_selected = menu_count - 1;
					} else {
						menu_selected = menu_selected - 1;
					}
					MenuPage();
					break;
				case KEY_RIGHT:
					if (menu_selected < 8) {
						ReadPage();
						RunUpdate();
					} else {
						SetPage();
					}
					break;
				case KEY_DOWN:
					if (menu_selected == menu_count - 1) {
						menu_selected = 0;
					} else {
						menu_selected = menu_selected + 1;
					}
					MenuPage();
					break;
				case KEY_LEFT:
					InfoPage();
					break;
			}
		} else if (lcd_showing == READ_PAGE) {
			switch (pressed_key) {
				case KEY_LEFT:
				MenuPage();
			}
		} else if (lcd_showing == SETTING_PAGE) {
			switch (pressed_key) {
				case KEY_LEFT:
					MenuPage();
					break;
				case KEY_DOWN:
					if (setting_selected == setting_count - 1) {
						setting_selected = 0;
					} else {
						setting_selected = setting_selected + 1;
					}
					SetPage();
					break;
				case KEY_UP:
					if (setting_selected == 0) {
						setting_selected = setting_count - 1;
					} else {
						setting_selected = setting_selected - 1;
					}
					SetPage();
					break;
				case KEY_RIGHT:
					ChangeSet();
					SetPage();
					break;
			}
		}
	}
}

/*******************************************************************************
* Function Name		: RunUpdate
* Description		: Used for update reading for READ_PAGE
* Input				: void
* Return			: void
*******************************************************************************/
static void RunUpdate(void) {
	lcd_text_set(READ_PAGE, 11, 0, "        ");
	char reading_string[8];
	switch (SettingGetSensorType(menu_selected)) {
		case ST_UNUSED:
			lcd_text_set(READ_PAGE, 11, 0, "UNUSED");
			lcd_text_set(READ_PAGE, 9, 1, "           ");
			lcd_text_set(READ_PAGE, 6, 2, "              ");
			lcd_text_show(READ_PAGE);
			page_status = DISP;
			RunDisp();
			return;
		case ST_VIBRATING_WIRE:
			lcd_text_set(READ_PAGE, 11, 0, "VW");
			break;
		case ST_RESISTANCE:
			lcd_text_set(READ_PAGE, 11, 0, "RESIST");
			break;
		case ST_VOLTAGE:
			lcd_text_set(READ_PAGE, 11, 0, "VOLTAGE");
			break;
		case ST_FREQUENCY:
			lcd_text_set(READ_PAGE, 11, 0, "FREQ");
			break;
		case ST_COUNTER:
			lcd_text_set(READ_PAGE, 11, 0, "COUNTER");
			break;
			
	}
	
	lcd_text_set(READ_PAGE, 6, 2, (char*)U32ToU8(SettingGetSensorUnit(menu_selected)));
	lcd_text_set(READ_PAGE, 10, 2, "          ");
	// Reading function here
	lcd_text_set(READ_PAGE, 9, 1, "           ");
	if (GetRealReading()[menu_selected] < 999999) {
		lcd_text_set(READ_PAGE, 9, 1, dtostrf((GetRealReading()[menu_selected]), 4, 3, reading_string));
	} else {
		lcd_text_set(READ_PAGE, 9, 1, "Too Large");
	}
	
	lcd_text_show(READ_PAGE);
	page_status = DISP;
	RunDisp();
}

/*******************************************************************************
* Function Name		: set_chan
* Description		: Set state to CHAN
* Input				: void
* Return			: void
*******************************************************************************/
static void SetChan(void) {
	page_status = CHAN;

	pressed_key = ChannelFromKey(key_store);
}

/*******************************************************************************
* Function Name		: channel_from_key
* Description		: Convert code to key pressed
* Input				: void
* Return			: void
*******************************************************************************/
static uint8_t ChannelFromKey(uint8_t code) {
	switch (code) {
		case  8 : return KEY_DOWN;		// key 1  //Down
		case  1 : return KEY_RIGHT;		// key 2  //Right
		case  4 : return KEY_LEFT;		// key 3  //Left
		case  2 : return KEY_UP;		// key 4  //Up
	}
	return 0;
}