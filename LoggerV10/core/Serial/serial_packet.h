/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: serial_packet.h
 * Description: Header file for serial_packet, all flags and constants for serial communication are defined here
 */ 


#ifndef SERIAL_PACKET_H_
#define SERIAL_PACKET_H_

#include "../RTC/DS3232.h"

#define START_BYTE		0xB2
#define STOP_BYTE		0xDC

#define FLAG_1			4
#define FLAG_2			5
#define BODY_START		6

enum Serial_flag
{
	FIN =				0x01,
	SYN =				0x02,
	ACK =				0x04,
	ACK_SYN =			0x06,
	SET =				0x08,
	DAT =				0x10,
	FOR =				0x20,
	UPLOAD =			0x40,
};

enum Set_flag
{
	SER =               0x01,
	FWV =               0x02,
	RTC =               0x03,
	BUILD_DATE =        0x04,
	CHECK_DATE =        0x05,
	MODE =              0x06,
	ROLE =              0x07,
	SENSOR_TYPE =       0X08,
	SENESOR_SETTLE =    0X09,
	SENSOR_START =      0X0A,
	SENSOR_STOP =       0x0B,
	SENSOR_CYCLES =     0x0C,
	SENSOR_INTERVAL =   0x0D,
	SENSOR_EQUATION =   0x0E,
	SENSOR_COF =        0x0F,
	SENSOR_COMP =		0x10,
	SENSOR_COMPCHAN =   0x11,
	COUNTER_RESET =		0x12,
	SAMPLE_CHANGE =		0x13,
	SENSOR_UNIT =       0x14,
	RESET_SENSOR =      0x15,
	RESET_SD =          0x16,
	SLAVE_LIST =		0x17,
	SLAVE_NUMBER =		0x18,
	SENSOR =			0x19,
	ENABLE_RECORD =		0x1A,
	RECORD =			0x1B,
	ROLE_INFO_SET =		0x1C,
	SYSTEM_INFO_SET =   0x1D,
	RADIO_HPID =		0x1E,
	ENABLE_SYNCHRONIZE = 0x1F,
	ENABLE_FASTREAD =	0x20,
};

enum Dat_flag
{
	RTM =				0x01,
	RED =				0x02,
	LST =				0x03,
	SENSOR_INFO =		0x04,
	RECORD_INFO =       0x05,
	SLST =				0x06,
	ROLE_INFO_DAT =		0x07,
	RADIO_DAT =			0x08,
};

void SerialInit(void);

void SerialWalk(void);

void SerialRun(void);

void SendRTMData(uint8_t channel, float data);

uint8_t GetSerialState(void);

#endif /* SERIAL_PACKET_H_ */