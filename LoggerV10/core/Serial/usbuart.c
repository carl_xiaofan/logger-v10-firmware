/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: usbuart.c
 * Description: File for USB Uart communication
 */ 


#include <avr/interrupt.h>
#include <avr/io.h>
#include "../../common/util.h"
#include "usbuart.h"
#include "radio.h"

// variables
uint8_t USB_RxBuffer[USB_INPUT_BUFFER_SIZE];
uint8_t USB_RxReadPosition = 0;
uint8_t USB_RxWritePosition = 0;
uint8_t USB_is_next_buffer;

void USBUartInit(void) {
	USB_is_next_buffer = 0;
	USBUART_SETBAUD();
	USBUART_HIGH_END();
	
	USBUART_RX_ENABLE();
	USBUART_TX_ENABLE();
	
	USBUART_RX_INT_ON();
}

/*******************************************************************************
* Function Name		: usbuart_sendbyte
* Description		: Send one byte to serial
* Input				: data byte
* Return			: void
*******************************************************************************/
void USBUartSendByte(uint8_t data) {
	while (USBUART_DATA_REG_EMPTY() == 0);	// make sure the data register is cleared
	
	USBUART_DATA_REG() = data;
}

/*******************************************************************************
* Function Name		: usbuart_sendint
* Description		: Send int to serial to display as hex
* Input				: unsigned int
* Return			: void
*******************************************************************************/
void USBUartSendInt(unsigned long i) {
	if (i == 0) {
		USBUartSendByte(48);
	}
	unsigned long int_copy = i;
	uint8_t count = 0;
	
	while (int_copy) {
		count++;
		int_copy = int_copy / 10;
	}
	
	int_copy = count - 1;
	uint8_t number[count];
	while (i) {
		number[int_copy] = (uint8_t)(i % 10 + 48);
		i = i / 10;
		int_copy--;
	}
	
	for (uint8_t y = 0; y < count; y++) {
		USBUartSendByte(number[y]);
	}
}

/*******************************************************************************
* Function Name		: usbuart_senduin8array
* Description		: Send uint8_t* for certain size
* Input				: data byte array
*					: size size of array
* Return			: void
*******************************************************************************/
void USBUartSendUin8Array(uint8_t* data ,unsigned int size) {
	for (unsigned int i = 0; i < size; i++) {
		USBUartSendByte(data[i]);
	}
}

/*******************************************************************************
* Function Name		: usbuart_sendstring
* Description		: Send char* for certain size
* Input				: data byte array
*					: size size of array
* Return			: void
*******************************************************************************/
void USBUartSendString(char* data ,uint8_t size) {
	for (unsigned int i = 0; i < size; i++) {
		USBUartSendByte(data[i]);
	}
}

/*******************************************************************************
* Function Name		: usbuart_ischaravailable
* Description		: is any char in input buffer
* Input				: void
* Return			: true if does
*******************************************************************************/
uint8_t USBUartIsCharAvailable(void)  {
	if (USB_RxWritePosition > USB_RxReadPosition) {
		return 1;
	}
	
	if (USB_is_next_buffer) {
		return 1;
	}

	return 0;
}

/*******************************************************************************
* Function Name		: usbuart_clearbuffer
* Description		: Clear the input buffer
* Input				: void
* Return			: void
*******************************************************************************/
void USBClearBuffer(void) {
	USB_RxReadPosition = USB_RxWritePosition;
}

/*******************************************************************************
* Function Name		: usbuart_readbyte
* Description		: Read a char from in buffer
* Input				: void
* Return			: uint_8 read
*******************************************************************************/
uint8_t USBUartReadByte(void) {
	uint8_t byte_read;
	byte_read = USB_RxBuffer[USB_RxReadPosition];
	USB_RxReadPosition++;
	if (USB_RxReadPosition == USB_INPUT_BUFFER_SIZE) {
		USB_RxReadPosition = 0;
		USB_is_next_buffer = 0;
	}
	return byte_read;
}

/*******************************************************************************
* Function Name		: USBUART_RX_INT_ISR
* Description		: Interrupt for receive serial data and put into in buffer
*******************************************************************************/
USBUART_RX_INT_ISR() {
	uint8_t rData = USBUART_DATA_REG();
	USB_RxBuffer[USB_RxWritePosition] = rData;
	USB_RxWritePosition++;
	if (USB_RxWritePosition == USB_INPUT_BUFFER_SIZE) {
		USB_RxWritePosition = 0;
		USB_is_next_buffer = 1;
	}
}