/*
 * packetHandler.h
 *
 * Created: 8/07/2020 12:56:05 PM
 *  Author: xiaofan
 */ 


#ifndef PACKETHANDLER_H_
#define PACKETHANDLER_H_

void PacketReceiveSystemInfo(uint8_t* packet);

void PacketReceiveRoleInfo(uint8_t* packet);

void PacketReceiveRecordInfo(uint8_t* packet);

void PacketReceiveSensorInfo(uint8_t* packet);

void PacketReceiveFor(uint8_t* packet,uint8_t size);

void PacketSendFOR(void);

void PacketSendRadioInfo(void);

void PacketSendUpload(void);

void PacketSendSLST(void);

void PacketSendROLE(void);

void PacketSendRecordInfo(void);

void PacketSendSensorInfo(uint8_t channel);

void PacketSendRTMData(uint8_t channel, float data);

void PacketSendACKSYN(void);

#endif /* PACKETHANDLER_H_ */