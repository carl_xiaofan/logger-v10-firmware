/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: rs232uart.h
 * Description: Header file of rs232uart.c
 */ 

#ifndef RS232UART_H_
#define RS232UART_H_

// Variables
#define BAUD_RS232 115200
#define RS232_INPUT_BUFFER_SIZE        80

#define RS232UART_BAUDRATE_REGISTER  UBRR2L
#define RS232UART_SETBAUD()          UBRR2L  = (((F_CPU / (BAUD_RS232 * 16UL))) - 1)
#define RS232UART_HIGH_END()		 UBRR2H = ((((F_CPU / (BAUD_RS232 * 16UL))) - 1) >> 8)
#define RS232UART_RX_ENABLE()        UCSR2B |=  (_BV(RXEN2))
#define RS232UART_TX_ENABLE()        UCSR2B |=  (_BV(TXEN2))
#define RS232UART_TX_DISABLE()       UCSR2B &= ~(_BV(TXEN2))

#define RS232UART_DATA_REG_EMPTY()   (UCSR2A &   (1<<UDRE2))

#define RS232UART_DATA_REG()         UDR2
#define RS232UART_RXD()              PINH   &   (_BV(PH0))
#define RS232UART_RX_INT_ON()        UCSR2B |=  (_BV(RXCIE2))
#define RS232UART_RX_INT_OFF()       UCSR2B &= ~(_BV(RXCIE2))
#define RS232UART_RX_INT_CLR()       UCSR2A |=  (_BV(RXC2))
#define RS232UART_RX_INT_ISR()       ISR(USART2_RX_vect)

#define RS232UART_TXD_HI()           PORTH  |=  (_BV(PH1))
#define RS232UART_TXD_LO()           PORTH  &= ~(_BV(PH1))
#define RS232UART_TX_INT_ON()        UCSR2B |=  (_BV(TXCIE2))
#define RS232UART_TX_INT_OFF()       UCSR2B &= ~(_BV(TXCIE2))
#define RS232UART_TX_INT_IS_ON()     UCSR2B &   (_BV(TXCIE2))

#define RS232UART_TX_INT_CLR()       UCSR2A |=  (_BV(TXC2))
#define RS232UART_TX_INT_ISR()       ISR(USART2_TX_vect)

#define RS232UART_DRIVER_ON()        PORTL  |=  (_BV(PL3))
#define RS232UART_DRIVER_OFF()       PORTL  &= ~(_BV(PL3))

#define RS232LINK_WAKE_INT_MSK_SET() PCMSK0 |=  (_BV(PCINT6))
#define RS232LINK_WAKE_INT_MSK_CLR() PCMSK0 &= ~(_BV(PCINT6))

#define RS232_INT                    _BV(PB6)

#define RS232UART_PARITY_NONE()      UCSR0C &= ~((_BV(UPM01)) | (_BV(UPM00)))
#define RS232UART_PARITY_EVEN()      UCSR0C  =  ((_BV(UPM01)) | (_BV(UCSZ01)) | (_BV(UCSZ00)))
#define RS232UART_PARITY_ODD()       UCSR0C |=  ((_BV(UPM01)) | (_BV(UPM00)))
#define RS232UART_TWO_STOP_BITS()    UCSR0C |=  (_BV(USBS0))
#define RS232UART_ONE_STOP_BITS()    UCSR0C &= ~(_BV(USBS0))

#define RS232UART_STATUS()           UCSR0A
#define RS232UART_PARITY_ERROR_BIT   _BV(UPE0)

#define RTU232_TIMER_COUNT()         TCNT3
#define RTU232_TIMER_CONTROL_B()     TCCR3B
#define RTU232_TIMER_OUT_COMP_A()    OCR3A
#define RTU232_TIMER_OUT_COMP_B()    OCR3B
#define RTU232_TIMER_INT_MASK_REG()  TIMSK3
#define RTU232_TIMER_INT_FLAG_REG()  TIFR3

#define RTU232_T15_MASK()            1<<OCIE3A
#define RTU232_T35_MASK()            1<<OCIE3B
#define RTU232_T15_FLAG_CLR()        1<<OCF3A
#define RTU232_T35_FLAG_CLR()        1<<OCF3B
#define RTU232_PRESCALE()            TCCR3B = ((1<<CS31) | (1<<CS30))
#define RTU232_PRESCALE_VALUE        64


void RS232UartInit(void);

void RS232UartSendByte(uint8_t data);

void RS232UartSendUint8Array(uint8_t* data ,unsigned int size);

void RS232UartSendString(char* data ,uint8_t size);

uint8_t RS232UartIsCharAvailable(void);

uint8_t RS232UartReadByte(void);

void RS232UartClearBuffer(void);

#endif /* RS232UART_H_ */