/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: rs485uart.c
 * Description: File for RS485 Uart communication
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "../Setting/setting.h"
#include "rs485uart.h"

// variables
uint8_t RS485_RxBuffer[RS485_INPUT_BUFFER_SIZE];
uint16_t RS485_RxReadPosition = 0;
uint16_t RS485_RxWritePosition = 0;
uint8_t rs485_is_next_buffer;

void RS485UartInit(void) {
	rs485_is_next_buffer = 0;
	RS485UART_SETBAUD();
	
	RS485UART_RX_ENABLE();
	RS485UART_TX_ENABLE();
	RS485UART_DE_OFF();
	RS485UART_TX_INT_OFF();
	RS485UART_RE_ON();
	RS485UART_RX_INT_ON();
}

void RS485UartTXStart(void) {
	RS485UART_DE_ON();
	RS485UART_RE_OFF();
}

void RS485UartTXStop(void) {
	_delay_ms(1);
	RS485UART_DE_OFF();
	RS485UART_RE_ON();
}

/*******************************************************************************
* Function Name		: rs485uart_sendbyte
* Description		: Send one byte to RS485, need tx_start first
* Input				: data byte
* Return			: void
*******************************************************************************/
void RS485UartSendByte(uint8_t data) {
	while (RS485UART_DATA_REG_EMPTY() == 0);
	RS485UART_DATA_REG() = data;
}

/*******************************************************************************
* Function Name		: rs485uart_senduin8array
* Description		: Send uint8_t* for certain size, need tx_start first
* Input				: data byte array
*					: size size of array
* Return			: void
*******************************************************************************/
void RS485UartSendUint8Array(uint8_t* data ,unsigned int size) {
	for (unsigned int i = 0; i < size; i++) {
		RS485UartSendByte(data[i]);
	}
}

/*******************************************************************************
* Function Name		: rs485uart_ischaravailable
* Description		: is any char in RS485 input buffer
* Input				: void
* Return			: true if does
*******************************************************************************/
uint8_t RS485UartIsCharAvailable(void) {
	if (RS485_RxWritePosition > RS485_RxReadPosition) {
		return 1;
	}
	
	if (rs485_is_next_buffer) {
		return 1;
	}
	return 0;
}

/*******************************************************************************
* Function Name		: rs485uart_clearbuffer
* Description		: Clear the input buffer
* Input				: void
* Return			: void
*******************************************************************************/
void RS485UartClearBuffer(void) {
	RS485_RxReadPosition = RS485_RxWritePosition;
}

/*******************************************************************************
* Function Name		: rs485uart_readbyte
* Description		: Read a char from in buffer
* Input				: void
* Return			: uint_8 read
*******************************************************************************/
uint8_t RS485UartReadByte(void) {
	uint8_t byte_read;
	byte_read = RS485_RxBuffer[RS485_RxReadPosition];
	RS485_RxReadPosition++;
	if (RS485_RxReadPosition == RS485_INPUT_BUFFER_SIZE) {
		rs485_is_next_buffer = 0;
		RS485_RxReadPosition = 0;
	}
	return byte_read;
}

/*******************************************************************************
* Function Name		: RS485UART_RX_INT_ISR
* Description		: Interrupt for receive serial data and put into RS485 in buffer
*******************************************************************************/
RS485UART_RX_INT_ISR() {
	uint8_t rData = RS485UART_DATA_REG();
	RS485_RxBuffer[RS485_RxWritePosition] = rData;
	RS485_RxWritePosition++;
	if (RS485_RxWritePosition == RS485_INPUT_BUFFER_SIZE) {
		RS485_RxWritePosition = 0;
		rs485_is_next_buffer = 1;
	}
}