/*
 * uart.c
 *
 * Created: 4/06/2020 11:10:54 AM
 *  Author: xiaofan
 */

#include <avr/io.h>
#include <stdlib.h>
#include <util/delay.h>
#include "../../common/util.h"
#include "../Setting/setting.h"
#include "../../main.h"
#include "uart.h"
#include "rs232uart.h"
#include "rs485uart.h"
#include "radio.h"
#include "serial_packet.h"
#include "usbuart.h"

static uint8_t role;
static uint8_t serial_mode;

void UartInit(void) {
	USBUartInit();
	RS232UartInit();
	RS485UartInit();
	RadioInit();
	
	role = SettingGetRole();
	serial_mode = SettingGetSerialMode();
}

void UartSetSerialMode(uint8_t mode) {
	SettingSetSerialMode(mode);
	serial_mode = mode;
	
	UartClearBuffer();
}

void UartSetRole(uint8_t r) {
	SettingSetRole(r);
	role = r;
}

uint8_t UartGetSerialMode(void) {
	return serial_mode;
}

void UartClearBuffer(void) {
	switch (serial_mode) {
		case MODE_USB:
			USBClearBuffer();
			break;
		case MODE_RS232:
			RS232UartClearBuffer();
			break;
		case MODE_RS485:
			RS485UartClearBuffer();
			break;
		case MODE_RADIO:
			RadioClearBuffer();
			break;
	}
}

uint8_t UartIsNextCharAvailable(uint8_t c) {
	switch (serial_mode) {
		case MODE_USB:
			if(USBUartIsCharAvailable()) {
				if (USBUartReadByte() == c) {
					return 1;
				}
			}
			break;
		case MODE_RS232:
			if(RS232UartIsCharAvailable()) {
				if (RS232UartReadByte() == c) {
					return 1;
				}
			}
			break;
		case MODE_RS485:
			if(RS485UartIsCharAvailable()) {
				if (RS485UartReadByte() == c) {
					return 1;
				}
			}
			break;
		case MODE_RADIO:
			if(RadioIsCharAvailable()) {
				if (RadioReadByte() == c) {
					return 1;
				}
			}
			break;
	}
	return 0;
}

uint8_t UartMasterIsNextCharAvailable(uint8_t c) {
	if (role == ROLE_MASTER) {
		switch (SettingGetSlaveSerialMode()) {
			case MODE_RS485:
				if(RS485UartIsCharAvailable()) {
					if (RS485UartReadByte() == c) {
						return 1;
					}
				}
				break;
			case MODE_RADIO:
				if(RadioIsCharAvailable()) {
					if (RadioReadByte() == c) {
						return 1;
					}
				}
				break;
		}
	}
	return 0;
}

void UartSendPacket(uint8_t* packet, uint8_t size) {
	uint16_t serial_number = SettingsGetSerial();
	
	packet[0] = START_BYTE;
	packet[1] = START_BYTE;
	packet[2] = 0;
	packet[3] = 0;
	packet[4] = U16ToU8(serial_number)[0];
	packet[5] = U16ToU8(serial_number)[1];
	
	packet[size++] = STOP_BYTE;
	packet[size++] = STOP_BYTE;
	
	switch (serial_mode) {
		case MODE_USB:
			USBUartSendUin8Array(packet, size);
			break;
		case MODE_RS232:
			RS232UartSendUint8Array(packet, size);
			break;
		case MODE_RS485:
			RS485UartTXStart();
			RS485UartSendUint8Array(packet, size);
			RS485UartTXStop();
			break;
		case MODE_RADIO:
			RadioSendUint8Array(packet, size);
			break;
	}
}

