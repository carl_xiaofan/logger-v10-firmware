/*
 * radio.c
 *
 * Created: 1/05/2020 10:20:16 AM
 *  Author: xiaofan
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdlib.h>
#include "radio.h"
#include "usbuart.h"

// variables
uint8_t Radio_RxBuffer[RADIO_INPUT_BUFFER_SIZE];
uint16_t Radio_RxReadPosition = 0;
uint16_t Radio_RxWritePosition = 0;
uint8_t Radio_is_next_buffer;

static void RadioEnterAT(void);
static void RadioSendCommend(uint8_t c1, uint8_t c2, char* parameters, unsigned int size);
static uint8_t RadioCheckOK(void);

void RadioInit() {	
	DMUART_RTS_LO();
	DMUART_SLEEP_CON_LO();
	DMUART_SETBAUD_115200();
	DMUART_TX_INT_OFF();
	DMUART_RX_INT_ON();
	DMUART_RX_ENABLE();
	DMUART_TX_ENABLE();
	DMUART_POWER_ON();
	DMUART_RESET_HI();
	
	Radio_is_next_buffer = 0;
}

/*******************************************************************************
* Function Name		: radio_sendbyte
* Description		: Send one byte to radio
* Input				: data byte
* Return			: void
*******************************************************************************/
void RadioSendByte(uint8_t data) {
	while (DMUART_DATA_REG_EMPTY() == 0);
	DMUART_DATA_REG() = data;
}

/*******************************************************************************
* Function Name		: radio_senduin8array
* Description		: Send uint8_t* for certain size
* Input				: data byte array
*					: size size of array
* Return			: void
*******************************************************************************/
void RadioSendUint8Array(uint8_t* data ,unsigned int size) {
	for (unsigned int i = 0; i < size; i++) {
		RadioSendByte(data[i]);
	}
}

/*******************************************************************************
* Function Name		: radio_ischaravailable
* Description		: is any char in radio input buffer
* Input				: void
* Return			: true if does
*******************************************************************************/
uint8_t RadioIsCharAvailable(void) {
	if (Radio_RxWritePosition > Radio_RxReadPosition) {
		return 1;
	}
	
	if (Radio_is_next_buffer) {
		return 1;
	}

	return 0;
}

/*******************************************************************************
* Function Name		: radio_clearbuffer
* Description		: Clear the input buffer
* Input				: void
* Return			: void
*******************************************************************************/
void RadioClearBuffer(void) {
	Radio_RxReadPosition = Radio_RxWritePosition;
}

/*******************************************************************************
* Function Name		: radio_readbyte
* Description		: Read a char from in buffer
* Input				: void
* Return			: uint_8 read
*******************************************************************************/
uint8_t RadioReadByte(void) {
	uint8_t byte_read;
	byte_read = Radio_RxBuffer[Radio_RxReadPosition];
	Radio_RxReadPosition++;
	if (Radio_RxReadPosition == RADIO_INPUT_BUFFER_SIZE) {
		Radio_is_next_buffer = 0;
		Radio_RxReadPosition = 0;
	}
	return byte_read;
}

uint8_t RadioGetHPID(uint8_t* hp, uint8_t** id) {
	RadioClearBuffer();
	RadioEnterAT();
	
	if (!RadioCheckOK()) {
		return 0;
	}
	
	RadioClearBuffer();
	RadioSendCommend('H', 'P', " ", 0);
	
	while (!RadioIsCharAvailable()) {
		_delay_ms(1);
	}

	*hp = RadioReadByte();
	
	RadioClearBuffer();
	RadioSendCommend('I', 'D', "    ", 0);
	
	while (!RadioIsCharAvailable()) {
		_delay_ms(1);
	}
	
	(*id)[0] = RadioReadByte();
	(*id)[1] = RadioReadByte();
	(*id)[2] = RadioReadByte();
	(*id)[3] = RadioReadByte();
	
	RadioSendCommend('C', 'N', "", 0);
	
	return 1;
}

uint8_t RadioSetHPID(uint8_t hp, uint8_t id1, uint8_t id2, uint8_t id3, uint8_t id4) {
	RadioClearBuffer();
	RadioEnterAT();
	
	if (!RadioCheckOK()) {
		RadioClearBuffer();
		RadioEnterAT();
		if (!RadioCheckOK()) {
			return 0;
		}
	}
	
	char buf_string[4];
	sprintf(buf_string, "%u", hp);
	RadioSendCommend('H', 'P', buf_string, 1);
	
	RadioSendCommend('W', 'R', "", 0);
	
	sprintf(buf_string, "%c%c%c%c", id1, id2, id3, id4);
	RadioSendCommend('I', 'D', buf_string, 4);
		
	RadioSendCommend('W', 'R', "", 0);
		
	RadioSendCommend('C', 'N', "", 0);
	
	return 1;
}

static void RadioEnterAT(void) {
	RadioSendByte('+');
	RadioSendByte('+');
	RadioSendByte('+');
}

static void RadioSendCommend(uint8_t c1, uint8_t c2, char* parameters, unsigned int size) {
	RadioSendByte('A');
	RadioSendByte('T');
	RadioSendByte(c1);
	RadioSendByte(c2);
	RadioSendUint8Array((uint8_t*)parameters, size);
	RadioSendByte(0x0D);
}

static uint8_t RadioCheckOK(void) {
	uint8_t t = 0;
	while (!RadioIsCharAvailable()) {
		_delay_ms(10);
		t++;
		if (t > 200) {
			return 0;
		}
	}
	
	if (RadioReadByte() == 'O') {
		if (RadioReadByte() == 'K') {
			return 1;
		} else {
			return 0;
		}
	} else {
		return 0;
	}
}

DMUART_RX_INT_ISR() {
	uint8_t rData = DMUART_DATA_REG();
	Radio_RxBuffer[Radio_RxWritePosition] = rData;
	Radio_RxWritePosition++;
	if (Radio_RxWritePosition == RADIO_INPUT_BUFFER_SIZE) {
		Radio_RxWritePosition = 0;
		Radio_is_next_buffer = 1;
	}
}