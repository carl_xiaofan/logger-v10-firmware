/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: usbuart.h
 * Description: Header file of usbuart.c
 */ 

#ifndef USBUART_H_
#define USBUART_H_

// Variables
#define BAUD_USB 9600
#define USB_INPUT_BUFFER_SIZE        80
#define BAUD_PRESCALE (((F_CPU / (BAUD_USB * 16UL))) - 1)

// Firmware
#define USBUART_SETBAUD()            UBRR1L  = BAUD_PRESCALE
#define USBUART_HIGH_END()			 UBRR1H = (BAUD_PRESCALE >> 8)
#define USBUART_RX_ENABLE()          UCSR1B |= (_BV(RXEN1))
#define USBUART_TX_ENABLE()          UCSR1B |= (_BV(TXEN1))

#define USBUART_RX_INT_ON()          UCSR1B |=  (_BV(RXCIE1))
#define USBUART_RX_INT_OFF()         UCSR1B &= ~(_BV(RXCIE1))
#define USBUART_RX_INT_ISR()         ISR(USART1_RX_vect)

#define USBUART_TX_INT_ON()          UCSR1B |=  (_BV(UDRIE1))
#define USBUART_TX_INT_OFF()         UCSR1B &= ~(_BV(UDRIE1))
#define USBUART_TX_INT_ISR()         ISR(USART1_UDRE_vect)

#define USBUART_DATA_REG()           UDR1

#define USBUART_DATA_REG_EMPTY()     (UCSR1A & (_BV(UDRE1)))

#define USBLINK_WAKE_INT_MSK_SET()   PCMSK0 |=  (_BV(PCINT7))	// Used for sleep mode
#define USBLINK_WAKE_INT_MSK_CLR()   PCMSK0 &= ~(_BV(PCINT7))	// Used for sleep mode

#define USB_INT                      _BV(PB7)					// Do not know the use of this

// Prototypes
void USBUartInit(void);

void USBUartSendByte(uint8_t data);

void USBUartSendInt(unsigned long i);

void USBUartSendUin8Array(uint8_t* data ,unsigned int size);

void USBUartSendString(char* data ,uint8_t size);

uint8_t USBUartIsCharAvailable(void);

void USBClearBuffer(void);

uint8_t USBUartReadByte(void);

#endif /* USBUART_H_ */