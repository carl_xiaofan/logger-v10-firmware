/*
 * uart.h
 *
 * Created: 4/06/2020 11:11:03 AM
 *  Author: xiaofan
 */ 


#ifndef UART_H_
#define UART_H_

void UartInit(void);

void UartSetSerialMode(uint8_t mode);

void UartSetRole(uint8_t r);

uint8_t UartGetSerialMode(void);

void UartClearBuffer(void);

uint8_t UartIsNextCharAvailable(uint8_t c);

uint8_t UartMasterIsNextCharAvailable(uint8_t c);

void UartSendPacket(uint8_t* packet, uint8_t size);


#endif /* UART_H_ */