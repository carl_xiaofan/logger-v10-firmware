/*
 * packetHandler.c
 *
 * Created: 8/07/2020 12:55:51 PM
 *  Author: xiaofan
 */ 
#include <avr/io.h>
#include <stdlib.h>
#include "../../common/util.h"
#include "../Setting/setting.h"
#include "../Read/sample.h"
#include "../../main.h"
#include "../RTC/DS3232.h"
#include "packetHandler.h"
#include "serial_packet.h"
#include "usbuart.h"
#include "rs232uart.h"
#include "rs485uart.h"
#include "radio.h"
#include "uart.h"

static uint8_t next_reading_logger[10];
static uint8_t forward_size;;

/*******************************************************************************
* Function Name		: PacketReceiveSystemInfo
* Description		: Handle Receive System Info settings
* Input				: receive packet
* Return			: void
*******************************************************************************/
void PacketReceiveSystemInfo(uint8_t* packet) {
	SettingsSetSerial(U8ToU16(packet[BODY_START], packet[BODY_START + 1]));
	
	SettingsSetFWversion(U8ToU16(packet[BODY_START + 2], packet[BODY_START + 3]));
	
	SettingsSetBuildDate(bytes_to_datetime(packet, BODY_START + 4));
	
	SettingSetLastSetting(bytes_to_datetime(packet, BODY_START + 11));
	
	SettingSetCheck(bytes_to_datetime(packet, BODY_START + 18));
	
	SettingSetSerialMode(packet[BODY_START + 25]);
	
	UartSetSerialMode(packet[BODY_START + 25]);
}

/*******************************************************************************
* Function Name		: PacketReceiveRoleInfo
* Description		: Handle Receive Role Info settings
* Input				: receive packet
* Return			: void
*******************************************************************************/
void PacketReceiveRoleInfo(uint8_t* packet) {
	SettingSetRole(packet[BODY_START]);
	
	SettingSetSlaveSerialMode(packet[BODY_START + 1]);
	
	SettingSetSlaveNumber(packet[BODY_START + 2]);

	SettingSetSlaveList(0, U8ToU16(packet[BODY_START + 3], packet[BODY_START + 4]));
	SettingSetSlaveList(1, U8ToU16(packet[BODY_START + 5], packet[BODY_START + 6]));
	SettingSetSlaveList(2, U8ToU16(packet[BODY_START + 7], packet[BODY_START + 8]));
	SettingSetSlaveList(3, U8ToU16(packet[BODY_START + 9], packet[BODY_START + 10]));
	SettingSetSlaveList(4, U8ToU16(packet[BODY_START + 11], packet[BODY_START + 12]));
	}
}

/*******************************************************************************
* Function Name		: PacketReceiveRecordInfo
* Description		: Handle Receive Record Info settings
* Input				: receive packet
* Return			: void
*******************************************************************************/
void PacketReceiveRecordInfo(uint8_t* packet) {
	SettingSetEnableRecord(packet[BODY_START]);
	SettingSetEnableSynchronize(packet[BODY_START + 1]);
	
	if (packet[BODY_START + 1] == 0)
	{
		SettingSetSlaveSerialMode(MODE_DISABLE);
	}
	
	SettingSetMeasureInterval(U8ToU16(packet[BODY_START + 2], packet[BODY_START + 3]));
	SettingSetEnableFastRead(packet[BODY_START + 4]);
	
	if (packet[BODY_START + 4] == 1) {
		SampleReadEnable();
	}
	
	for (uint8_t i = 0; i < 8; i++) {
		SettingSetSampleChange(i, BytesToFloat(packet[BODY_START + 5 + 4 * i], packet[BODY_START + 6 + 4 * i],
		packet[BODY_START + 7 + 4 * i], packet[BODY_START + 8 + 4 * i]));
	}
}

/*******************************************************************************
* Function Name		: PacketReceiveSensorInfo
* Description		: Handle Receive Sensor Info settings
* Input				: receive packet
* Return			: void
*******************************************************************************/
void PacketReceiveSensorInfo(uint8_t* packet) {
	uint8_t chan = packet[BODY_START];
	SettingSetSensorType(chan, packet[BODY_START + 1]);
	SettingSetSensorUnit(chan, U8ToU32(packet[BODY_START + 2], packet[BODY_START + 3],
	packet[BODY_START + 4], packet[BODY_START + 5]));
	SettingSetSensorSettle(chan, U8ToU16(packet[BODY_START + 6], packet[BODY_START + 7]));
	SettingSetCycles(chan, U8ToU16(packet[BODY_START + 8], packet[BODY_START + 9]));
	SettingSetExciteStart(chan, U8ToU16(packet[BODY_START + 10], packet[BODY_START + 11]));
	SettingSetExciteStop(chan, U8ToU16(packet[BODY_START + 12], packet[BODY_START + 13]));
	SettingSetSensorEquation(chan, packet[BODY_START + 14]);
	
	for (uint8_t i = 0; i < 5; i++) {
		SettingSetCoefficient(chan, i, BytesToFloat(packet[BODY_START + 15 + i * 4], packet[BODY_START + 16 + i * 4],
		packet[BODY_START + 17 + i * 4], packet[BODY_START + 18 + i * 4]));
	}
	
	if (packet[BODY_START + 35] == 0) {
		SettingSetCompensated(chan, 0);
	} else {
		SettingSetCompensated(chan, 1);
		SettingSetCompensation(chan, packet[BODY_START + 35]);
	}
	
	if (packet[BODY_START + 36]) {
		SettingSetCounterReset(SettingGetCounterReset() | (1 << chan));
	} else {
		SettingSetCounterReset(SettingGetCounterReset() & ~(1 << chan));
	}
	
	SettingSetSampleChange(chan, BytesToFloat(packet[BODY_START + 37], packet[BODY_START + 38],
	packet[BODY_START + 39], packet[BODY_START + 40]));
}


/*******************************************************************************
* Function Name		: PacketReceiveFor
* Description		: Handle Receive For packet
* Input				: receive packet size
* Return			: void
*******************************************************************************/
void PacketReceiveFor(uint8_t* packet, uint8_t size) {
	if (size >= 7) {
		for (uint8_t i = 5; i < size; i++) {
			next_reading_logger[i - 5] = packet[i];
		}
		forward_size = size - 5;
	}
	
	if (SettingGetEnableSynchronize()) {
		SampleReadEnable();
	}
}

/*******************************************************************************
* Function Name		: PacketSendFOR
* Description		: Send for package
* Input				: void
* Return			: void
*******************************************************************************/
void PacketSendFOR(void) {
	if (SettingGetRole() == ROLE_MASTER && SettingGetSlaveList(0) == 0) {
		return;
	}
	if (SettingGetRole() == ROLE_SLAVE && forward_size <= 0) {
		return;
	}
	
	uint8_t* package;
	uint8_t size = 0;
	package = malloc(sizeof(uint8_t) * 20);
	
	package[size++] = START_BYTE;
	package[size++] = START_BYTE;
	if (SettingGetRole() == ROLE_MASTER) {
		package[size++] = U16ToU8(SettingGetSlaveList(0))[0];
		package[size++] = U16ToU8(SettingGetSlaveList(0))[1];
	} else {
		package[size++] = next_reading_logger[0];
		package[size++] = next_reading_logger[1];
	}
	package[size++] = U16ToU8(SettingsGetSerial())[0];
	package[size++] = U16ToU8(SettingsGetSerial())[1];
	package[size++] = FOR;
	
	uint8_t index = 1;
	if (SettingGetRole() == ROLE_MASTER) {
		while (SettingGetSlaveList(index) != 0 && index < 5) {
			package[size++] = U16ToU8(SettingGetSlaveList(index))[0];
			package[size++] = U16ToU8(SettingGetSlaveList(index))[1];
			index += 1;
		}
	} else {
		for (uint8_t i = 0; i < forward_size - 2; i++) {
			package[size++] = next_reading_logger[2 + i];
		}
	}
	package[size++] = STOP_BYTE;
	package[size++] = STOP_BYTE;
	
	if (SettingGetRole() == ROLE_MASTER) {
		if (SettingGetSlaveSerialMode() == MODE_RS485) {
			RS485UartTXStart();
			RS485UartSendUint8Array(package, size);
			RS485UartTXStop();
		} else {
			RadioSendUint8Array(package, size);
		}
	} else {
		if (SettingGetSerialMode() == MODE_RS485) {
			RS485UartTXStart();
			RS485UartSendUint8Array(package, size);
			RS485UartTXStop();
		} else {
			RadioSendUint8Array(package, size);
		}
	}
	
	free(package);
	
	forward_size = 0;
}

/*******************************************************************************
* Function Name		: PacketSendFOR
* Description		: Send Radio info package
* Input				: void
* Return			: void
*******************************************************************************/
void PacketSendRadioInfo(void) {
	uint8_t hp;
	
	uint8_t* id;
	id = malloc(sizeof(uint8_t) * 4);
	RadioGetHPID(&hp, &id);
	
	uint8_t* package;
	uint8_t size = 6;
	package = malloc(sizeof(uint8_t) * 80);

	package[size++] = DAT;
	package[size++] = hp;
	package[size++] = id[0];
	package[size++] = id[1];
	package[size++] = id[2];
	package[size++] = id[3];
	
	UartSendPacket(package, size);
	
	free(package);
}

/*******************************************************************************
* Function Name		: PacketSendUpload
* Description		: Send Upload package
* Input				: void
* Return			: void
*******************************************************************************/
void PacketSendUpload(void) {
	if (SettingGetRole() == ROLE_SLAVE) {
		uint8_t* package;
		uint8_t size = 6;
		package = malloc(sizeof(uint8_t) * 80);

		package[size++] = UPLOAD;
		
		UartSendPacket(package, size);
		
		// Delay 500ms to let master switch to send mode
		_delay_ms(500);
	}
}

/*******************************************************************************
* Function Name		: PacketSendSLST
* Description		: Send the slave list info
* Input				: void
* Return			: void
*******************************************************************************/
void PacketSendSLST(void) {
	uint8_t* package;
	uint8_t size = 6;
	package = malloc(sizeof(uint8_t) * 80);
	if (SettingGetRole() == ROLE_MASTER) {
		uint16_t s0 = SettingGetSlaveList(0);
		uint16_t s1 = SettingGetSlaveList(1);
		uint16_t s2 = SettingGetSlaveList(2);
		uint16_t s3 = SettingGetSlaveList(3);
		uint16_t s4 = SettingGetSlaveList(4);
		
		package[size++] = DAT;
		package[size++] = U16ToU8(s0)[0];
		package[size++] = U16ToU8(s0)[1];
		package[size++] = U16ToU8(s1)[0];
		package[size++] = U16ToU8(s1)[1];
		package[size++] = U16ToU8(s2)[0];
		package[size++] = U16ToU8(s2)[1];
		package[size++] = U16ToU8(s3)[0];
		package[size++] = U16ToU8(s3)[1];
		package[size++] = U16ToU8(s4)[0];
		package[size++] = U16ToU8(s4)[1];
		
		UartSendPacket(package, size);
		
		free(package);
		} else {
		package[size++] = DAT;
		package[size++] = SettingGetSlaveNumber();
		
		UartSendPacket(package, size);

		free(package);
	}
	
}

/*******************************************************************************
* Function Name		: PacketSendROLE
* Description		: Send the Role Info
* Input				: void
* Return			: void
*******************************************************************************/
void PacketSendROLE(void) {
	uint8_t* package;
	uint8_t size = 6;
	package = malloc(sizeof(uint8_t) * 80);
	
	package[size++] = DAT;
	package[size++] = SettingGetRole();
	package[size++] = SettingGetSlaveNumber();
	package[size++] = SettingGetSlaveSerialMode();
	for (uint8_t i = 0; i < 5; i++) {
		package[size++] = U16ToU8(SettingGetSlaveList(i))[0];
		package[size++] = U16ToU8(SettingGetSlaveList(i))[1];
	}
	
	UartSendPacket(package, size);
	
	free(package);
}

/*******************************************************************************
* Function Name		: PacketSendRecordInfo
* Description		: Send the Record Info
* Input				: void
* Return			: void
*******************************************************************************/
void PacketSendRecordInfo(void) {
	uint8_t* package;
	uint8_t size = 6;
	package = malloc(sizeof(uint8_t) * 80);
	uint16_t interval = SettingGetMeasureInterval();
	
	package[size++] = DAT;
	package[size++] = SettingGetEnableRecord();
	package[size++] = SettingGetEnableSynchronize();
	package[size++] = U16ToU8(interval)[0];
	package[size++] = U16ToU8(interval)[1];
	package[size++] = SettingGetEnableFastRead();
	
	UartSendPacket(package, size);
	
	free(package);
}

/*******************************************************************************
* Function Name		: PacketSendSensorInfo
* Description		: Send the sensor info package
* Input				: channel which send
* Return			: void
*******************************************************************************/
void PacketSendSensorInfo(uint8_t channel) {
	uint8_t* package;
	uint8_t size = 6;
	uint16_t settle = SettingGetSensorSettle(channel);
	uint16_t excite_start = SettingGetExciteStart(channel);
	uint16_t excite_stop = SettingGetExciteStop(channel);
	uint16_t cycles = SettingGetCycles(channel);
	float record_change = SettingGetSampleChange(channel);
	uint32_t unit = SettingGetSensorUnit(channel);
	package = malloc(sizeof(uint8_t) * 80);
	
	package[size++] = DAT;
	package[size++] = SettingGetSensorType(channel);
	package[size++] = U16ToU8(settle)[0];
	package[size++] = U16ToU8(settle)[1];
	package[size++] = U16ToU8(excite_start)[0];
	package[size++] = U16ToU8(excite_start)[1];
	package[size++] = U16ToU8(excite_stop)[0];
	package[size++] = U16ToU8(excite_stop)[1];
	package[size++] = U16ToU8(cycles)[0];
	package[size++] = U16ToU8(cycles)[1];
	package[size++] = SettingGetSensorEquation(channel);
	for (uint8_t i = 0; i < 5; i ++) {
		uint8_t* f = FloatToBytes(SettingGetCoefficient(channel, i));
		package[size++] = f[0];
		package[size++] = f[1];
		package[size++] = f[2];
		package[size++] = f[3];
	}
	
	if (SettingGetCompensation(channel)) {
		package[size++] = SettingGetCompensated(channel) + 1;
		} else {
		package[size++] = 0;
	}
	package[size++] = FloatToBytes(record_change)[0];
	package[size++] = FloatToBytes(record_change)[1];
	package[size++] = FloatToBytes(record_change)[2];
	package[size++] = FloatToBytes(record_change)[3];
	package[size++] = SettingGetCounterReset() & (1 << channel);
	package[size++] = U32ToU8(unit)[0];
	package[size++] = U32ToU8(unit)[1];
	package[size++] = U32ToU8(unit)[2];
	package[size++] = U32ToU8(unit)[3];
	
	UartSendPacket(package, size);
	
	free(package);
}

/*******************************************************************************
* Function Name		: PacketSendRTMData
* Description		: Send Real Time Data Packet
* Input				: channel and data
* Return			: void
*******************************************************************************/
void PacketSendRTMData(uint8_t channel, float data) {
	uint8_t* package;
	uint8_t size = 0;
	uint8_t* bytes = FloatToBytes(data);
	package = malloc(sizeof(uint8_t) * 14);
	
	package[size++] = START_BYTE;
	package[size++] = START_BYTE;
	package[size++] = 0;
	package[size++] = 0;
	package[size++] = U16ToU8(SettingsGetSerial())[0];
	package[size++] = U16ToU8(SettingsGetSerial())[1];
	package[size++] = DAT;
	package[size++] = channel;
	package[size++] = bytes[0];
	package[size++] = bytes[1];
	package[size++] = bytes[2];
	package[size++] = bytes[3];
	package[size++] = STOP_BYTE;
	package[size++] = STOP_BYTE;
	
	switch (SettingGetSerialMode()) {
		case MODE_USB:
			USBUartSendUin8Array(package, size);
			break;
		case MODE_RS232:
			RS232UartSendUint8Array(package, size);
			break;
		case MODE_RS485:
			RS485UartSendUint8Array(package, size);
			break;
		case MODE_RADIO:
			RadioSendUint8Array(package, size);
			break;
	}
	
	free(package);
}

/*******************************************************************************
* Function Name		: PacketSendACKSYN
* Description		: Send syn package
* Input				: void
* Return			: void
*******************************************************************************/
void PacketSendACKSYN(void) {
	uint8_t* package;
	package = malloc(sizeof(uint8_t) * 54);
	
	uint16_t fwv = SettingsGetFWversion();
	DateTime_t last_set = SettingGetLastSetting();
	DateTime_t build_date = SettingsGetBuildDate();
	DateTime_t check_point = SettingGetCheck();
	
	package[6] = ACK_SYN;
	package[7] = SettingsGetSerial() & 0x00ff;
	package[8] = SettingsGetSerial() >> 8 & 0x00ff;
	package[9] = fwv & 0x00ff;
	package[10] = fwv >> 8 & 0x00ff;
	datetime_to_bytes(last_set, package, 11);
	datetime_to_bytes(build_date, package, 18);
	datetime_to_bytes(check_point, package, 25);
	for (uint8_t i = 0; i < 8; i ++) {
		package[32 + i] = SettingGetSensorType(i);
	}
	package[40] = SettingGetRole();
	for (uint8_t i = 0; i < 5; i ++) {
		package[41 + i * 2] = U16ToU8(SettingGetSlaveList(i))[0];
		package[42 + i * 2] = U16ToU8(SettingGetSlaveList(i))[1];
	}
	package[51] = SettingGetSlaveNumber();
	
	UartSendPacket(package, 54);
	
	free(package);
}