/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: rs232uart.c
 * Description: File for RS232 Uart communication
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include "rs232uart.h"
#include "../Serial/usbuart.h"

// variables
uint8_t RS232_RxBuffer[RS232_INPUT_BUFFER_SIZE];
uint8_t RS232_RxReadPosition = 0;
uint8_t RS232_RxWritePosition = 0;
uint8_t rs232_is_next_buffer;

void RS232UartInit(void) {
	rs232_is_next_buffer = 0;
	RS232UART_SETBAUD();
	RS232UART_HIGH_END();

	RS232UART_RX_ENABLE();
	RS232UART_TX_ENABLE();
	RS232UART_RX_INT_ON();
	
	RS232UART_TX_INT_OFF();
	RS232UART_DRIVER_ON();
	
	RS232UART_PARITY_NONE();
	RS232UART_ONE_STOP_BITS();
	// RS232UART_DRIVER_ON();
}

/*******************************************************************************
* Function Name		: rs232uart_sendbyte
* Description		: Send one byte to RS232
* Input				: data byte
* Return			: void
*******************************************************************************/
void RS232UartSendByte(uint8_t data) {
	while (RS232UART_DATA_REG_EMPTY() == 0);
	RS232UART_DATA_REG() = data;
}

/*******************************************************************************
* Function Name		: rs232uart_senduin8array
* Description		: Send uint8_t* for certain size
* Input				: data byte array
*					: size size of array
* Return			: void
*******************************************************************************/
void RS232UartSendUint8Array(uint8_t* data ,unsigned int size) {
	for (unsigned int i = 0; i < size; i++) {
		RS232UartSendByte(data[i]);
	}
}

/*******************************************************************************
* Function Name		: rs232uart_sendstring
* Description		: Send char* for certain size
* Input				: data byte array
*					: size size of array
* Return			: void
*******************************************************************************/
void RS232UartSendString(char* data ,uint8_t size) {
	for (unsigned int i = 0; i < size; i++) {
		RS232UartSendByte(data[i]);
	}
}

/*******************************************************************************
* Function Name		: rs232uart_ischaravailable
* Description		: is any char in RS232 input buffer
* Input				: void
* Return			: true if does
*******************************************************************************/
uint8_t RS232UartIsCharAvailable(void) {
	if (RS232_RxWritePosition > RS232_RxReadPosition) {
		return 1;
	}
	
	if (rs232_is_next_buffer) {
		return 1;
	}

	return 0;
}

/*******************************************************************************
* Function Name		: rs232uart_readbyte
* Description		: Read a char from in buffer
* Input				: void
* Return			: uint_8 read
*******************************************************************************/
uint8_t RS232UartReadByte(void) {
	uint8_t byte_read;
	byte_read = RS232_RxBuffer[RS232_RxReadPosition];
	RS232_RxReadPosition++;
	if (RS232_RxReadPosition == RS232_INPUT_BUFFER_SIZE) {
		rs232_is_next_buffer = 0;
		RS232_RxReadPosition = 0;
	}
	return byte_read;
}

/*******************************************************************************
* Function Name		: usbuart_clearbuffer
* Description		: Clear the input buffer
* Input				: void
* Return			: void
*******************************************************************************/
void RS232UartClearBuffer(void) {
	RS232_RxReadPosition = RS232_RxWritePosition;
}

/*******************************************************************************
* Function Name		: RS232UART_RX_INT_ISR
* Description		: Interrupt for receive serial data and put into RS232 in buffer
*******************************************************************************/
RS232UART_RX_INT_ISR() {
	uint8_t rData = RS232UART_DATA_REG();
	RS232_RxBuffer[RS232_RxWritePosition] = rData;
	RS232_RxWritePosition++;
	if (RS232_RxWritePosition == RS232_INPUT_BUFFER_SIZE) {
		RS232_RxWritePosition = 0;
		rs232_is_next_buffer = 1;
	}
}