/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: rs485uart.h
 * Description: Header file of rs485uart.c
 */ 


#ifndef RS485UART_H_
#define RS485UART_H_

#define RS485_INPUT_BUFFER_SIZE			2048
#define RS485_OUTPUT_BUFFER_SIZE		80
#define BAUD_RS485						115200

#define RS485UART_BAUDRATE_REGISTER  UBRR0L
#define RS485UART_SETBAUD()          UBRR0L  = (((F_CPU / (BAUD_RS485 * 16UL))) - 1)
#define RS485UART_HIGH_END()		 UBRR0H  = ((((F_CPU / (BAUD_RS485 * 16UL))) - 1) >> 8)
#define RS485UART_TXD_HI()           PORTE  |=  (1<<PE1)
#define RS485UART_TXD_LO()           PORTE  &= ~(1<<PE1)

#define RS485UART_DATA_REG_EMPTY()   (UCSR0A &   (1<<UDRE0))

#define RS485UART_DATA_REG()         UDR0

#define RS485UART_RX_INT_ON()        UCSR0B |=  (_BV(RXCIE0))
#define RS485UART_RX_INT_OFF()       UCSR0B &= ~(_BV(RXCIE0))
#define RS485UART_RX_INT_CLR()       UCSR0A |=  (1<<RXC0)
#define RS485UART_RX_INT_ISR()       ISR(USART0_RX_vect)

#define RS485UART_TX_INT_ON()        UCSR0B |=  (_BV(TXCIE0))
#define RS485UART_TX_INT_OFF()       UCSR0B &= ~(_BV(TXCIE0))
#define RS485UART_TX_INT_CLR()       UCSR0A |=  (1<<TXC0)
#define RS485UART_TX_INT_ISR()       ISR(USART0_TX_vect)

#define RS485UART_RX_ENABLE()        UCSR0B |=  (1<<RXEN0)
#define RS485UART_RX_DISABLE()       UCSR0B &= ~(1<<RXEN0)
#define RS485UART_TX_ENABLE()        UCSR0B |=  (1<<TXEN0)
#define RS485UART_TX_DISABLE()       UCSR0B &= ~(1<<TXEN0)

#define RS485UART_DE_ON()            PORTH  |=  (1<<PH7)
#define RS485UART_DE_OFF()           PORTH  &= ~(1<<PH7)
#define RS485UART_RE_ON()            PORTG  &= ~(1<<PG3)
#define RS485UART_RE_OFF()           PORTG  |=  (1<<PG3)

#define RS485LINK_WAKE_INT_MSK_SET() PCMSK1 |=  (_BV(PCINT8))
#define RS485LINK_WAKE_INT_MSK_CLR() PCMSK1 &= ~(_BV(PCINT8))

#define RS485UART_PARITY_NONE()      UCSR0C &= ~((_BV(UPM01)) | (_BV(UPM00)))
#define RS485UART_PARITY_EVEN()      UCSR0C  =  ((_BV(UPM01)) | (_BV(UCSZ01)) | (_BV(UCSZ00)))
#define RS485UART_PARITY_ODD()       UCSR0C |=  ((_BV(UPM01)) | (_BV(UPM00)))
#define RS485UART_TWO_STOP_BITS()    UCSR0C |=  (_BV(USBS0))
#define RS485UART_ONE_STOP_BITS()    UCSR0C &= ~(_BV(USBS0))

#define RS485UART_STATUS()           UCSR0A
#define RS485UART_PARITY_ERROR_BIT   _BV(UPE0)

#define RS485_INT                    _BV(PE0)

#define RTU485_TIMER_COUNT()         TCNT5
#define RTU485_TIMER_CONTROL_B()     TCCR5B
#define RTU485_TIMER_OUT_COMP_A()    OCR5A
#define RTU485_TIMER_OUT_COMP_B()    OCR5B
#define RTU485_TIMER_INT_MASK_REG()  TIMSK5
#define RTU485_TIMER_INT_FLAG_REG()  TIFR5

#define RTU485_T15_MASK()            1<<OCIE5A
#define RTU485_T35_MASK()            1<<OCIE5B
#define RTU485_T15_FLAG_CLR()        1<<OCF5A
#define RTU485_T35_FLAG_CLR()        1<<OCF5B
#define RTU485_PRESCALE()            TCCR5B = (1<<CS51) | (1<<CS50)
#define RTU485_PRESCALE_VALUE        64

void RS485UartInit(void);

void RS485UartTXStart(void);

void RS485UartTXStop(void);

void RS485UartSendByte(uint8_t data);

uint8_t RS485UartReadByte(void);

void RS485UartSendUint8Array(uint8_t* data ,unsigned int size);

uint8_t RS485UartIsCharAvailable(void);

void RS485UartClearBuffer(void);

#endif /* RS485UART_H_ */