/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: serial_packet.c
 * Description: Handle for serial packages
 */ 


#include <avr/io.h>
#include <stdlib.h>
#include <util/delay.h>
#include "../../common/util.h"
#include "../../common/state.h"
#include "../Display/menu.h"
#include "../Setting/setting.h"
#include "../RTC/DS3232.h"
#include "../Log/log.h"
#include "../Read/sample.h"
#include "../../process.h"
#include "../../main.h"
#include "rs232uart.h"
#include "rs485uart.h"
#include "radio.h"
#include "serial_packet.h"
#include "packetHandler.h"
#include "usbuart.h"
#include "uart.h"

static unsigned char serial_status;
static uint32_t timer_tick;
static uint8_t second_stop;
static uint8_t pre_byte;
static uint8_t forwawrd_read;

uint8_t write_pos;
uint8_t packet_size;
uint8_t current_packet[USB_INPUT_BUFFER_SIZE];

static void RunWait(void);
static void RunWait2(void);
static void RunReading(void);
static void RunFinish(void);
static void RunCheck(void);
static void RunForward(void);
static void RunSend(void);

static void ReceiveSET(void);
static void ReceiveDAT(void);

void SerialInit(void) {
	write_pos = 0;
	packet_size = 0;
	serial_status = WAIT;
	timer_tick = 0;
	second_stop = 0;
	forwawrd_read = 0;
}

/*******************************************************************************
* Function Name		: serial_walk
* Description		: Walk for serial communication for main process
*					: WAIT wait for coming packet
*					: READING wait for reading coming packet
*					: FINISH finish reading packet
* Input				: void
* Return			: void
*******************************************************************************/
void SerialWalk(void) {
	switch (serial_status) {
		case WAIT:
			RunWait();
			break;
		case WAIT2:
			RunWait2();	
			break;
		case CHECK:
			RunCheck();
			break;
		case FORWARD:
			RunForward();
			break;
		case SEND:
			RunSend();
			break;
		case READING:
			RunReading();
			break;
		case FINISH:
			RunFinish();
			break;
	}
}

void SerialRun(void) {
	if (serial_status == WAIT2) {
		if ((--timer_tick) == 0) {
			serial_status = WAIT;
		}
	}
}

uint8_t GetSerialState(void) {
	return serial_status;
}


/*******************************************************************************
* Function Name		: RunWait
* Description		: Wait until first Start Byte
* Input				: void
* Return			: void
*******************************************************************************/
static void RunWait(void) {
	if (UartMasterIsNextCharAvailable(START_BYTE))
	{
		serial_status = WAIT2;
		timer_tick = 500;
		return;
	}
	
	if (UartIsNextCharAvailable(START_BYTE))
	{
		serial_status = WAIT2;
		timer_tick = 500;
	}
}

/*******************************************************************************
* Function Name		: RunWait2
* Description		: Wait until second Start Byte
* Input				: void
* Return			: void
*******************************************************************************/
static void RunWait2(void) {
	if (UartMasterIsNextCharAvailable(START_BYTE)) {
		serial_status = READING;
		forwawrd_read = 1;
		return;
	}
	
	if (UartIsNextCharAvailable(START_BYTE)) {
		serial_status = READING;
	}
}

/*******************************************************************************
* Function Name		: RunCheck
* Description		: Check if the coming message is for this logger
* Input				: void
* Return			: void
*******************************************************************************/
static void RunCheck() {
	if (SettingGetSerialMode() == MODE_USB) {
		serial_status = FINISH;
		return;
	}
	
	if (U8ToU16(current_packet[0], current_packet[1]) == SettingsGetSerial()) {
		serial_status = FINISH;
	} else {
		if (SettingGetRole() == ROLE_MASTER) {
			// If send to PC
			if (U8ToU16(current_packet[0], current_packet[1]) == 0) {
				serial_status = SEND;
				return;
			}
			
			// If send from PC
			if (U8ToU16(current_packet[2], current_packet[3]) == 0) {
				if (SettingGetSerialMode() == MODE_RS232)
				{
					serial_status = FORWARD;
					return;
				}
			}
			
			// Skip For message
			if (current_packet[FLAG_1] == FOR) {
				serial_status = WAIT;
				return;
			}
			
			serial_status = WAIT;
		} else {
			serial_status = WAIT;
		}
	}
}

/*******************************************************************************
* Function Name		: run_forward
* Description		: Forward the message from 485 to 232, only for master
* Input				: void
* Return			: void
*******************************************************************************/
static void RunForward() {
	switch (SettingGetSlaveSerialMode()) {
		case MODE_RS485:
			RS485UartTXStart();
			RS485UartSendByte(START_BYTE);
			RS485UartSendByte(START_BYTE);
			RS485UartSendUint8Array(current_packet, packet_size);
			RS485UartSendByte(STOP_BYTE);
			RS485UartSendByte(STOP_BYTE);
			RS485UartTXStop();
			break;
		case MODE_RADIO:
			RadioSendByte(START_BYTE);
			RadioSendByte(START_BYTE);
			RadioSendUint8Array(current_packet, packet_size);
			RadioSendByte(STOP_BYTE);
			RadioSendByte(STOP_BYTE);
			break;
	}
	serial_status = WAIT;
}

static void RunSend() {
	if (current_packet[FLAG_1] != UPLOAD) {
		RS232UartSendByte(START_BYTE);
		RS232UartSendByte(START_BYTE);
		RS232UartSendUint8Array(current_packet, packet_size);
		RS232UartSendByte(STOP_BYTE);
		RS232UartSendByte(STOP_BYTE);
			
		serial_status = WAIT;
	} else {
		uint16_t time_out = 0;
		
		SetTickEnable(0);
		
		switch (SettingGetSlaveSerialMode()) {
			case MODE_RS485:
				while (1) {
					if (RS485UartIsCharAvailable()) {
						RS232UartSendByte(RS485UartReadByte());
						time_out = 0;
					} else {
						time_out++;
						_delay_ms(1);
					}
					
					if (time_out > 3000) {
						serial_status = WAIT;
						break;
					}
				}
				break;
			case MODE_RADIO:
				while (1) {
					if (RadioIsCharAvailable()) {
						RS232UartSendByte(RadioReadByte());
						time_out = 0;
					} else {
						time_out++;
						_delay_ms(1);
					}
					
					if (time_out > 3000) {
						serial_status = WAIT;
						break;
					}
				}
				break;
				
		}
		SetTickEnable(1);
		
	}
}

/*******************************************************************************
* Function Name		: run_reading
* Description		: Read a complete packet from in buffer
* Input				: void
* Return			: void
*******************************************************************************/
static void RunReading(void) {
	uint8_t current_byte;
	
	if (forwawrd_read) {
		switch (SettingGetSlaveSerialMode()) {
			case MODE_RS485:
				while (RS485UartIsCharAvailable()) {
					current_byte = RS485UartReadByte();
					if (current_byte != STOP_BYTE) {
						if (second_stop) {
							current_packet[write_pos] = pre_byte;
							write_pos++;
							second_stop = 0;
						}
						current_packet[write_pos] = current_byte;
						write_pos++;
					} else {
						if (second_stop) {
							packet_size = write_pos;
							write_pos = 0;
							second_stop = 0;
							forwawrd_read = 0;
							serial_status = CHECK;
							break;
						} else {
							second_stop = 1;
							pre_byte = current_byte;
						}
					}
				}
				break;
			case MODE_RADIO:
				while (RadioIsCharAvailable()) {
					current_byte = RadioReadByte();
					if (current_byte != STOP_BYTE) {
						if (second_stop) {
							current_packet[write_pos] = pre_byte;
							write_pos++;
							second_stop = 0;
						}
						current_packet[write_pos] = current_byte;
						write_pos++;
					} else {
						if (second_stop) {
							packet_size = write_pos;
							write_pos = 0;
							second_stop = 0;
							forwawrd_read = 0;
							serial_status = CHECK;
							break;
						} else {
							second_stop = 1;
							pre_byte = current_byte;
						}
					}
				}
				break;
		}
		return;
	}
	
	switch(UartGetSerialMode()) {
		case MODE_USB:
			while (USBUartIsCharAvailable()) {
				current_byte = USBUartReadByte();
				if (current_byte != STOP_BYTE) {
					if (second_stop) {
						current_packet[write_pos] = pre_byte;
						write_pos++;
						second_stop = 0;
					}
					current_packet[write_pos] = current_byte;
					write_pos++;
				} else {
					if (second_stop) {
						packet_size = write_pos;
						write_pos = 0;
						second_stop = 0;
						serial_status = CHECK;
						break;
					} else {
						second_stop = 1;
						pre_byte = current_byte;
					}
				}
			}
			break;
		case MODE_RS232:
			while (RS232UartIsCharAvailable()) {
				current_byte = RS232UartReadByte();
				if (current_byte != STOP_BYTE) {
					if (second_stop) {
						current_packet[write_pos] = pre_byte;
						write_pos++;
						second_stop = 0;
					}
					current_packet[write_pos] = current_byte;
					write_pos++;
				} else {
					if (second_stop) {
						packet_size = write_pos;
						write_pos = 0;
						second_stop = 0;
						serial_status = CHECK;
						break;
					} else {
						second_stop = 1;
						pre_byte = current_byte;
					}
				}
			}
			break;
		case MODE_RS485:
			while (RS485UartIsCharAvailable()) {
				current_byte = RS485UartReadByte();
				if (current_byte != STOP_BYTE) {
					if (second_stop) {
						current_packet[write_pos] = pre_byte;
						write_pos++;
						second_stop = 0;
					}
					current_packet[write_pos] = current_byte;
					write_pos++;
				} else {
					if (second_stop) {
						packet_size = write_pos;
						write_pos = 0;
						second_stop = 0;
						serial_status = CHECK;
						_delay_ms(100);
						break;
					} else {
						second_stop = 1;
						pre_byte = current_byte;
					}
				}
			}
			break;
		case MODE_RADIO:
			while (RadioIsCharAvailable()) {
				current_byte = RadioReadByte();
				if (current_byte != STOP_BYTE) {
					if (second_stop) {
						current_packet[write_pos] = pre_byte;
						write_pos++;
						second_stop = 0;
					}
					current_packet[write_pos] = current_byte;
					write_pos++;
				} else {
					if (second_stop) {
						packet_size = write_pos;
						write_pos = 0;
						second_stop = 0;
						serial_status = CHECK;
						break;
					} else {
						second_stop = 1;
						pre_byte = current_byte;
					}
				}
			}
			break;
	}
}

/*******************************************************************************
* Function Name		: run_finish
* Description		: reading is done, process packet now
* Input				: void
* Return			: void
*******************************************************************************/
static void RunFinish(void) {
	uint8_t flag = current_packet[FLAG_1];

	switch (flag) {
		case SYN:
			PacketSendACKSYN();
			break;
		case ACK_SYN:
			break;
		case SET:
			ReceiveSET();
			break;
		case DAT:
			ReceiveDAT();
			break;
		case FOR:
			PacketReceiveFor(current_packet, packet_size);
			break;
	}
	serial_status = WAIT;
}

/*******************************************************************************
* Function Name		: receive_SET
* Description		: Handle after receive settings
* Input				: void
* Return			: void
*******************************************************************************/
static void ReceiveSET() {
	uint8_t set_flag = current_packet[FLAG_2];

	switch (set_flag) {
		case SER:
			SettingsSetSerial(U8ToU16(current_packet[BODY_START], current_packet[BODY_START + 1]));
			break;
		case FWV:
			SettingsSetFWversion(U8ToU16(current_packet[BODY_START], current_packet[BODY_START + 1]));
			break;
		case RTC:
			RTC_Set(bytes_to_datetime(current_packet, BODY_START));
			break;
		case BUILD_DATE:
			SettingsSetBuildDate(bytes_to_datetime(current_packet, BODY_START));
			break;
		case CHECK_DATE:
			SettingSetCheck(bytes_to_datetime(current_packet, BODY_START));
			break;
		case SENSOR_TYPE:
			SettingSetSensorType(current_packet[BODY_START], current_packet[BODY_START + 1]);
			// Reset channel on set to type 0
			if (current_packet[BODY_START + 1] == 0)
			{
				SettingSetSensorUnit(current_packet[BODY_START], U8ToU32(' ', ' ', ' ', ' '));
				SettingSetCycles(current_packet[BODY_START], 250);
				SettingSetSensorSettle(current_packet[BODY_START], 40);
				SettingSetExciteStart(current_packet[BODY_START], 1600);
				SettingSetExciteStop(current_packet[BODY_START], 3200);
				SettingSetSensorEquation(current_packet[BODY_START], 0);
				SettingSetCoefficient(current_packet[BODY_START], 0, 0);
				SettingSetCoefficient(current_packet[BODY_START], 1, 0);
				SettingSetCoefficient(current_packet[BODY_START], 2, 0);
				SettingSetCoefficient(current_packet[BODY_START], 3, 0);
				SettingSetCoefficient(current_packet[BODY_START], 4, 0);
				SettingSetCompensated(current_packet[BODY_START], 0);
				SettingSetCompensation(current_packet[BODY_START], 0);
				SettingSetSampleChange(current_packet[BODY_START], 0);
				SettingSetCounterReset(SettingGetCounterReset() & ~(1 << current_packet[BODY_START]));
			}
			UploadRead();
			break;
		case SENESOR_SETTLE:
			SettingSetSensorSettle(current_packet[BODY_START], U8ToU16(current_packet[BODY_START + 1], current_packet[BODY_START + 2]));
			break;
		case SENSOR_START:
			SettingSetExciteStart(current_packet[BODY_START], U8ToU16(current_packet[BODY_START + 1], current_packet[BODY_START + 2]));
			break;
		case SENSOR_STOP:
			SettingSetExciteStop(current_packet[BODY_START], U8ToU16(current_packet[BODY_START + 1], current_packet[BODY_START + 2]));
			break;
		case SENSOR_EQUATION:
			SettingSetSensorEquation(current_packet[BODY_START], current_packet[BODY_START + 1]);
			break;
		case SENSOR_COF:
			SettingSetCoefficient(current_packet[BODY_START], current_packet[BODY_START + 1],
			BytesToFloat(current_packet[BODY_START + 2], current_packet[BODY_START + 3], 
			current_packet[BODY_START + 4], current_packet[BODY_START + 5]));
			break;
		case SENSOR_CYCLES:
			SettingSetCycles(current_packet[BODY_START], U8ToU16(current_packet[BODY_START + 1], current_packet[BODY_START + 2]));
			break;
		case SENSOR_INTERVAL:
			SettingSetMeasureInterval(U8ToU16(current_packet[BODY_START], current_packet[BODY_START +1]));
			break;
		case SENSOR_COMP:
			SettingSetCompensated(current_packet[BODY_START], current_packet[BODY_START +1]);
			break;
		case SENSOR_COMPCHAN:
			SettingSetCompensation(current_packet[BODY_START], current_packet[BODY_START +1]);
			break;
		case RESET_SENSOR:
			ResetReadSetting();
			break;
		case RESET_SD:
			LogReset();
			break;
		case MODE:
			UartSetSerialMode(current_packet[BODY_START]);
			break;
		case ROLE:
			UartSetRole(current_packet[BODY_START]);
			break;
		case COUNTER_RESET:
			if (current_packet[5])
			{
				SettingSetCounterReset(SettingGetCounterReset() | (1 << current_packet[BODY_START]));
			}
			else
			{
				SettingSetCounterReset(SettingGetCounterReset() & ~(1 << current_packet[BODY_START]));
			}
			break;
		case SAMPLE_CHANGE:
			SettingSetSampleChange(current_packet[BODY_START],
			BytesToFloat(current_packet[BODY_START + 1], current_packet[BODY_START + 2], 
			current_packet[BODY_START + 3], current_packet[BODY_START + 4]));
			break;
		case SENSOR_UNIT:
			SettingSetSensorUnit(current_packet[BODY_START], U8ToU32(current_packet[BODY_START + 1],
			current_packet[BODY_START + 2], current_packet[BODY_START + 3], current_packet[BODY_START + 4]));
			break;
		case SLAVE_LIST:
			SettingSetSlaveList(current_packet[BODY_START], U8ToU16(current_packet[BODY_START + 1], current_packet[BODY_START + 2]));
			break;
		case SLAVE_NUMBER:
			SettingSetSlaveNumber(current_packet[BODY_START]);
			break;
		case SENSOR:
			PacketReceiveSensorInfo(current_packet);
			break;
		case RECORD:
			PacketReceiveRecordInfo(current_packet);
			break;
		case ENABLE_RECORD:
			SettingSetEnableRecord(current_packet[BODY_START]);
			break;
		case ROLE_INFO_SET:
			PacketReceiveRoleInfo(current_packet);
			break;
		case SYSTEM_INFO_SET:
			PacketReceiveSystemInfo(current_packet);
			break;
		case RADIO_HPID:
			RadioSetHPID(current_packet[BODY_START], current_packet[BODY_START + 1],
			current_packet[BODY_START + 2], current_packet[BODY_START + 3], current_packet[BODY_START + 4]);
			break;
		case ENABLE_SYNCHRONIZE:
			SettingSetEnableSynchronize(current_packet[BODY_START]);
			break;
		case ENABLE_FASTREAD:
			SettingSetEnableFastRead(current_packet[BODY_START]);
			break;
		
	}
	// Update last setting date after receive SET
	SettingSetLastSetting(RTC_Get());
	
	been_set();
}

/*******************************************************************************
* Function Name		: receive_DAT
* Description		: Handle after receive Dat packet
* Input				: void
* Return			: void
*******************************************************************************/
static void ReceiveDAT() {
	uint8_t dat_flag = current_packet[FLAG_2];
	switch (dat_flag) {
		case RTM:
			SettingSetRTM(current_packet[BODY_START]);
			break;
		case RED:
			PacketSendUpload();
			LogSend(U8ToU16(current_packet[BODY_START], current_packet[BODY_START + 1]), 
			current_packet[BODY_START + 2], current_packet[BODY_START + 3]);
			break;
		case LST:
			LogPage();
			break;
		case SENSOR_INFO:
			PacketSendSensorInfo(current_packet[BODY_START]);
			break;
		case RECORD_INFO:
			PacketSendRecordInfo();
			break;
		case ROLE_INFO_DAT:
			PacketSendROLE();
			break;
		case SLST:
			PacketSendSLST();
			break;
		case RADIO_DAT:
			PacketSendRadioInfo();
			break;
	}
}