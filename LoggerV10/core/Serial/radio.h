/*
 * radio.h
 *
 * Created: 1/05/2020 10:20:30 AM
 *  Author: xiaofan
 */ 


#ifndef RADIO_H_
#define RADIO_H_

#define DMUART_RX_ENABLE()           UCSR3B |=  (1<<RXEN3)
#define DMUART_RX_DISABLE()          UCSR3B &= ~(1<<RXEN3)
#define DMUART_TX_ENABLE()           UCSR3B |=  (1<<TXEN3)
#define DMUART_TX_DISABLE()          UCSR3B &= ~(1<<TXEN3)

#define DMUART_SETBAUD_115200()      UBRR3L  = (F_CPU/(115200UL << 4))-1
#define DMUART_SETBAUD_REG()         UBRR3L
#define DMUART_DISABLE()             UCSR3B  = (0)

#define DMUART_DATA_REG()            UDR3
#define DMUART_DATA_REG_EMPTY()		(UCSR3A &   (1<<UDRE3))

#define DMUART_RXM()                 PINJ   &   (_BV(PJ0))
#define DMUART_RX_INT_ON()           UCSR3B |=  (_BV(RXCIE3))
#define DMUART_RX_INT_OFF()          UCSR3B &= ~(_BV(RXCIE3))
#define DMUART_RX_INT_ISR()          ISR(USART3_RX_vect)

#define DMUART_TXM_HI()              PORTJ  |=  (_BV(PJ1))
#define DMUART_TXM_LO()              PORTJ  &= ~(_BV(PJ1))
#define DMUART_TX_INT_ON()           UCSR3B |=  (_BV(UDRIE3))
#define DMUART_TX_INT_OFF()          UCSR3B &= ~(_BV(UDRIE3))
#define DMUART_TX_INT_ISR()          ISR(USART3_UDRE_vect)

//  #define DMUART_CTS_INT_MSK_SET()  (PCMSK0 |=  (_BV(PCINT7)))
//  #define DMUART_CTS_INT_MSK_CLR()  (PCMSK0 &= ~(_BV(PCINT7)))

#define DMUART_SLEEP_CON_HI()        PORTC  |=  (_BV(PC2))
#define DMUART_SLEEP_CON_LO()        PORTC  &= ~(_BV(PC2))

#define DMLINK_WAKE_INT_MSK_SET()    PCMSK1 |=  (_BV(PCINT9))
#define DMLINK_WAKE_INT_MSK_CLR()    PCMSK1 &= ~(_BV(PCINT9))

#define DMLINK_INT                   _BV(PJ0)

#define DMUART_POWER_ON()		     PORTA  |=  (1<<PA6)    // also called SOCKET
#define DMUART_POWER_OFF()           PORTA  &= ~(1<<PA6)     // also called SOCKET
#define DMUART_RESET_LO()            PORTC  &= ~(_BV(PC1))   // also called SOCKET
#define DMUART_RESET_HI()            PORTC  |=  (_BV(PC1))   // also called SOCKET

#define DMUART_CTS()                 PINA   &   (_BV(PA4))   // also called SOCKET

#define DMUART_RTS_HI()              PORTC  |=  (_BV(PC3))   // also called SOCKET
#define DMUART_RTS_LO()              PORTC  &= ~(_BV(PC3))   // also called SOCKET

#define RADIO_INPUT_BUFFER_SIZE		 2048

void RadioInit();

void RadioSendByte(uint8_t data);

void RadioSendUint8Array(uint8_t* data ,unsigned int size);

uint8_t RadioIsCharAvailable(void);

void RadioClearBuffer(void);

uint8_t RadioReadByte(void);

uint8_t RadioGetHPID(uint8_t* hp, uint8_t** id);

uint8_t RadioSetHPID(uint8_t hp, uint8_t id1, uint8_t id2, uint8_t id3, uint8_t id4);

#endif /* RADIO_H_ */