 /* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: battery.h
 * Description: Header file for battery.c
 */ 


#ifndef BATTERY_H_
#define BATTERY_H_

// CPU ADC
#define CPUADC_MUXBASE              1<<REFS0
#define BATTERY_CHANNEL             0
#define VOLTAGE_COEFFICIENT         20152UL
#define CPUADC_PRESCALE				(1<<ADPS0)

#define CPUADC_START                ((1<<ADSC ) | (1<<ADIF ))
#define CPUADC_ENABLE               ((1<<ADEN ) | (CPUADC_PRESCALE))
#define CPUADC_CONVERT              (CPUADC_ENABLE | CPUADC_START)
#define CPUADC_DISABLE              (0x00)

#define ADC_DATA					360
#define LOSE_RATE					1.0e-5f
#define BATTERY_MEASURE_INTERVAL	5

// Function prototypes
void BatteryInit(void);

void BatteryWalk(void);

void BatterySecondTick(void);

unsigned int  BatteryGetVolts(void);

unsigned int  BatteryGetVoltsRaw(void);

#endif /* BATTERY_H_ */