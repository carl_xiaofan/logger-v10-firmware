 /* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: battery.c
 * Description: File for input power and battery power
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include "battery.h"
#include "../../main.h"

// Variables
static uint8_t state;
static uint8_t reading_required;
static uint8_t timer;
static uint32_t battery_coef;

static float adc_data;

// Function prototypes
static void Idel(void);
static void Wake(void);
static void Busy(void);

static void SetIdle(void);

static unsigned int Read(void);
static uint8_t Ready(void);
static unsigned int Voltage(unsigned long input);

/***************************************************************************  
	Function Name:	BatteryInit
	@ Description:	Initialize the battery setting for measuring the voltage
	@ Parameter:	None
	@ Return:		None                                            
***************************************************************************/
void BatteryInit(void) {
	ADMUX = CPUADC_MUXBASE; // Set AREF - charge cap
	reading_required = TRUE;
	adc_data = ADC_DATA;
	battery_coef = VOLTAGE_COEFFICIENT;
	timer = BATTERY_MEASURE_INTERVAL;
	SetIdle();
}

/***************************************************************************  
	Function Name:	BatterySecondTick
	@ Description:	Count down timer to enable power voltage read
	@ Parameter:	None
	@ Return:		None                                            
***************************************************************************/
void BatterySecondTick(void) {
	if (timer) {
		if (!(--timer)) {
			reading_required = TRUE;
		}
	}
}

/*****************************************************************
	Function name:	BatteryGetVolts
	@ Description:	To obtain the (real) voltage of the battery
	@ Parameter:	None
	@ Return:		unsigned int value - voltage
******************************************************************/
unsigned int BatteryGetVolts(void) {
	return Voltage((int) (adc_data + 0.5));
}

/*****************************************************************
	Function name:	battery_get_volts_raw
	@ Description:	To obtain the raw voltage of the battery
	@ Parameter:	None
	@ Return:		unsigned int value - voltage
******************************************************************/
unsigned int BatteryGetVoltsRaw(void) {
	return (int) (adc_data + 0.5);
}

/*****************************************************************
	Function name:	battery_walk
	@ Description:	Battery class walk
	@ Parameter:	None
	@ Return:		None
******************************************************************/
void BatteryWalk(void) {
	switch (state) {
		case IDLE: Idel(); break;
		case WAKE: Wake(); break;
		case BUSY: Busy(); break;
	}
}

/*****************************************************************
	Function name:	idle
	@ Description:	Wait for require read
	@ Parameter:	None
	@ Return:		None
******************************************************************/
static void Idel(void) {
	if (reading_required) {
		ADMUX = CPUADC_MUXBASE | BATTERY_CHANNEL;
		ADCSRA = CPUADC_CONVERT;
		state = WAKE;
	}
}

/*****************************************************************
	Function name:	wake
	@ Description:	Ready to measure input voltage
	@ Parameter:	None
	@ Return:		None
******************************************************************/
static void Wake(void) {
	if (Ready()) {
		state = BUSY;
		ADCSRA = CPUADC_CONVERT;
		adc_data = ADC_DATA;
	}
}

/*****************************************************************
	Function name:	busy
	@ Description:	Measuring input voltage
	@ Parameter:	None
	@ Return:		None
******************************************************************/
static void Busy(void) {
	unsigned int temp;
	
	if (Ready()) {
		temp = Read();
		adc_data -= (LOSE_RATE * adc_data);
		
		if (temp > adc_data) {
			adc_data = temp;
		}
	}
	reading_required = FALSE;
	timer = BATTERY_MEASURE_INTERVAL;
	SetIdle();
}

/*****************************************************************************************
	Function name: ready
	@ Description:	Check that the logger be ready for reading battery voltage
	@ Parameter:	None
	@ Return:		Zero if it is not ready. Otherwise, returns non-zero for being ready
*****************************************************************************************/
static unsigned char Ready(void) {
	return (ADCSRA & (1 << ADIF));
}

/*****************************************************************************************
	Function: read
	@ Description:	Take the reading from 16-bit register ADCW
	@ Parameter:	None
	@ Return:		Unsigned int value - raw voltage 
*****************************************************************************************/
static unsigned int Read(void) {
	unsigned int temp;
	unsigned int result;
	
	temp = SREG;
	cli();
	result = ADCW;
	SREG = temp;
	return result;
}

/**************************************************************************************
	Function name:	voltage
	@ Description:	Using the coefficient got from the setting to calculate the voltage
	@ Parameter:		input - value of battery reading
	@ Return:			unsigned int - voltage value
***************************************************************************************/
static unsigned int Voltage(unsigned long input) {
	return (battery_coef * input) / 1024; // will be changed after setting_coefficient()
}

/**************************************************************************************
	Function name:	set_idle
	@ Description:	Set the state to Idle
	@ Parameter:	None
	@ Return:		None
***************************************************************************************/
static void SetIdle(void) {
	state = IDLE;
	ADCSRA = CPUADC_DISABLE; // disable A/D
}