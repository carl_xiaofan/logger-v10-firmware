/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: sdhc.c
 * Description: The basic level for read and write into sector of SD card
 * Get this from FATFS
 */ 

#include <avr/io.h>	
#include <util/delay.h>
#include "../Serial/usbuart.h"
#include "../Serial/rs232uart.h"
#include "../Serial/rs485uart.h"
#include "../Serial/radio.h"
#include "../Ram/sram.h"
#include "sdhc.h"

// SPI module
void init_spi (void)
{
	SPCR  = (1 << SPE) | (1 << MSTR);
	SPSR |= (1 << SPI2X);
	//SPCR  = (1 << SPE)  | (1 << MSTR) | (1 << CPOL) | (1 << CPHA);
	//SPSR |= (1 << SPI2X); // use max speed
}

void xmit_spi (uint8_t d)
{
	SPDR = d;
	while(!(SPSR & (1 << SPIF)));
}

uint8_t rcv_spi (void)
{
	SPDR = 0xFF;
	
	// Wait for complete
	while (!(SPSR & (1<<SPIF)));
	
	return SPDR;
}

static uint8_t CardType;
static uint8_t send_cmd (uint8_t cmd, uint32_t arg);
static uint8_t sd_writef (uint8_t has_buffer, uint32_t sc);

/*******************************************************************************
* Function Name		: send_cmd
* Description		: Send a command packet to MMC
* Input				: command
*					: arguments
* Return			: response for R1
*******************************************************************************/
static uint8_t send_cmd (uint8_t cmd, uint32_t arg)
{
	uint8_t n, res;


	if (cmd & 0x80) 
	{	/* ACMD<n> is the command sequense of CMD55-CMD<n> */
		cmd &= 0x7F;
		res = send_cmd(CMD55, 0);
		if (res > 1) 
		{
			return res;
		}
	}

	/* Select the card */
	CS_HIGH();
	rcv_spi();
	CS_LOW();
	rcv_spi();

	/* Send a command packet */
	xmit_spi(cmd);						/* Start + Command index */
	xmit_spi((uint8_t)(arg >> 24));		/* Argument[31..24] */
	xmit_spi((uint8_t)(arg >> 16));		/* Argument[23..16] */
	xmit_spi((uint8_t)(arg >> 8));			/* Argument[15..8] */
	xmit_spi((uint8_t)arg);				/* Argument[7..0] */
	n = 0x01;							/* Dummy CRC + Stop */
	if (cmd == CMD0) n = 0x95;			/* Valid CRC for CMD0(0) */
	if (cmd == CMD8) n = 0x87;			/* Valid CRC for CMD8(0x1AA) */
	xmit_spi(n);

	/* Receive a command response */
	n = 10;								/* Wait for a valid response in timeout of 10 attempts */
	do {
		res = rcv_spi();
	} while ((res & 0x80) && --n);

	return res;			/* Return with the response value */
}

/*******************************************************************************
* Function Name		: sd_initialize
* Description		: Initialize Disk Drive   
* Input				: void
* Return			: 1 for fail
*					: 0 for success
*******************************************************************************/
uint8_t SDInit (void)
{
	uint8_t n, cmd, ty, ocr[4];
	unsigned int  tmr;

	if (CardType != 0 && IS_CS_LOW) 
	{
		sd_writef(0, 0);	/* Finalize write process if it is in progress */
	}
	
	init_spi();		/* Initialize ports to control MMC */
	CS_HIGH();
	for (n = 10; n; n--) 
	{
		rcv_spi();	/* 80 dummy clocks with CS=H */
	}

	ty = 0;
	uint8_t res = send_cmd(CMD0, 0);
	if (res == 1) 
	{			/* GO_IDLE_STATE */
		if (send_cmd(CMD8, 0x1AA) == 1) 
		{	/* SDv2 */
			for (n = 0; n < 4; n++) 
			{
				ocr[n] = rcv_spi();		/* Get trailing return value of R7 resp */
			}
			if (ocr[2] == 0x01 && ocr[3] == 0xAA) 
			{			/* The card can work at vdd range of 2.7-3.6V */
				for (tmr = 10000; tmr && send_cmd(ACMD41, 1UL << 30); tmr--) 
				{
					_delay_us(100);	/* Wait for leaving idle state (ACMD41 with HCS bit) */
				}
				if (tmr && send_cmd(CMD58, 0) == 0) 
				{		/* Check CCS bit in the OCR */
					for (n = 0; n < 4; n++) 
					{
						ocr[n] = rcv_spi();
					}
					ty = (ocr[0] & 0x40) ? CT_SD2 | CT_BLOCK : CT_SD2;	/* SDv2 (HC or SC) */
				}
			}
		} else 
		{							/* SDv1 or MMCv3 */
			if (send_cmd(ACMD41, 0) <= 1) 	
			{
				ty = CT_SD1; cmd = ACMD41;	/* SDv1 */
			} else 
			{
				ty = CT_MMC; cmd = CMD1;	/* MMCv3 */
			}
			for (tmr = 10000; tmr && send_cmd(cmd, 0); tmr--) 
			{
				_delay_us(100);	/* Wait for leaving idle state */
			}
			if (!tmr || send_cmd(CMD16, 512) != 0) 
			{	/* Set R/W block length to 512 */
				ty = 0;
			}
		}
	}
	
	CardType = ty;
	CS_HIGH();
	rcv_spi();

	return ty ? 0 : STA_NOINIT;
}

/*******************************************************************************
* Function Name		: sd_read
* Description		: Read sector into sram
* Input				: sector where to read
*					: choice 0 for sram
							 1 for usbaurt
* Return			: 1 for fail
*					: 0 for success
*******************************************************************************/
uint8_t SDRead (uint32_t sector, uint8_t choice) {
	uint8_t res, rc;
	unsigned int bc;
	uint16_t count = 512;
	uint8_t data[512];
	
	if (!(CardType & CT_BLOCK)) {
		sector *= 512;	/* Convert to byte address if needed */
	}

	res = 1;
	if (send_cmd(CMD17, sector) == 0) 
	{	/* READ_SINGLE_BLOCK */
		bc = 40000;	/* Time counter */
		do {				/* Wait for data block */
			rc = rcv_spi();
		} while (rc == 0xFF && --bc);
		
		if (rc == 0xFE) 
		{	/* A data block arrived */
			bc = 2;	/* Number of trailing bytes to skip */
			/* Receive a part of the sector */
			
			switch (choice)
			{
				case 0:
					SramSequentialWriteStart(LOGGING_BUFFER);
					do {
						SramSequentialWriteByte(rcv_spi());
					} while (--count);
					SramSequentialEnd();
					break;
				default:
					do {
						//rs232uart_sendbyte(rcv_spi());
						data[512 - count] = rcv_spi();
					} while (--count);
					break;
			}
			/* Skip trailing bytes in the sector and block CRC */
			do rcv_spi(); while (--bc);

			res = 0;
		}
	}

	CS_HIGH();
	rcv_spi();
	
	switch (choice)
	{
		case 1:
			USBUartSendUin8Array(data, 512);
			break;
		case 2:
			RS232UartSendUint8Array(data, 512);
			break;
		case 3:
			RS485UartSendUint8Array(data, 512);
			break;
		case 4:
			RadioSendUint8Array(data, 512);
			break;
	}

	return res;
}

/*******************************************************************************
* Function Name		: sd_writef
* Description		: Write to sd card, 
*					  (0, sector) for initialize
*					  (1, sector) for writing
*					  (0, 0) for finalize
* Input				: uint8 has_buffer
*					: sector where to read
* Return			: 1 for fail
*					: 0 for success
*******************************************************************************/
static uint8_t sd_writef (uint8_t has_buffer, uint32_t sc)
{
	uint8_t res;
	unsigned int  bc;
	static unsigned int wc;	/* Sector write counter */

	res = 1;

	if (has_buffer) 
	{		/* Send data bytes */
		bc = sc;
		SramSequentialReadStart(LOGGING_BUFFER);
		
		while (bc && wc)
		{		/* Send data bytes to the card */
			xmit_spi(SramSequentialReadByte());
			wc--; bc--;
		}
		SramSequentialEnd();
		res = 0;
	} 
	else 
	{
		if (sc) 
		{	/* Initiate sector write process */
			if (!(CardType & CT_BLOCK)) 
			{
				sc *= 512;	/* Convert to byte address if needed */
			}
			
			if (send_cmd(CMD24, sc) == 0) 
			{	/* WRITE_SINGLE_BLOCK */
				xmit_spi(0xFF); xmit_spi(0xFE);		/* Data block header */
				wc = 512;							/* Set byte counter */
				res = 0;
			}
		} 
		else 
		{	/* Finalize sector write process */
			bc = wc + 2;
			while (bc--) xmit_spi(0);	/* Fill left bytes and CRC with zeros */
			if ((rcv_spi() & 0x1F) == 0x05) 
			{	/* Receive data resp and wait for end of write process in timeout of 500ms */
				for (bc = 5000; rcv_spi() != 0xFF && bc; bc--) 
				{	/* Wait for ready */
					_delay_us(100);
				}
				
				if (bc) 
				{
					res = 0;
				}
			}
			
			CS_HIGH();
			rcv_spi();
		}
	}

	return res;
}

/*******************************************************************************
* Function Name		: sd_writef
* Description		: Write srame to sd card,
*					  (0, sector) for initialize
*					  (buffer, sector) for writing
*					  (0, 0) for finalize
* Input				: sector where to read
* Return			: 1 for fail
*					: 0 for success
*******************************************************************************/
uint8_t SDWrite (uint32_t sector) {
	// Initialize
	if (sd_writef(0, sector)) {
		return 1;
	}
	// Write
	if (sd_writef(1, 512)) {
		return 1;
	}
	// Finalize
	return sd_writef(0, 0);
}