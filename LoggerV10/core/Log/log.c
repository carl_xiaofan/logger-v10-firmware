/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: log.c
 * Description: Handle logging of the data
 */ 

#include <avr/io.h>
#include "../../common/util.h"
#include "../../common/state.h"
#include "../Serial/usbuart.h"
#include "../Serial/rs232uart.h"
#include "../Serial/rs485uart.h"
#include "../Serial/serial_packet.h"
#include "../Serial/radio.h"
#include "../Setting/setting.h"
#include "../Display/menu.h"
#include "../Display/lcd.h"
#include "../Ram/sram.h"
#include "sdhc.h"
#include "file_system.h"
#include "log.h"

static volatile uint8_t log_state;
static volatile uint8_t next_log_state;
static uint32_t current_sector;
static uint8_t last_date;

static uint16_t send_year;
static uint8_t send_month;
static uint8_t send_date;

static void RunSave(void);
static void RunSend(void);
static void RunPage(void);

void LogInit(void) {
	if (SDInit()) {
		log_state = STOP;
		return;
	}
	
	lcd_text_set(INFO_PAGE, 11, 2, "       ");
	
	// Clear RAM
	SramSequentialWriteStart(RECORD_BUFFER);
	for (uint8_t i = 0; i < RECORD_SIZE; ++i) {
		SramSequentialWriteByte(0x00);
	}
	SramSequentialEnd();
	
	current_sector = 0;
	last_date = 0;
	log_state = IDLE;
	next_log_state = IDLE;
}

void LogRun(void) {
	if (next_log_state != IDLE) {
		if (log_state == IDLE) {
			log_state = next_log_state;
			next_log_state = IDLE;
		}
	}
}


void LogWalk(void) {
	switch (log_state) {
		case IDLE:
			break;
		case SAVE:
			RunSave();
			break;
		case SEND:
			RunSend();
			break;
		case PAGE:
			RunPage();
			break;
		case RESET:
			break;
		case INIT:
			LogInit();
			break;
		case STOP:
			LogInit();
			break;
		default:
			break;
	}
}

/*******************************************************************************
* Function Name		: GetLogState
* Description		: return log state
* Input				: void
* Return			: log_state
*******************************************************************************/
uint8_t GetLogState(void) {
	return log_state;
}

/*******************************************************************************
* Function Name		: LogRecord
* Description		: ready to record data
* Input				: void
* Return			: void
*******************************************************************************/
void LogRecord(void) {
	if (log_state == IDLE) {
		log_state = SAVE;
	} else {
		next_log_state = SAVE;
	}
}

/*******************************************************************************
* Function Name		: LogSend
* Description		: ready to send data
* Input				: year, month and date for the data getting
* Return			: void
*******************************************************************************/
void LogSend(uint16_t year, uint8_t month, uint8_t date) {
	if (log_state == IDLE) {
		log_state = SEND;
	} else {
		next_log_state = SEND;
	}
	
	send_year = year;
	send_month = month;
	send_date = date;
}

/*******************************************************************************
* Function Name		: LogPage
* Description		: ready to send menu
* Input				: void
* Return			: void
*******************************************************************************/
void LogPage(void) {
	if (log_state == IDLE) {
		log_state = PAGE;
	} else {
		next_log_state = PAGE;
	}
}

/*******************************************************************************
* Function Name		: RunSave
* Description		: save the data into SD card
* Input				: void
* Return			: void
*******************************************************************************/
static void RunSave(void) {
	uint16_t year;
	uint8_t record_buffer[RECORD_SIZE];
	SramSequentialReadStart(RECORD_BUFFER);
	for (uint8_t i = 0; i < RECORD_SIZE; i ++) {
		record_buffer[i] = SramSequentialReadByte();
	}
	SramSequentialEnd();
	year = U8ToU16(record_buffer[0], record_buffer[1]);
	if (current_sector) {
		if ((record_buffer[3] != last_date)) {
			FSAddDateSector(year, record_buffer[2], record_buffer[3], &current_sector);
			last_date = record_buffer[3];
		}
	} else {
		current_sector = FSGetDateSector(year, record_buffer[2], record_buffer[3]);
		if (!current_sector) {
			FSAddDateSector(year, record_buffer[2], record_buffer[3], &current_sector);
			last_date = record_buffer[3];
		}
	}
	FSRecordData(current_sector, record_buffer);
	log_state = IDLE;
}

/*******************************************************************************
* Function Name		: RunSend
* Description		: Send the data to USB or modem
* Input				: void
* Return			: void
*******************************************************************************/
static void RunSend(void) {
	MenuBusyUpload(1);
	uint32_t date_sector = FSGetDateSector(send_year, send_month, send_date);
	if (date_sector) {
		switch (SettingGetSerialMode()) {
			case MODE_USB:
				USBUartSendByte(START_BYTE);
				USBUartSendByte(START_BYTE);
				USBUartSendByte(0);
				USBUartSendByte(0);
				USBUartSendByte(U16ToU8(SettingsGetSerial())[0]);
				USBUartSendByte(U16ToU8(SettingsGetSerial())[1]);
				USBUartSendByte(DAT);
				break;
			case MODE_RS232:
				RS232UartSendByte(START_BYTE);
				RS232UartSendByte(START_BYTE);
				RS232UartSendByte(0);
				RS232UartSendByte(0);
				RS232UartSendByte(U16ToU8(SettingsGetSerial())[0]);
				RS232UartSendByte(U16ToU8(SettingsGetSerial())[1]);
				RS232UartSendByte(DAT);
				break;
			case MODE_RS485:
				RS485UartTXStart();
				RS485UartSendByte(START_BYTE);
				RS485UartSendByte(START_BYTE);
				RS485UartSendByte(0);
				RS485UartSendByte(0);
				RS485UartSendByte(U16ToU8(SettingsGetSerial())[0]);
				RS485UartSendByte(U16ToU8(SettingsGetSerial())[1]);
				RS485UartSendByte(DAT);
				break;
			case MODE_RADIO:
				RadioSendByte(START_BYTE);
				RadioSendByte(START_BYTE);
				RadioSendByte(0);
				RadioSendByte(0);
				RadioSendByte(U16ToU8(SettingsGetSerial())[0]);
				RadioSendByte(U16ToU8(SettingsGetSerial())[1]);
				RadioSendByte(DAT);
		}
		
		FSReadData(date_sector);
		
		switch (SettingGetSerialMode()) {
			case MODE_USB:
				USBUartSendByte(STOP_BYTE);
				USBUartSendByte(STOP_BYTE);
				break;
			case MODE_RS232:
				RS232UartSendByte(STOP_BYTE);
				RS232UartSendByte(STOP_BYTE);
				break;
			case MODE_RS485:
				RS485UartSendByte(STOP_BYTE);
				RS485UartSendByte(STOP_BYTE);
				RS485UartTXStop();
				break;
			case MODE_RADIO:
				RadioSendByte(STOP_BYTE);
				RadioSendByte(STOP_BYTE);
				break;
		}
	} else {
		switch (SettingGetSerialMode()) {
			case MODE_USB:
				USBUartSendByte(START_BYTE);
				USBUartSendByte(START_BYTE);
				USBUartSendByte(0);
				USBUartSendByte(0);
				USBUartSendByte(U16ToU8(SettingsGetSerial())[0]);
				USBUartSendByte(U16ToU8(SettingsGetSerial())[1]);
				USBUartSendByte(STOP_BYTE);
				break;
			case MODE_RS232:
				RS232UartSendByte(START_BYTE);
				RS232UartSendByte(START_BYTE);
				RS232UartSendByte(0);
				RS232UartSendByte(0);
				RS232UartSendByte(U16ToU8(SettingsGetSerial())[0]);
				RS232UartSendByte(U16ToU8(SettingsGetSerial())[1]);
				RS232UartSendByte(STOP_BYTE);
				break;
			case MODE_RS485:
				RS485UartTXStart();
				RS485UartSendByte(START_BYTE);
				RS485UartSendByte(START_BYTE);
				RS485UartSendByte(0);
				RS485UartSendByte(0);
				RS485UartSendByte(U16ToU8(SettingsGetSerial())[0]);
				RS485UartSendByte(U16ToU8(SettingsGetSerial())[1]);
				RS485UartSendByte(STOP_BYTE);
				RS485UartTXStop();
				break;
			case MODE_RADIO:
				RadioSendByte(START_BYTE);
				RadioSendByte(START_BYTE);
				RadioSendByte(0);
				RadioSendByte(0);
				RadioSendByte(U16ToU8(SettingsGetSerial())[0]);
				RadioSendByte(U16ToU8(SettingsGetSerial())[1]);
				RadioSendByte(STOP_BYTE);
		}
	}
	
	log_state = IDLE;
	MenuBusyUpload(0);
}

/*******************************************************************************
* Function Name		: RunPage
* Description		: Send the menu to USB or modem
* Input				: void
* Return			: void
*******************************************************************************/
static void RunPage(void) {
	switch (SettingGetSerialMode()) {
		case MODE_USB:
			USBUartSendByte(START_BYTE);
			USBUartSendByte(START_BYTE);
			USBUartSendByte(0);
			USBUartSendByte(0);
			USBUartSendByte(U16ToU8(SettingsGetSerial())[0]);
			USBUartSendByte(U16ToU8(SettingsGetSerial())[1]);
			USBUartSendByte(DAT);
			break;
		case MODE_RS232:
			RS232UartSendByte(START_BYTE);
			RS232UartSendByte(START_BYTE);
			RS232UartSendByte(0);
			RS232UartSendByte(0);
			RS232UartSendByte(U16ToU8(SettingsGetSerial())[0]);
			RS232UartSendByte(U16ToU8(SettingsGetSerial())[1]);
			RS232UartSendByte(DAT);
			break;
		case MODE_RS485:
			RS485UartTXStart();
			RS485UartSendByte(START_BYTE);
			RS485UartSendByte(START_BYTE);
			RS485UartSendByte(0);
			RS485UartSendByte(0);
			RS485UartSendByte(U16ToU8(SettingsGetSerial())[0]);
			RS485UartSendByte(U16ToU8(SettingsGetSerial())[1]);
			RS485UartSendByte(DAT);
			break;
		case MODE_RADIO:
			RadioSendByte(START_BYTE);
			RadioSendByte(START_BYTE);
			RadioSendByte(0);
			RadioSendByte(0);
			RadioSendByte(U16ToU8(SettingsGetSerial())[0]);
			RadioSendByte(U16ToU8(SettingsGetSerial())[1]);
			RadioSendByte(DAT);
			break;
	}
	
	FSReadList();
	
	switch (SettingGetSerialMode()) {
		case MODE_USB:
			USBUartSendByte(STOP_BYTE);
			USBUartSendByte(STOP_BYTE);
			break;
		case MODE_RS232:
			RS232UartSendByte(STOP_BYTE);
			RS232UartSendByte(STOP_BYTE);
			break;
		case MODE_RS485:
			RS485UartSendByte(STOP_BYTE);
			RS485UartSendByte(STOP_BYTE);
			RS485UartTXStop();
			break;
		case MODE_RADIO:
			RadioSendByte(STOP_BYTE);
			RadioSendByte(STOP_BYTE);
			break;
	}
	
	log_state = IDLE;
}

/*******************************************************************************
* Function Name		: LogReset
* Description		: Reset the log (SD card), clear the data
* Input				: void
* Return			: void
*******************************************************************************/
void LogReset(void) {
	FSResetSector(SETTING_MENU_SECTOR);
	FSResetSector(YEAR_MENU_SECTOR);
	FSResetSector(MONTH_MENU_SECTOR_START);
	FSResetSector(DAY_MENU_SECTOR_START);
	
	LOG_SETTING settings;
	settings.current_data_sector = 	DATA_SECTOR_START;
	WriteLogSetting(settings);
	
	current_sector = 0;
	last_date = 0;
}