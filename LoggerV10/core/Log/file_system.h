/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: filesystem.h
 * Description: Header file for filesystem.c
 */ 

#ifndef FILE_SYSTEM_H_
#define FILE_SYSTEM_H_

#define FILE_COUNT_POSITION		0

#define SETTING_MENU_SECTOR		1
#define YEAR_MENU_SECTOR		2
#define MONTH_MENU_SECTOR_START	3	// Inclusive	50 Years
#define MONTH_MENU_SECTOR_END	52	// Inclusive
#define DAY_MENU_SECTOR_START	53	// Inclusive	600 Months
#define DAY_MENU_SECTOR_END		652
#define	DATA_SECTOR_START		653

#define SECTOR_SIZE_EACH_DAY	1687

#define SETTING_MENU_SIZE		4
#define YEAR_MENU_SIZE			6
#define MONTH_MENU_SIZE			5
#define DAY_MENU_SIZE			5

#define SECTOR_SIZE				512

typedef struct LOG_SETTING
{
	uint32_t current_data_sector;
}LOG_SETTING;

uint8_t FSResetSector(uint32_t sector);

uint8_t FSAddDateSector(uint16_t year, uint8_t month, uint8_t day, uint32_t* sector);

uint32_t FSGetDateSector(uint16_t year, uint8_t month, uint8_t day);

uint8_t FSRecordData(uint32_t sector, uint8_t* record_buffer);

uint8_t FSReadData(uint32_t sector);

uint8_t FSReadList();

void WriteLogSetting(LOG_SETTING setting);

#endif /* FILESYSTEM_H_ */