/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: log.h
 * Description: Header file for log.c
 */ 

#include "../RTC/DS3232.h"

#ifndef LOG_H_
#define LOG_H_

#define RECORD_SIZE		40

void LogInit(void);

void LogRun(void);

void LogWalk(void);

uint8_t GetLogState(void);

void LogRecord(void);

void LogSend(uint16_t year, uint8_t month, uint8_t date);

void LogPage(void);

void LogReset(void);

#endif /* LOG_H_ */