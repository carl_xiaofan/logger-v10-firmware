/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: sdhc.h
 * Description: Header file for sdhc.c
 */ 

#ifndef SDHC_H_
#define SDHC_H_

#define STA_NOINIT		0x01	/* Drive not initialized */

#define SPI_CLK_HI()                 PORTB  |=  (_BV(PB1))
#define SPI_CLK_LO()                 PORTB  &= ~(_BV(PB1))
#define SPI_MOSI_HI()                PORTB  |=  (_BV(PB2))
#define SPI_MOSI_LO()                PORTB  &= ~(_BV(PB2))
#define SPI_MISO()                   PINB   &   (_BV(PB3))

#define CS_LOW()	PORTB  &= ~(_BV(PB4))	/* Set CS low */
#define	CS_HIGH()	PORTB  |=  (_BV(PB4))	/* Set CS high */
#define	IS_CS_LOW	!(PINB & _BV(4))	/* Test if CS is low */

/* Definitions for MMC/SDC command */
#define CMD0	(0x40+0)	/* GO_IDLE_STATE */
#define CMD1	(0x40+1)	/* SEND_OP_COND (MMC) */
#define	ACMD41	(0xC0+41)	/* SEND_OP_COND (SDC) */
#define CMD8	(0x40+8)	/* SEND_IF_COND */
#define CMD16	(0x40+16)	/* SET_BLOCKLEN */
#define CMD17	(0x40+17)	/* READ_SINGLE_BLOCK */
#define CMD24	(0x40+24)	/* WRITE_BLOCK */
#define CMD55	(0x40+55)	/* APP_CMD */
#define CMD58	(0x40+58)	/* READ_OCR */

/* Card type flags (CardType) */
#define CT_MMC				0x01	/* MMC ver 3 */
#define CT_SD1				0x02	/* SD ver 1 */
#define CT_SD2				0x04	/* SD ver 2 */
#define CT_SDC				(CT_SD1|CT_SD2)	/* SD */
#define CT_BLOCK			0x08	/* Block addressing */

/*---------------------------------------*/
/* Prototypes for disk control functions */

uint8_t SDInit (void);
uint8_t SDRead (uint32_t sector, uint8_t choice);
uint8_t SDWrite (uint32_t sector);

#endif /* SDHC_H_ */