/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: filesystem.c
 * Description: Implement file system in SD card, handle for read and write
 */ 

#include <avr/io.h>
#include <stdlib.h>
#include <util/delay.h>
#include "../../common/util.h"
#include "../Serial/usbuart.h"
#include "../Serial/radio.h"
#include "../Serial/rs232uart.h"
#include "../Serial/rs485uart.h"
#include "../Serial/serial_packet.h"
#include "../Setting/setting.h"
#include "../Ram/sram.h"
#include "log.h"
#include "file_system.h"
#include "sdhc.h"

static void ReadLogSetting(LOG_SETTING* setting);
static uint8_t FSAddYearSector(uint16_t year , uint32_t* sector);
static uint32_t FSGetYearSector(uint16_t year);
static uint8_t FSAddMonthSector(uint32_t menu, uint8_t month , uint32_t* sector);
static uint32_t FSGetMonthSector(uint32_t menu, uint8_t month);
static uint8_t FSAddDaySector(uint32_t menu, uint8_t day , uint32_t* sector);
static uint32_t FSGetDaySector(uint32_t menu, uint8_t day);

/*******************************************************************************
* Function Name		: FSResetSector
* Description		: reset certain sector
* Input				: uin32_t sector
* Return			: 1 for fail
*					: 0 for success
*******************************************************************************/
uint8_t FSResetSector(uint32_t sector) {
	SramSequentialWriteStart(LOGGING_BUFFER);
	for (uint16_t i = 0; i < SECTOR_SIZE; i++) {
		SramSequentialWriteByte(0x00);
	}
	SramSequentialEnd();
	return SDWrite(sector);
}


/*******************************************************************************
* Function Name		: FSAddDateSector
* Description		: Add a date to menu
* Input				: year
*					: month
*					: day
*					: sector pointer to get the pointer to data sector
* Return			: 1 for success
*					: 0 for date already existed
*******************************************************************************/
uint8_t FSAddDateSector(uint16_t year, uint8_t month, uint8_t day, uint32_t* sector) {
	uint32_t month_sector, day_sector, data_sector;
	
	month_sector = FSGetYearSector(year);
	if (month_sector)
	{
		day_sector = FSGetMonthSector(month_sector, month);
		if (day_sector) {
			data_sector = FSGetDaySector(day_sector, day);
			if (data_sector) {
				*sector = data_sector;
				return 0;
			} else {
				FSAddDaySector(day_sector, day, sector);
				FSResetSector(*sector);
			}
		} else { 
			FSAddMonthSector(month_sector, month, &day_sector);
			FSResetSector(day_sector);
			FSAddDaySector(day_sector, day, sector);
			FSResetSector(*sector);
		}
	} else {
		FSAddYearSector(year, &month_sector);
		FSResetSector(month_sector);
		FSAddMonthSector(month_sector, month, &day_sector);
		FSResetSector(day_sector);
		FSAddDaySector(day_sector, day, sector);
		FSResetSector(*sector);
	}
	
	return 1;
}

/*******************************************************************************
* Function Name		: FSGetDateSector
* Description		: Get a date sector from menu
* Input				: year
*					: month
*					: day
* Return			: sector address if success
*					: 0 if not found
*******************************************************************************/
uint32_t FSGetDateSector(uint16_t year, uint8_t month, uint8_t day) {
	uint32_t year_sector, month_sector, day_sector;
	
	year_sector = FSGetYearSector(year);
	if (year_sector) {
		month_sector = FSGetMonthSector(year_sector, month);
		if (month_sector) {
			day_sector = FSGetDaySector(month_sector, day);
			if (day_sector) {
				return day_sector;
			}
		}
	}
	
	return 0;
}


/*******************************************************************************
* Function Name		: FSRecordData
* Description		: Record data to sector page
* Input				: sector Start sector for day
*					: record
* Return			: 0 for success
*					: 1 for fail
*******************************************************************************/
uint8_t FSRecordData(uint32_t sector, uint8_t* record_buffer) {
	uint8_t t1, t2;
	uint32_t count ,page, offset;
	// Read count
	SDRead(sector, 0);
	SramSequentialReadStart(LOGGING_BUFFER);
	t1 = SramSequentialReadByte();
	t2 = SramSequentialReadByte();
	count = U8ToU32(t1, t2, 0, 0);
	SramSequentialEnd();
	
	page = (2 + count * RECORD_SIZE) / SECTOR_SIZE;
	offset = (2 + count * RECORD_SIZE) % SECTOR_SIZE;
	
	// Add Count
	SramSequentialWriteStart(LOGGING_BUFFER);
	SramSequentialWriteByte(U32ToU8(count + 1)[0]);
	SramSequentialWriteByte(U32ToU8(count + 1)[1]);
	SramSequentialEnd();
	SDWrite(sector);
		
	if ((offset + RECORD_SIZE) <= SECTOR_SIZE) {
		SDRead(sector + page, 0);
		SramSequentialWriteStart(LOGGING_BUFFER + offset);
		for (uint8_t i = 0; i < RECORD_SIZE; ++i) {
			SramSequentialWriteByte(record_buffer[i]);
		}
		SramSequentialEnd();
		return SDWrite(sector + page);
	} else {
		SDRead(sector + page, 0);
		SramSequentialWriteStart(LOGGING_BUFFER + offset);
		for (uint8_t i = 0; i < SECTOR_SIZE - offset; ++i) {
			SramSequentialWriteByte(record_buffer[i]);
		}
		SramSequentialEnd();
		SDWrite(sector + page);
		
		SramSequentialWriteStart(LOGGING_BUFFER);
		for (uint8_t i = SECTOR_SIZE - offset; i < RECORD_SIZE; ++i) {
			SramSequentialWriteByte(record_buffer[i]);
		}
		SramSequentialEnd();
		return SDWrite(sector + page + 1);
	}
}

/*******************************************************************************
* Function Name		: FSReadData
* Description		: Read data and output to choice
* Input				: sector Start sector for day
*					: choice 1 for USB
*					         2 for Modem
* Return			: 0 for success
*					: 1 for fail
*******************************************************************************/
uint8_t FSReadData(uint32_t sector) {
	uint8_t t1, t2;
	uint32_t count ,page;
	// Read count
	SDRead(sector, 0);
	SramSequentialReadStart(LOGGING_BUFFER);
	t1 = SramSequentialReadByte();
	t2 = SramSequentialReadByte();
	count = U8ToU32(t1, t2, 0, 0);
	SramSequentialEnd();
	
	page = (2 + count * RECORD_SIZE) / SECTOR_SIZE;
	
	switch (SettingGetSerialMode()) {
		case MODE_USB:
			for (unsigned int i = 0; i < page; i++) {
				SDRead(sector + i, 1);
			}
			
			SDRead(sector + page, 0);
			SramSequentialReadStart(LOGGING_BUFFER);
			for (uint16_t i = 0; i < (2 + count * RECORD_SIZE) % SECTOR_SIZE; i++) {
				USBUartSendByte(SramSequentialReadByte());
			}
			break;
		case MODE_RS232:
			for (unsigned int i = 0; i < page; i++) {
				SDRead(sector + i, 2);
				_delay_ms(100);
			}
			
			SDRead(sector + page, 0);
			SramSequentialReadStart(LOGGING_BUFFER);
			for (uint16_t i = 0; i < (2 + count * RECORD_SIZE) % SECTOR_SIZE; i++) {
				RS232UartSendByte(SramSequentialReadByte());
			}
			break;
		case MODE_RS485:
			for (unsigned int i = 0; i < page; i++) {
				SDRead(sector + i, 3);
				_delay_ms(200);
			}
				
			SDRead(sector + page, 0);
			SramSequentialReadStart(LOGGING_BUFFER);
			for (uint16_t i = 0; i < (2 + count * RECORD_SIZE) % SECTOR_SIZE; i++) {
				RS485UartSendByte(SramSequentialReadByte());
			}
			break;
		case MODE_RADIO:
			for (unsigned int i = 0; i < page; i++) {
				SDRead(sector + i, 4);
				_delay_ms(200);
			}
			
			SDRead(sector + page, 0);
			SramSequentialReadStart(LOGGING_BUFFER);
			for (uint16_t i = 0; i < (2 + count * RECORD_SIZE) % SECTOR_SIZE; i++) {
				RadioSendByte(SramSequentialReadByte());
			}
			break;
	}
	
	SramSequentialEnd();
	return 1;
}

/*******************************************************************************
* Function Name		: FSReadList
* Description		: Get a list of available date in the sd card
* Input				: choice 1 for USB
*					         2 for Modem
* Return			: 0 for success
*					: 1 for fail
*******************************************************************************/
uint8_t FSReadList() {
	uint8_t month, day, year_count, month_count, day_count;
	uint16_t year;
	uint32_t year_sector, month_sector;
	// Read sector
	SDRead(YEAR_MENU_SECTOR, 0);
	SramSequentialReadStart(LOGGING_BUFFER);
	year_count = SramSequentialReadByte();
	SramSequentialEnd();
	
	for (uint8_t i = 0; i < year_count; i++) {
		SDRead(YEAR_MENU_SECTOR, 0);
		SramSequentialReadStart(LOGGING_BUFFER + 1 + i * YEAR_MENU_SIZE);
		year = SramSequentialRead16bits();
		year_sector = SramSequentialRead32bits();
		SramSequentialEnd();
		
		// Month
		SDRead(year_sector, 0);
		SramSequentialReadStart(LOGGING_BUFFER);
		month_count = SramSequentialReadByte();
		SramSequentialEnd();
		
		for (uint8_t y = 0; y < month_count; y++) {
			SDRead(year_sector, 0);
			SramSequentialReadStart(LOGGING_BUFFER + 1 + y * MONTH_MENU_SIZE);
			month = SramSequentialReadByte();
			month_sector = SramSequentialRead32bits();
			SramSequentialEnd();
			
			// Day
			SDRead(month_sector, 0);
			SramSequentialReadStart(LOGGING_BUFFER);
			day_count = SramSequentialReadByte();
			SramSequentialEnd();

			for (uint8_t z = 0; z < day_count; z++) {
				SramSequentialReadStart(LOGGING_BUFFER + 1 + z * DAY_MENU_SIZE);
				day = SramSequentialReadByte();
				
				switch (SettingGetSerialMode()) {
					case MODE_USB:
						USBUartSendByte(U16ToU8(year)[0]);
						USBUartSendByte(U16ToU8(year)[1]);
						USBUartSendByte(month);
						USBUartSendByte(day);
						break;
					case MODE_RS232:
						RS232UartSendByte(U16ToU8(year)[0]);
						RS232UartSendByte(U16ToU8(year)[1]);
						RS232UartSendByte(month);
						RS232UartSendByte(day);
						break;
					case MODE_RS485:
						RS485UartSendByte(U16ToU8(year)[0]);
						RS485UartSendByte(U16ToU8(year)[1]);
						RS485UartSendByte(month);
						RS485UartSendByte(day);
						break;
					case MODE_RADIO:
						RadioSendByte(U16ToU8(year)[0]);
						RadioSendByte(U16ToU8(year)[1]);
						RadioSendByte(month);
						RadioSendByte(day);
						break;
				}
				SramSequentialEnd();
			}
		}
		
	}
	return 1;
}

/*******************************************************************************
* Function Name		: FSAddYearSector
* Description		: Add a year sector to menu
* Input				: year
*					: sector pointer to get the pointer to data sector
* Return			: 1 for success
*					: 0 for date already existed
*******************************************************************************/
static uint8_t FSAddYearSector(uint16_t year , uint32_t* sector) {
	SDRead(YEAR_MENU_SECTOR, 0);
	SramSequentialReadStart(LOGGING_BUFFER);
	uint8_t count = SramSequentialReadByte();
	SramSequentialEnd();
		
	uint8_t buf[YEAR_MENU_SIZE];
	buf[0] = U16ToU8(year)[0];
	buf[1] = U16ToU8(year)[1];
	*sector = U8ToU32(count + 3, 0, 0, 0);
	buf[2] = count + 3;
	buf[3] = 0;
	buf[4] = 0;
	buf[5] = 0;
		
	// Add Count
	SramSequentialWriteStart(LOGGING_BUFFER);
	SramSequentialWriteByte(count + 1);
	SramSequentialEnd();
		
	// Write Day_Menu
	SramSequentialWriteStart(LOGGING_BUFFER + count * YEAR_MENU_SIZE + 1);
	for (uint8_t i = 0; i < YEAR_MENU_SIZE; ++i) {
		SramSequentialWriteByte(buf[i]);
	}
	SramSequentialEnd();
		
	return SDWrite(YEAR_MENU_SECTOR);
}

/*******************************************************************************
* Function Name		: FSGetYearSector
* Description		: Get year sector from menu
* Input				: year
* Return			: year sector
*******************************************************************************/
static uint32_t FSGetYearSector(uint16_t year) {
	// Read sector
	SDRead(YEAR_MENU_SECTOR, 0);
	SramSequentialReadStart(LOGGING_BUFFER);
	uint8_t count = SramSequentialReadByte();
	
	// Loop to find
	for (uint8_t i = 0; i < count; i++) {
		if(SramSequentialRead16bits() == year) {
			uint32_t year_sector = SramSequentialRead32bits();
			SramSequentialEnd();
			
			return year_sector;
		} else {
			SramSequentialRead32bits();
		}
	}
	SramSequentialEnd();
	return 0;
}

/*******************************************************************************
* Function Name		: FSAddMonthSector
* Description		: Add a month sector to menu
* Input				: month
*					: menu year menu
*					: sector pointer to get the pointer to data sector
* Return			: 1 for success
*					: 0 for date already existed
*******************************************************************************/
static uint8_t FSAddMonthSector(uint32_t menu, uint8_t month , uint32_t* sector) {
	SDRead(menu, 0);
	SramSequentialReadStart(LOGGING_BUFFER);
	uint8_t count = SramSequentialReadByte();
	SramSequentialEnd();
	
	uint8_t buf[MONTH_MENU_SIZE];
	buf[0] = month;
	*sector = U16ToU32(53 + count + 12 * (menu - 3), 0);
	buf[1] = U16ToU8(53 + count + 12 * (menu - 3))[0];
	buf[2] = U16ToU8(53 + count + 12 * (menu - 3))[1];
	buf[3] = 0;
	buf[4] = 0;
	
	// Add Count
	SramSequentialWriteStart(LOGGING_BUFFER);
	SramSequentialWriteByte(count + 1);
	SramSequentialEnd();
	
	// Write Day_Menu
	SramSequentialWriteStart(LOGGING_BUFFER + count * MONTH_MENU_SIZE + 1);
	for (uint8_t i = 0; i < MONTH_MENU_SIZE; i++) {
		SramSequentialWriteByte(buf[i]);
	}
	SramSequentialEnd();
	
	return SDWrite(menu);
}

/*******************************************************************************
* Function Name		: FSGetMonthSector
* Description		: Get month sector from menu
* Input				: year
*					: menu year menu
* Return			: month sector
*******************************************************************************/
static uint32_t FSGetMonthSector(uint32_t menu, uint8_t month) {
	// Read sector
	SDRead(menu, 0);
	SramSequentialReadStart(LOGGING_BUFFER);
	uint8_t count = SramSequentialReadByte();
	
	// Loop to find
	for (uint8_t i = 0; i < count; i++) {
		if(SramSequentialReadByte() == month) {
			uint32_t month_sector = SramSequentialRead32bits();
			SramSequentialEnd();
			return month_sector;
		} else {
			// Skip four byte
			SramSequentialRead32bits();
		}
	}
	SramSequentialEnd();
	return 0;
}

/*******************************************************************************
* Function Name		: FSAddDaySector
* Description		: Add a day sector to menu
* Input				: year
*					: menu month menu
*					: sector pointer to get the pointer to data sector
* Return			: 1 for success
*					: 0 for date already existed
*******************************************************************************/
static uint8_t FSAddDaySector(uint32_t menu, uint8_t day , uint32_t* sector) {
	LOG_SETTING settings;
	ReadLogSetting(&settings);
	*sector = settings.current_data_sector;
	settings.current_data_sector += SECTOR_SIZE_EACH_DAY;
	WriteLogSetting(settings);

	SDRead(menu, 0);
	SramSequentialReadStart(LOGGING_BUFFER);
	uint8_t count = SramSequentialReadByte();
	SramSequentialEnd();
	
	uint8_t buf[DAY_MENU_SIZE];
	buf[0] = day;
	buf[1] = U32ToU8(*sector)[0];
	buf[2] = U32ToU8(*sector)[1];
	buf[3] = U32ToU8(*sector)[2];
	buf[4] = U32ToU8(*sector)[3];
	
	// Add Count
	SramSequentialWriteStart(LOGGING_BUFFER);
	SramSequentialWriteByte(count + 1);
	SramSequentialEnd();
	
	// Write Day_Menu
	SramSequentialWriteStart(LOGGING_BUFFER + count * DAY_MENU_SIZE + 1);
	for (uint8_t i = 0; i < DAY_MENU_SIZE; i++) {
		SramSequentialWriteByte(buf[i]);
	}
	SramSequentialEnd();
	
	return SDWrite(menu);
}

/*******************************************************************************
* Function Name		: FSGetDaySector
* Description		: Get day sector from menu
* Input				: year
*					: menu month menu
* Return			: day sector
*******************************************************************************/
static uint32_t FSGetDaySector(uint32_t menu, uint8_t day) {
	// Read sector
	SDRead(menu, 0);
	SramSequentialReadStart(LOGGING_BUFFER);
	uint8_t count = SramSequentialReadByte();
	
	// Loop to find
	for (uint8_t i = 0; i < count; i++) {
		if(SramSequentialReadByte() == day) {
			uint32_t day_sector = SramSequentialRead32bits();
			SramSequentialEnd();
			return day_sector;
		} else {
			// Skip four byte
			SramSequentialRead32bits();
		}
	}
	SramSequentialEnd();
	return 0;
}

/*******************************************************************************
* Function Name		: WriteLogSetting
* Description		: Write log setting to log page
* Input				: Log setting structure
* Return			: void
*******************************************************************************/
void WriteLogSetting(LOG_SETTING setting) {
	SDRead(SETTING_MENU_SECTOR, 0);
	SramSequentialWriteStart(LOGGING_BUFFER);
	for (uint8_t i = 0; i < SETTING_MENU_SIZE; i++) {
		SramSequentialWriteByte(U32ToU8(setting.current_data_sector)[i]);
	}
	SramSequentialEnd();
	SDWrite(SETTING_MENU_SECTOR);
}

/*******************************************************************************
* Function Name		: ReadLogSetting
* Description		: Read log setting from log page
* Input				: Log setting structure pointer
* Return			: void
*******************************************************************************/
static void ReadLogSetting(LOG_SETTING* setting) {
	SDRead(SETTING_MENU_SECTOR, 0);
	uint8_t buf[SETTING_MENU_SIZE];
	SramSequentialReadStart(LOGGING_BUFFER);
	for (uint8_t i = 0; i < SETTING_MENU_SIZE; i++) {
		buf[i] = SramSequentialReadByte();
	}
	SramSequentialEnd();
	setting->current_data_sector = U8ToU32(buf[0], buf[1], buf[2], buf[3]);
}