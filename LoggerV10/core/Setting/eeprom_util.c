/*
 * eeprom_util.c
 *
 * Created: 20/12/2019 11:13:03 AM
 *  Author: xiaofan
 */ 

#include <avr/eeprom.h>
#include <stdlib.h>
#include "../../common/util.h"
#include "eeprom_util.h"

/*******************************************************************************
* Function Name		: eeprom_write_16bit
* Description		: Write 16bit data into eeprom using 8 bit address
* Input				: address
*					: data
* Return			: void
*******************************************************************************/
void EEPROMWrite16bits(uint8_t* address, uint16_t data)
{
	uint8_t* bytes = U16ToU8(data);
	eeprom_write_byte(address, bytes[0]);
	eeprom_write_byte(address + 1, bytes[1]);
}

/*******************************************************************************
* Function Name		: eeprom_read_16bit
* Description		: Read 16bit data from eeprom using 8 bit address
* Input				: address
*					: data
* Return			: uint16_t Data read
*******************************************************************************/
uint16_t EEPROMRead6bits(uint8_t* address)
{
	return U8ToU16(eeprom_read_byte(address), eeprom_read_byte(address + 1));
}

/*******************************************************************************
* Function Name		: eeprom_write_32bit
* Description		: Write 32bit data into eeprom using 8 bit address
* Input				: address
*					: data
* Return			: void
*******************************************************************************/
void EEPROMWrite32bits(uint8_t* address, uint32_t data)
{
	uint8_t* bytes = U32ToU8(data);
	eeprom_write_byte(address, bytes[0]);
	eeprom_write_byte(address + 1, bytes[1]);
	eeprom_write_byte(address + 2, bytes[2]);
	eeprom_write_byte(address + 3, bytes[3]);
}

/*******************************************************************************
* Function Name		: eeprom_read_32bit
* Description		: Read 32bit data from eeprom using 8 bit address
* Input				: address
*					: data
* Return			: uint32_t Data read
*******************************************************************************/
uint32_t EEPROMRead32bits(uint8_t* address)
{
	return U8ToU32(eeprom_read_byte(address), eeprom_read_byte(address + 1), eeprom_read_byte(address + 2), eeprom_read_byte(address + 3));
}

/*******************************************************************************
* Function Name		: eeprom_write_datetime
* Description		: Write date time into eeprom, total size will be 7 byte
* Input				: address
*					: datetime
* Return			: void
*******************************************************************************/
void EEPROMWriteDatetime(uint8_t* address, DateTime_t datetime)
{
	EEPROMWrite16bits(address, datetime.Year);
	eeprom_write_byte(address + 2, (uint8_t)datetime.Month);
	eeprom_write_byte(address + 3, (uint8_t)datetime.Date);
	eeprom_write_byte(address + 4, datetime.Hour);
	eeprom_write_byte(address + 5, datetime.Minute);
	eeprom_write_byte(address + 6, datetime.Second);
}

/*******************************************************************************
* Function Name		: eeprom_read_datetime
* Description		: Read date time into eeprom, total size will be 7 byte
* Input				: address
* Return			: Datetime readed
*******************************************************************************/
DateTime_t EEPROMReadDatetime(uint8_t* address)
{
	DateTime_t t;
	t.Year = EEPROMRead6bits(address);
	t.Month = eeprom_read_byte(address+2);
	t.Date = eeprom_read_byte(address+3);
	t.Hour = eeprom_read_byte(address+4);
	t.Minute = eeprom_read_byte(address+5);
	t.Second = eeprom_read_byte(address+6);
	
	return t;
}