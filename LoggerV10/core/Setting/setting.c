/*
 * setting.c
 *
 * Created: 20/12/2019 10:12:29 AM
 *  Author: xiaofan
 */ 

#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include "../../common/util.h"
#include "../../main.h"
#include "../RTC/DS3232.h"
#include "eeprom_util.h"
#include "eeprom_structure.h"
#include "setting.h"

static uint8_t is_set = 0;

// Call after setting has been changed
void been_set(void)
{
	is_set = 1;
}

// Check if settings are changed
uint8_t has_set(void)
{
	if (is_set)
	{
		is_set = 0;
		return 1;
	}
	else
	{
		return 0;
	}
}

uint8_t SettingsInitComplete(void)
{
	return 1;
}

void ResetReadSetting(void)
{
	for (uint8_t i = 0; i < 8; i++)
	{
		SettingSetSensorType(i, 0);
		SettingSetCycles(i, 250);
		SettingSetSensorSettle(i, 40);
		SettingSetExciteStart(i, 1600);
		SettingSetExciteStop(i, 3200);
		SettingSetSensorEquation(i, 0);
		SettingSetCoefficient(i, 0, 0);
		SettingSetCoefficient(i, 1, 0);
		SettingSetCoefficient(i, 2, 0);
		SettingSetCoefficient(i, 3, 0);
		SettingSetCoefficient(i, 4, 0);
		SettingSetCompensated(i, 0);
		SettingSetCompensation(i, 0);
		SettingSetSampleChange(i, 0);
		SettingSetSensorUnit(i, 0x20202020);
	}
	
	SettingSetCounterReset(0x00);
	SettingSetRTM(0);
}

// Device Serial Number
void SettingsSetSerial(uint16_t serial)
{
	while(!eeprom_is_ready()){}
	EEPROMWrite16bits((uint8_t*)SETTING_SERIAL, serial);
}
uint16_t SettingsGetSerial(void)
{
	while(!eeprom_is_ready()){}
	return EEPROMRead6bits((uint8_t*)SETTING_SERIAL);
}

// Firmware Version
void SettingsSetFWversion(uint16_t fwversion)
{
	while(!eeprom_is_ready()){}
	EEPROMWrite16bits((uint8_t*)SETTING_FWVERSION, fwversion);
}
uint16_t SettingsGetFWversion(void)
{
	while(!eeprom_is_ready()){}
	return EEPROMRead6bits((uint8_t*)SETTING_FWVERSION);
}

// Build Date
void SettingsSetBuildDate(DateTime_t build_date)
{
	while(!eeprom_is_ready()){}
	EEPROMWriteDatetime((uint8_t*)SETTING_BUILDDATE, build_date);
}
DateTime_t SettingsGetBuildDate(void)
{
	while(!eeprom_is_ready()){}
	return EEPROMReadDatetime((uint8_t*)SETTING_BUILDDATE);
}

// Last setting datetime
void SettingSetLastSetting(DateTime_t last_setting)
{
	while(!eeprom_is_ready()){}
	EEPROMWriteDatetime((uint8_t*)SETTING_LASTSETTING, last_setting);
}
DateTime_t SettingGetLastSetting(void)
{
	while(!eeprom_is_ready()){}
	return EEPROMReadDatetime((uint8_t*)SETTING_LASTSETTING);
}

// Check Point datetime
void SettingSetCheck(DateTime_t check_point)
{
	while(!eeprom_is_ready()){}
	EEPROMWriteDatetime((uint8_t*)SETTING_CHECK_DATE, check_point);
}
DateTime_t SettingGetCheck(void)
{
	while(!eeprom_is_ready()){}
	return EEPROMReadDatetime((uint8_t*)SETTING_CHECK_DATE);
}

// Sensor Type
void SettingSetSensorType(uint8_t channel, uint8_t type)
{
	while(!eeprom_is_ready()){}
	eeprom_write_byte((uint8_t*)(SETTING_SENSOR_TYPE + channel), type);
	
}
uint8_t SettingGetSensorType(uint8_t channel)
{
	while(!eeprom_is_ready()){}
	return eeprom_read_byte((uint8_t*)(SETTING_SENSOR_TYPE + channel));
}

// Sensor Equation
void SettingSetSensorEquation(uint8_t channel, uint8_t equation)
{
	while(!eeprom_is_ready()){}
	eeprom_write_byte((uint8_t*)(SETTING_SENSOR_EQUATION + channel), equation);
	
}
uint8_t SettingGetSensorEquation(uint8_t channel)
{
	while(!eeprom_is_ready()){}
	return eeprom_read_byte((uint8_t*)(SETTING_SENSOR_EQUATION + channel));
}

// Sensor settle
void SettingSetSensorSettle(uint8_t channel, uint16_t settle)
{
	while(!eeprom_is_ready()){}
	EEPROMWrite16bits((uint8_t*)(SETTING_SENSOR_SETTLE + 2 * channel), settle);
	
}
uint16_t SettingGetSensorSettle(uint8_t channel)
{
	while(!eeprom_is_ready()){}
	return EEPROMRead6bits((uint8_t*)(SETTING_SENSOR_SETTLE + 2 * channel));
}

// Excite Start
void SettingSetExciteStart(uint8_t channel, uint16_t start)
{
	while(!eeprom_is_ready()){}
	EEPROMWrite16bits((uint8_t*)(SETTING_EXCITE_START + 2 * channel), start);
	
}
uint16_t SettingGetExciteStart(uint8_t channel)
{
	while(!eeprom_is_ready()){}
	return EEPROMRead6bits((uint8_t*)(SETTING_EXCITE_START + 2 * channel));
}

// Excite Stop
void SettingSetExciteStop(uint8_t channel, uint16_t stop)
{
	while(!eeprom_is_ready()){}
	EEPROMWrite16bits((uint8_t*)(SETTING_EXCITE_STOP + 2 * channel), stop);
	
}
uint16_t SettingGetExciteStop(uint8_t channel)
{
	while(!eeprom_is_ready()){}
	return EEPROMRead6bits((uint8_t*)(SETTING_EXCITE_STOP + 2 * channel));
}

// Cycles
void SettingSetCycles(uint8_t channel, uint16_t cycle)
{
	while(!eeprom_is_ready()){}
	EEPROMWrite16bits((uint8_t*)(SETTING_CYCLES + 2 * channel), cycle);
	
}
uint16_t SettingGetCycles(uint8_t channel)
{
	while(!eeprom_is_ready()){}
	return EEPROMRead6bits((uint8_t*)(SETTING_CYCLES + 2 * channel));
}

// Coefficient for channels
void SettingSetCoefficient(uint8_t channel, uint8_t coeff, float value)
{
	while (!eeprom_is_ready()){}
	EEPROMWrite32bits((uint8_t*)(SETTING_COEFFICIENT + 20 * channel + 4 * coeff), FloatToU32(value));
}
float SettingGetCoefficient(uint8_t channel, uint8_t coeff)
{
	while(!eeprom_is_ready()){}
	return U32ToFloat(EEPROMRead32bits((uint8_t*)(SETTING_COEFFICIENT + 20 * channel + 4 * coeff)));
}

// Compensation
void SettingSetCompensated(uint8_t channel, uint8_t enable)
{
	while (!eeprom_is_ready()){}
	eeprom_write_byte((uint8_t*)(SETTING_COMPENSATED + channel), enable);
}
uint8_t SettingGetCompensation(uint8_t channel)
{
	while(!eeprom_is_ready()){}
	return eeprom_read_byte((uint8_t *)(SETTING_COMPENSATED + channel));
}

/* Set the compensation channel */
void SettingSetCompensation(uint8_t channel, uint8_t source)
{
	while (!eeprom_is_ready()){}
	eeprom_write_byte((uint8_t*)(SETTING_COMPENSATION + channel), source);
}
uint8_t SettingGetCompensated(uint8_t channel)
{
	while(!eeprom_is_ready()){}
	return eeprom_read_byte((uint8_t*)(SETTING_COMPENSATION + channel));
}

// Measure Interval
void SettingSetMeasureInterval(uint16_t interval)
{
	while(!eeprom_is_ready()){}
	EEPROMWrite16bits((uint8_t*)(SETTING_MEASURE_INTERVAL), interval);
}
uint16_t SettingGetMeasureInterval(void)
{
	while(!eeprom_is_ready()){}
	return EEPROMRead6bits((uint8_t*)(SETTING_MEASURE_INTERVAL));
}

// Counter reset on record
void SettingSetCounterReset(uint8_t reset)
{
	while(!eeprom_is_ready()){}
	eeprom_write_byte((uint8_t*)(SETTING_COUNTER_RESET), reset);
}

uint8_t SettingGetCounterReset(void)
{
	while(!eeprom_is_ready()){}
	return eeprom_read_byte((uint8_t*)(SETTING_COUNTER_RESET));
}

// Coefficient for channels
void SettingSetSampleChange(uint8_t channel, float value)
{
	while (!eeprom_is_ready()){}
	EEPROMWrite32bits((uint8_t*)(SETTING_SAMPLE_CHANGE + 4 * channel), FloatToU32(value));
}
float SettingGetSampleChange(uint8_t channel)
{
	while(!eeprom_is_ready()){}
	return U32ToFloat(EEPROMRead32bits((uint8_t*)(SETTING_SAMPLE_CHANGE + 4 * channel)));
}

// Sensor Units
void SettingSetSensorUnit(uint8_t channel, uint32_t unit)
{
	while (!eeprom_is_ready()){}
	EEPROMWrite32bits((uint8_t*)(SETTING_SENSOR_UNIT + 4 * channel), unit);
}

uint32_t SettingGetSensorUnit(uint8_t channel)
{
	while(!eeprom_is_ready()){}
	return EEPROMRead32bits((uint8_t*)(SETTING_SENSOR_UNIT + 4 * channel));
}

// Real Time Measure Enable
void SettingSetRTM(uint8_t enable)
{
	while(!eeprom_is_ready()){}
	eeprom_write_byte((uint8_t*)(SETTING_RTM_ENABLE), enable);
}
uint8_t SettingGetRTM(void)
{
	while(!eeprom_is_ready()){}
	return eeprom_read_byte((uint8_t*)(SETTING_RTM_ENABLE));
}

// Serial Mode: USB:0 RS232:1 RS485:2
void SettingSetSerialMode(uint8_t mode)
{
	while(!eeprom_is_ready()){}
	eeprom_write_byte((uint8_t*)(SETTING_SERIAL_MODE), mode);
}
uint8_t SettingGetSerialMode(void)
{
	while(!eeprom_is_ready()){}
	return eeprom_read_byte((uint8_t*)(SETTING_SERIAL_MODE));
}

// Slave Serial Mode RS485 Radio
void SettingSetSlaveSerialMode(uint8_t mode)
{
	while(!eeprom_is_ready()){}
	eeprom_write_byte((uint8_t*)(SETTING_SLAVE_SERIAL_MODE), mode);
}

uint8_t SettingGetSlaveSerialMode(void)
{
	while(!eeprom_is_ready()){}
	return eeprom_read_byte((uint8_t*)(SETTING_SLAVE_SERIAL_MODE));
}

// ModeBus Role
void SettingSetRole(uint8_t role)
{
	while(!eeprom_is_ready()){}
	eeprom_write_byte((uint8_t*)(SETTING_MODEBUS_ROLE), role);
}
uint8_t SettingGetRole(void)
{
	while(!eeprom_is_ready()){}
	return eeprom_read_byte((uint8_t*)(SETTING_MODEBUS_ROLE));
}

// Slave List
void SettingSetSlaveList(uint8_t slave_number, uint16_t interval)
{
	while(!eeprom_is_ready()){}
	EEPROMWrite16bits((uint8_t*)(SETTING_SLAVE_LIST + 2 * slave_number), interval);
}
uint16_t SettingGetSlaveList(uint8_t slave_number)
{
	while(!eeprom_is_ready()){}
	return EEPROMRead6bits((uint8_t*)(SETTING_SLAVE_LIST + 2 * slave_number));
}

// Slave Number
void SettingSetSlaveNumber(uint8_t slave_number)
{
	while(!eeprom_is_ready()){}
	eeprom_write_byte((uint8_t*)(SETTING_SLAVE_NUMBER), slave_number);
}
uint8_t SettingGetSlaveNumber(void)
{
	while(!eeprom_is_ready()){}
	return eeprom_read_byte((uint8_t*)(SETTING_SLAVE_NUMBER));
}

// Enable Record
void SettingSetEnableRecord(uint8_t enable)
{
	while(!eeprom_is_ready()){}
	eeprom_write_byte((uint8_t*)(SETTING_ENABLE_RECORD), enable);
}

uint8_t SettingGetEnableRecord(void)
{
	while(!eeprom_is_ready()){}
	return eeprom_read_byte((uint8_t*)(SETTING_ENABLE_RECORD));
}

// Enable Synchronize
void SettingSetEnableSynchronize(uint8_t enable)
{
	while(!eeprom_is_ready()){}
	eeprom_write_byte((uint8_t*)(SETTING_ENABLE_SYNCHRONIZE), enable);
}

uint8_t SettingGetEnableSynchronize(void)
{
	while(!eeprom_is_ready()){}
	return eeprom_read_byte((uint8_t*)(SETTING_ENABLE_SYNCHRONIZE));
}

// Enable Fast Read
void SettingSetEnableFastRead(uint8_t enable)
{
	while(!eeprom_is_ready()){}
	eeprom_write_byte((uint8_t*)(SETTING_ENBALE_FASTREAD), enable);	
}

uint8_t SettingGetEnableFastRead(void)
{
	while(!eeprom_is_ready()){}
	return eeprom_read_byte((uint8_t*)(SETTING_ENBALE_FASTREAD));
}