/*
 * setting.h
 *
 * Created: 20/12/2019 10:12:17 AM
 *  Author: xiaofan
 */ 


#ifndef SETTING_H_
#define SETTING_H_

#include "../RTC/DS3232.h"

void been_set(void);

// Return true if logger just been set
uint8_t has_set(void);

uint8_t SettingsInitComplete(void);

void ResetReadSetting(void);

// Device Serial Number
void SettingsSetSerial(uint16_t serial);
uint16_t SettingsGetSerial(void);

// Firmware Version
void SettingsSetFWversion(uint16_t fwversion);
uint16_t SettingsGetFWversion(void);

// Build Date
void SettingsSetBuildDate(DateTime_t build_date);
DateTime_t SettingsGetBuildDate(void);

// Last setting datetime
void SettingSetLastSetting(DateTime_t last_setting);
DateTime_t SettingGetLastSetting(void);

// Check Point datetime
void SettingSetCheck(DateTime_t check_point);
DateTime_t SettingGetCheck(void);

// Sensor Type
void SettingSetSensorType(uint8_t channel, uint8_t type);
uint8_t SettingGetSensorType(uint8_t channel);

// Sensor Equation
void SettingSetSensorEquation(uint8_t channel, uint8_t equation);
uint8_t SettingGetSensorEquation(uint8_t channel);

// Sensor Settle
void SettingSetSensorSettle(uint8_t channel, uint16_t settle);
uint16_t SettingGetSensorSettle(uint8_t channel);

// Excite Start
void SettingSetExciteStart(uint8_t channel, uint16_t start);
uint16_t SettingGetExciteStart(uint8_t channel);

// Excite Stop
void SettingSetExciteStop(uint8_t channel, uint16_t stop);
uint16_t SettingGetExciteStop(uint8_t channel);

// Cycles
void SettingSetCycles(uint8_t channel, uint16_t cycle);
uint16_t SettingGetCycles(uint8_t channel);

// Coefficient
void SettingSetCoefficient(uint8_t channel, uint8_t coeff, float value);
float SettingGetCoefficient(uint8_t channel, uint8_t coeff);

// Compensation
void SettingSetCompensated(uint8_t channel, uint8_t enable);
uint8_t SettingGetCompensated(uint8_t channel);

// Compensated
void SettingSetCompensation(uint8_t channel, uint8_t source);
uint8_t SettingGetCompensation(uint8_t channel);

// Measure Interval
void SettingSetMeasureInterval(uint16_t interval);
uint16_t SettingGetMeasureInterval(void);

// Counter reset on record
void SettingSetCounterReset(uint8_t reset);
uint8_t SettingGetCounterReset();

// Sample Change Value
void SettingSetSampleChange(uint8_t channel, float value);
float SettingGetSampleChange(uint8_t channel);

// Sensor Units
void SettingSetSensorUnit(uint8_t channel, uint32_t unit);
uint32_t SettingGetSensorUnit(uint8_t channel);

// Real Time Measure Enable
void SettingSetRTM(uint8_t enable);
uint8_t SettingGetRTM(void);

// Serial Mode: USB RS232 RS485 Radio
void SettingSetSerialMode(uint8_t mode);
uint8_t SettingGetSerialMode(void);

// Slave Serial Mode RS485 Radio
void SettingSetSlaveSerialMode(uint8_t mode);
uint8_t SettingGetSlaveSerialMode(void);

// ModeBus Role
void SettingSetRole(uint8_t role);
uint8_t SettingGetRole(void);

// Slave List
void SettingSetSlaveList(uint8_t slave_number, uint16_t interval);
uint16_t SettingGetSlaveList(uint8_t slave_number);

// Slave Number
void SettingSetSlaveNumber(uint8_t slave_number);
uint8_t SettingGetSlaveNumber(void);

// Enable Record
void SettingSetEnableRecord(uint8_t enable);
uint8_t SettingGetEnableRecord(void);

// Enable Synchronize
void SettingSetEnableSynchronize(uint8_t enable);
uint8_t SettingGetEnableSynchronize(void);

// Enable Fast Read
void SettingSetEnableFastRead(uint8_t enable);
uint8_t SettingGetEnableFastRead(void);
#endif /* SETTING_H_ */