/*
 * eeprom_util.h
 *
 * Created: 20/12/2019 11:12:49 AM
 *  Author: xiaofan
 */ 


#ifndef EEPROM_UTIL_H_
#define EEPROM_UTIL_H_

#include "../RTC/DS3232.h"

void EEPROMWrite16bits(uint8_t* address, uint16_t data);

uint16_t EEPROMRead6bits(uint8_t* address);

void EEPROMWrite32bits(uint8_t* address, uint32_t data);

uint32_t EEPROMRead32bits(uint8_t* address);

void EEPROMWriteDatetime(uint8_t* address, DateTime_t datetimes);

DateTime_t EEPROMReadDatetime(uint8_t* address);


#endif /* EEPROM_UTIL_H_ */