/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: read.c
 * Description: Sensor excitation and reading driver for Sigra Logger V10
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

#include "read.h"
#include "../../main.h"
#include "../../common/state.h"
#include "../../common/timer.h"
#include "../Serial/usbuart.h"
#include "../Setting/setting.h"
#include "../Power/battery.h"

// Current State
static uint8_t current_sensor_channel;
static uint8_t current_sensor_type;
static uint8_t current_sensor_equation;
static uint8_t current_measure_type; 
static uint8_t current_measure_state;  
static uint8_t current_read_state;
static uint8_t next_read_state;

// Timer
static uint8_t discharge_tick;
static unsigned int settle_time;
static volatile unsigned int previous;
static volatile uint8_t first_icp;
static unsigned int timer;
static uint8_t timeout;
static unsigned int cycles;

// Reading
static volatile unsigned int capture_count;
static volatile float accumulator;
static uint8_t excite_done;
static unsigned int  iter_dn_c;
static unsigned int  iter_up_c;
static volatile unsigned int  step_dn_c;
static volatile unsigned int  step_up_c;
static volatile unsigned int  cycle_dn_c;
static volatile unsigned int  cycle_up_c;
static volatile unsigned int  next_step_c;
static volatile unsigned int  chirp_steps;

// Raw Data
static float raw_reading[NUM_CHANNELS];
static float real_reading[NUM_CHANNELS];

// Counter
static unsigned long count[NUM_CHANNELS];
static uint8_t count_update;
static uint8_t last_count_store;
static uint8_t count1;
static uint8_t count2;
static uint8_t count3;
 
 // Set Up
static void SetupDischarge(void);
static void Discharge(void);
static void Settle(void);
static void SetMultiplexerOn(void);
static void SetupMeasurment(void);
static void SetupExcitation(void);

// Analog
static void ReadAnalogPowerUp(void);
static void ReadAnalogPowerDown(void);

// Read Walk
static void MeasureInit(void);
static void MeasureIdle(void);
static void MeasureMeasure(void);
static void MeasureCounter(void);

// Type of measurement
static void MeasureVoltage(void);
static void MeasureResistance(void);
static void MeasureFrequency(void);
static void MeasureFinalise(void);

// Reading
static float ADCRead(void);
static void SetOutputHighz(uint8_t channel);
static void SetOutputHigh(uint8_t channel);
static void SetOutputLow(uint8_t channel);
static uint8_t MapChannelToInput(uint8_t channel);

static void SetRawReading(uint8_t chan, float raw_data);

void ReadInit(void) {
	// loggerdata    = &physical_loggerdata;
	current_read_state    = INIT;
	current_measure_state = IDLE;
	//flags         = 0;
	//count_update  = 0;
	for (uint8_t chan = 0; chan < NUM_CHANNELS; chan++) {
		SetOutputHighz(chan);
	}
	
	MEASURE_CONTROL_B();

	APOWER_OFF();
	RESISTOR_OFF();
	VWLOAD_OFF();
}

void ReadWalk(void) {
	switch (current_read_state) {
		case INIT    : MeasureInit();    break;
		case IDLE    : MeasureIdle();    break;
		case DISCHARGE:					  break;
		case MEASURE : MeasureMeasure(); break;
		case BUSY    :					  break;
	}
}

void ReadRun(void) {
	uint8_t input_pins = INPS_PINS();
	uint8_t count_pins = (input_pins & count_update);

	count3 = count2;
	count2 = count1;
	count1 = count_pins ^ last_count_store;

	if (count1) {
		uint8_t count_change = (count1 & count2 & count3);
		if (count_change) {
			input_pins = count_change & last_count_store;

			for (uint8_t chan = 0; chan < NUM_CHANNELS; chan++) {
				if (input_pins & MapChannelToInput(chan)) {
					count[chan]++; 
					count_update |= (1 << chan);
				}
			}
			last_count_store ^= count_change;
		}
	}
	
	if (current_read_state == DISCHARGE) {
		Discharge();
	}
	if (current_read_state == SETTLE) {
		Settle();
	}
	if (timer) {
		if ((--timer) == 0) {
			timeout = TRUE;
		}
	}
}

/*******************************************************************************
* Function Name		: ReadSensor
* Description		: change state into READ for certain channel
* Input				: sensor_channel
* Return			: void
*******************************************************************************/
void ReadSensor(uint8_t sensor_channel) {
	current_sensor_channel = sensor_channel;
	current_sensor_type = SettingGetSensorType(sensor_channel);
	current_sensor_equation = SettingGetSensorEquation(sensor_channel);

	if (!current_sensor_type == ST_UNUSED) {
		if (current_sensor_type == ST_VIBRATING_WIRE || current_sensor_type == ST_FREQUENCY) {
			ReadAnalogPowerUp();
			current_measure_type = ST_FREQUENCY;
		} else {
			current_measure_type = current_sensor_type;
		}
		
		if (current_sensor_type == ST_COUNTER) {
			count_update |= MapChannelToInput(sensor_channel);
			
			PORTK = INPS_MASK & count_update;
			uint8_t didr_mask = 0;
			didr_mask &= ~(INPS_MASK & (count_update | 0));
			INPS_DIDR() = didr_mask;
		}
		
		SetupDischarge(); // was setup_starting_state();
		current_read_state = DISCHARGE;
	}
}

/*******************************************************************************
* Function Name		: GetRawReading
* Description		: get the raw reading for sensors
* Input				: void
* Return			: void
*******************************************************************************/
float* GetRawReading(void) {
	return raw_reading;
}

void ResetRawReading(void) {
	for (uint8_t chan = 0; chan < NUM_CHANNELS; chan++) {
		raw_reading[chan] = 0;
	}
}

void SetRealReading(uint8_t channel, float reading) {
	real_reading[channel] = reading;
}

float* GetRealReading(void) {
	return real_reading;
}

/*******************************************************************************
* Function Name		: GetReadStatus
* Description		: get the current read state of logger
* Input				: void
* Return			: void
*******************************************************************************/
uint8_t GetReadStatus(void) {
	return current_read_state;
}

/*******************************************************************************
* Function Name		: ReadAnalogPowerUp
* Description		: power up the analog, set output low if measuring VW
* Input				: void
* Return			: void
*******************************************************************************/
static void ReadAnalogPowerUp(void) {
	uint8_t type;
	APOWER_ON();
	// output_read_power_up(); // handle aux power if required
	for (uint8_t chan = 0; chan < NUM_CHANNELS; chan++) {
		type = SettingGetSensorType(chan);
		switch (type) {
			// VW always starts low
			case ST_VIBRATING_WIRE: 
				SetOutputLow(chan); 
				break;
			// do nothing for OUTPUTS here output.c handles OUTPUTS
			case ST_OUTPUT:
				break;
			// do nothing for ALARM_OUTPUT here log.c handles ALARM_OUTPUT (via output.c)
			case ST_ALARMOUTPUT:
				break;
			// everything else
			default:
				SetOutputHighz(chan);
		}
	}
}

/*******************************************************************************
* Function Name		: read_analog_power_down
* Description		: power down the analog
* Input				: void
* Return			: void
*******************************************************************************/
static void ReadAnalogPowerDown(void) {
	APOWER_OFF();
	RESISTOR_OFF();
	VWLOAD_OFF();
	// output_read_power_down(); // handle aux power if required
}

/*******************************************************************************
* Function Name		: setup_discharge
* Description		: ready to discharge
* Input				: void
* Return			: void
*******************************************************************************/
static void SetupDischarge(void) {
	MUX_OFF();
	VWLOAD_ON();
	current_read_state = DISCHARGE;
	discharge_tick = 4;
}

/*******************************************************************************
* Function Name		: Discharge
* Description		: discharge the logger, normally 4 ticks
* Input				: void
* Return			: void
*******************************************************************************/
static void Discharge(void) {
	if (discharge_tick) {
		if ((--discharge_tick) == 0) {
			VWLOAD_OFF();
			if (current_sensor_type == ST_VIBRATING_WIRE)  {
				SetupExcitation(); 
			} else {
				SetupMeasurment();
			}
		}
	}
}

/*******************************************************************************
* Function Name		: Settle
* Description		: settle the measurement
* Input				: void
* Return			: void
*******************************************************************************/
static void Settle(void) {
	if (settle_time) {
		settle_time--;
	}
	if (settle_time == 0) {
		current_read_state = next_read_state;
	}
}

/*******************************************************************************
* Function Name		: SetMultiplexerOn
* Description		: set up mux channel
* Input				: void
* Return			: void
*******************************************************************************/
static void SetMultiplexerOn(void) {
	uint8_t mux_channel = 0;

	if ((current_sensor_channel < NUM_CHANNELS) &&
	(current_sensor_type != ST_EXTERNAL) &&
	(current_sensor_type != ST_COUNTER) &&
	(current_sensor_type != ST_INPUT)) {
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			switch (current_sensor_channel) {
				case 0 : mux_channel = MUX_CHANNEL_0; break;
				case 1 : mux_channel = MUX_CHANNEL_1; break;
				case 2 : mux_channel = MUX_CHANNEL_2; break;
				case 3 : mux_channel = MUX_CHANNEL_3; break;
				case 4 : mux_channel = MUX_CHANNEL_4; break;
				case 5 : mux_channel = MUX_CHANNEL_5; break;
				case 6 : mux_channel = MUX_CHANNEL_6; break;
				case 7 : mux_channel = MUX_CHANNEL_7; break;
			}
			MUX_OFF(); // set inhibit
			if (mux_channel & 0x01) MUX_A_HI(); else MUX_A_LO();
			if (mux_channel & 0x02) MUX_B_HI(); else MUX_B_LO();
			if (mux_channel & 0x04) MUX_C_HI(); else MUX_C_LO();
			MUX_ON(); // clear inhibit
		}
	}
}

/*******************************************************************************
* Function Name		: SetupMeasurment
* Description		: set up measurement, except VW
* Input				: void
* Return			: void
*******************************************************************************/
static void SetupMeasurment(void) {
	current_measure_state = IDLE;
	SetMultiplexerOn(); // always ?? check for VW, check for ext
	if (current_measure_type == ST_RESISTANCE) {
		RESISTOR_ON();
	} else {
		RESISTOR_OFF();
	}
	
	if ((current_sensor_channel < NUM_CHANNELS) && (current_sensor_type != ST_EXTERNAL)) {
		current_read_state = SETTLE;
		next_read_state = MEASURE;
		settle_time = BigMilliseconds(SettingGetSensorSettle(current_sensor_channel)) + 1;
	} else {
		current_read_state = MEASURE;
	}
}

/*******************************************************************************
* Function Name		: SetupExcitation
* Description		: excite the VW
* Input				: void
* Return			: void
*******************************************************************************/
static void SetupExcitation(void) {
	unsigned int i, start_c, stop_c, step_c, total_step_c = 1024;
	float acc_step_c, err = 1.0, erri;
	unsigned int cycle_size = 0;

	// find start_c and stop_c from settings

	start_c = (unsigned int) (((float) F_CPU * 0.5) / SettingGetExciteStart(current_sensor_channel));
	stop_c = (unsigned int) (((float) F_CPU * 0.5) / SettingGetExciteStop(current_sensor_channel));

	acc_step_c = (float)(start_c - stop_c) / (float) total_step_c; // float step count
	step_c = (start_c - stop_c) / total_step_c; // step count
	step_dn_c = step_c;
	step_up_c = step_c + 1;

	if (step_c == 0) // special case!
	{
		for (i=1; i<=50; i++)
		{
			erri = acc_step_c*i - (unsigned int)(acc_step_c*i);
			if (erri < err) {
				err = erri;
				cycle_size = i;
			}
		}
		cycle_up_c = iter_up_c = (unsigned int)(acc_step_c * cycle_size);
		cycle_dn_c = iter_dn_c = cycle_size - iter_up_c;
	} else {
		if ((acc_step_c - step_c) > 0.5) {
			cycle_dn_c = iter_dn_c = 1;
			cycle_up_c = iter_up_c = (unsigned int) ((float)((float)acc_step_c - (float)step_dn_c)/((float)step_up_c - (float)acc_step_c));
		} else {
			cycle_dn_c = iter_dn_c = (unsigned int) ((float)((float)step_up_c - (float)acc_step_c)/((float)acc_step_c - (float)step_dn_c));
			cycle_up_c = iter_up_c = 1;
		}
	}

	// calc chirp steps
	chirp_steps = total_step_c;

	next_step_c = start_c - step_dn_c;
	cycle_dn_c--;

	// set default values
	MUX_OFF();
	VWLOAD_ON(); // new in V90
	current_read_state    = EXCITE;
	current_measure_state = IDLE;
	excite_done   = FALSE;
	CHIRP_OCR()   = TCNT3 + next_step_c;            // first half cycle period
	if (cycle_dn_c == 0) {
		next_step_c  -= step_up_c;	// next step is smaller
		cycle_up_c--;
	} else {
		next_step_c  -= step_dn_c;	// next step is smaller
		cycle_dn_c--;
	}

	 CHIRP_INT_CLR();
	 CHIRP_ON();                           // enable rest of chirp
}

static void MeasureInit(void) {	
	if (SettingsInitComplete()) {
		//for (uint8_t chan = 0; chan < NUM_CHANNELS; chan++)
		//{
			 //previous_sensor_type[chan] = setting_get_sensor_type(chan);
			 //
			 //we only want to read count from RAM if we are doing a power up init with counters defined
			 //if counters are defined later (in an update) we set the count to zer o
			 //
			 //if (settings_sensor_type(chan) == ST_COUNTER)
			 //{
				 //count[chan] = clock_read_long_from_ram(COUNT_RAM + (chan * 4));
			 //}
		//}
		current_read_state = IDLE;
	}
}

static void MeasureIdle(void) {
	return;
}

/*******************************************************************************
* Function Name		: measure_measure
* Description		: switch different kind of measurement
* Input				: void
* Return			: void
*******************************************************************************/
static void MeasureMeasure(void) {
	switch (current_measure_type) {
		case ST_UNUSED           : MeasureFinalise();    break;
		case ST_VIBRATING_WIRE   : MeasureFrequency();   break;
		//case ST_OUTPUT         : output();      break;
		case ST_VOLTAGE          : MeasureVoltage();     break;
		case ST_FREQUENCY	     : MeasureFrequency();   break;
		case ST_RESISTANCE       : MeasureResistance();  break;
		case ST_COUNTER		     : MeasureCounter();     break;
		//case ST_EXTERNAL       : external();    break;
		//case ST_INPUT          : input();       break;
		//case ST_CALCULATION    : calculation(); break;
		//case ST_ALARMOUTPUT    : output();      break;
		default					 : MeasureFinalise();
	}
}

/*******************************************************************************
* Function Name		: MeasureVoltage
* Description		: measure the voltage to current channel
* Input				: void
* Return			: void
*******************************************************************************/
static void MeasureVoltage(void) {
	if (current_sensor_equation == BATTERY_VOLTAGE_EQN) {
		SetRawReading(current_sensor_channel, BatteryGetVolts());
		MeasureFinalise();
	} else {
		if (current_measure_state == IDLE) {
			current_measure_state = BUSY;
			accumulator   = 0;
			previous = 0;
			if (SettingGetEnableFastRead()) {
				capture_count = ADC_AVERAGES / 2;
			} else {
				capture_count = ADC_AVERAGES;
			}
		}
		accumulator += ADCRead();
		if ((--capture_count) == 0) {
			if (SettingGetEnableFastRead()) {
				SetRawReading(current_sensor_channel, 2 * accumulator / ADC_AVERAGES);
			} else {
				SetRawReading(current_sensor_channel, accumulator / ADC_AVERAGES);
			}
			MeasureFinalise();
		}
	}
}

/*******************************************************************************
* Function Name		: MeasureResistance
* Description		: measure the resistance to current channel
* Input				: void
* Return			: void
*******************************************************************************/
static void MeasureResistance(void) {
	if (current_measure_state == IDLE) {
		current_measure_state = BUSY;
		accumulator   = 0;
		capture_count = ADC_AVERAGES;
	}
	
	accumulator += ADCRead();

	if ((--capture_count) == 0) {
		// then for reading in ohms we need
		SetRawReading(current_sensor_channel, ((4750.0 * accumulator) / ((ADC_AVERAGES * ((float) 65536.0)) - accumulator)) - SWITCH_RESISTANCE);
		MeasureFinalise();
	}
}

/*******************************************************************************
* Function Name		: MeasureFrequency
* Description		: measure the frequency to current channel, using interrupt
*					  need to power up analog device first
* Input				: void
* Return			: void
*******************************************************************************/
static void MeasureFrequency(void) {
	if (current_measure_state == IDLE) {
		if (SettingGetEnableFastRead()) {
			capture_count = SettingGetCycles(current_sensor_channel) / 2;
		} else {
			capture_count = SettingGetCycles(current_sensor_channel);
		}
		cycles        = capture_count;
		// timer set to the period of the minimum frequency (2ms)
		// times the number of measure cycles (cycles)
		// divided by the run period (1.25)
		// so we have 2 * cycles / 1.25 approx 2 * cycles;

		timer         = 2 * cycles;
		timeout       = FALSE;
		current_measure_state = BUSY;
		first_icp     = TRUE;
		MEASURE_INT_CLR();
		MEASURE_ON();        // enable the icp interrupt
	} else {
		if (!capture_count) {
			SetRawReading(current_sensor_channel,((((float) F_CPU) * 10 * (cycles)) / accumulator));
			MeasureFinalise();
		}
		if (timeout) {
			MEASURE_OFF();
			SetRawReading(current_sensor_channel,0);
			MeasureFinalise();
		}
	}
}

/*******************************************************************************
* Function Name		: MeasureCounter
* Description		: return the record of paulse count
* Input				: void
* Return			: void
*******************************************************************************/
static void MeasureCounter(void) {
	SetRawReading(current_sensor_channel,count[current_sensor_channel]);

	if (SettingGetCounterReset() & (1 << current_sensor_channel)) {
		count[current_sensor_channel] = 0;
		count_update |= (1 << current_sensor_channel);
	}
	MeasureFinalise();
}

/*******************************************************************************
* Function Name		: measure_finalise
* Description		: finalize the measurement
* Input				: void
* Return			: void
*******************************************************************************/
static void MeasureFinalise(void) {
	if (current_sensor_type == ST_VIBRATING_WIRE) {
		// this is not an OUTPUT so we have control of the pin
		SetOutputLow(current_sensor_channel);
		ReadAnalogPowerDown();
	}
	current_measure_state = IDLE;
	current_read_state    = IDLE;
	MUX_OFF();            // turn MUX off
	VWLOAD_OFF();
}

/*******************************************************************************
* Function Name		: ADCRead
* Description		: read from analog device
* Input				: void
* Return			: readings
*******************************************************************************/
static float ADCRead(void) {
	unsigned char i;
	unsigned int data = 0;

	ADS8320_CLK_LO();
	ADS8320_CS_LO();

	i = 0;
	while (i < 22) {
		data <<= 1;
		
		asm("nop");
		if (ADS8320_DOUT()) {
			data |= 0x01;
		}
		ADS8320_CLK_HI();
		asm("nop");
		
		i++;
		ADS8320_CLK_LO();
	}
	ADS8320_CS_HI();

	return ((float) data);
}

/*******************************************************************************
* Function Name		: SetRawReading
* Description		: set the raw reading for channel
* Input				: void
* Return			: void
*******************************************************************************/
static void SetRawReading(uint8_t chan, float raw_data) {
	raw_reading[chan] = raw_data;
}

/*******************************************************************************
* Function Name		: SetOutputHighz
* Description		: SetOutputHighz
* Input				: channel
* Return			: void
*******************************************************************************/
static void SetOutputHighz(uint8_t channel) {
	switch (++channel) {
		case OUT1_CHAN : IOPE1_OFF(); break;
		case OUT2_CHAN : IOPE2_OFF(); break;
		case OUT3_CHAN : IOPE3_OFF(); break;
		case OUT4_CHAN : IOPE4_OFF(); break;
	}
}

/*******************************************************************************
* Function Name		: set_output_high
* Description		: set_output_high
* Input				: channel
* Return			: void
*******************************************************************************/
static void SetOutputHigh(uint8_t channel) {
	// Note IOPS1_HI sets the IOPS1 signal low to give a HIGH on the output pin
	switch (++channel) {
		case OUT1_CHAN : IOPS1_HI(); IOPE1_ON(); break;
		case OUT2_CHAN : IOPS2_HI(); IOPE2_ON(); break;
		case OUT3_CHAN : IOPS3_HI(); IOPE3_ON(); break;
		case OUT4_CHAN : IOPS4_HI(); IOPE4_ON(); break;
	}
}

/*******************************************************************************
* Function Name		: SetOutputLow
* Description		: SetOutputLow
* Input				: channel
* Return			: void
*******************************************************************************/
static void SetOutputLow(uint8_t channel) {
	// Note IOPS1_LO sets the IOPS1 signal high to give a LOW on the output pin
	switch (++channel) {
		case OUT1_CHAN : IOPS1_LO(); IOPE1_ON(); break;
		case OUT2_CHAN : IOPS2_LO(); IOPE2_ON(); break;
		case OUT3_CHAN : IOPS3_LO(); IOPE3_ON(); break;
		case OUT4_CHAN : IOPS4_LO(); IOPE4_ON(); break;
	}
}

/*******************************************************************************
* Function Name		: MapChannelToInput
* Description		: map channel number into input pins
* Input				: channel
* Return			: void
*******************************************************************************/
static uint8_t MapChannelToInput(uint8_t channel) {
	switch (channel + 1) {
		case INP1_CHAN: 
			return INP1_BIT;
		case INP2_CHAN: 
			return INP2_BIT;
		case INP3_CHAN: 
			return INP3_BIT;
		case INP4_CHAN: 
			return INP4_BIT;
		default: 
			return 0;
	}
	
	return 0;
}

/*******************************************************************************
* Function Name		: MEASURE_ISR
* Description		: interrupt to measure data
*******************************************************************************/
MEASURE_ISR {
  unsigned int current;

  current = ICR1;
  if (first_icp) {
	  first_icp = FALSE;
	  accumulator = 0;
  } else {
	  accumulator += current;
	  if (current < previous) accumulator += 65536UL;
	  accumulator -= previous;
	  if (capture_count) capture_count--;
	  if (capture_count == 0) MEASURE_OFF();
  }
  previous = current;
}

/*******************************************************************************
* Function Name		: CHIRP_ISR
* Description		: interrupt to excite VW
*******************************************************************************/
CHIRP_ISR {
	CHIRP_OCR() += next_step_c;
	if (cycle_dn_c == 0) {
		if (cycle_up_c == 0) {
			cycle_up_c = iter_up_c;
			cycle_dn_c = iter_dn_c;
			next_step_c  -= step_dn_c; // next step is smaller
			cycle_dn_c--;
		} else {
			next_step_c  -= step_up_c; // next step is smaller
			cycle_up_c--;
		}
	} else {
		next_step_c  -= step_dn_c; // next step is smaller
		cycle_dn_c--;
	}
	if ((--chirp_steps) == 0) {
		SetOutputHighz(current_sensor_channel);
		CHIRP_OFF();
		excite_done = TRUE;
		SetupMeasurment();
	} else {
		if (chirp_steps & 0x01) {
			SetOutputHigh(current_sensor_channel);
		} else {
			SetOutputLow(current_sensor_channel);
		}
	}
}