/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: calculation.h
 * Description: Header file for calculation.c
 */ 



#ifndef CALCULATION_H_
#define CALCULATION_H_

void CalcReadings(void);

#endif /* CALCULATION_H_ */