/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: calculation.c
 * Description: Perform calculations for data logger
 * Purpose: - This file defines how 16-bit data read from the logger sensor input channels
 *			or read from data memory is converted into calibrated floating point values.
			- The equations for this are obtained from the parameters stored in the setting
			pages in data memory
			- A state machine is used to track the progress through the calculations, the state variable
			is the channel to this file
			- External access to the calculation function call calc_start with the source data as a parameter
			and monitor the returned value to see when the call succeeded
			- External code waiting on completion of calculations calls calc_done until it returns true whereupon
			the results can be read from calc_result
 */ 

#include <avr/io.h>
#include <math.h>

#include "../../common/util.h"
#include "../Setting/setting.h"
#include "read.h"
#include "sample.h"
#include "calculation.h"

static float Poly(float x, unsigned char channel, float offset);
static float Steinhart(float x, unsigned char channel, float offset);
static float CheckedSettingsCoefficient(unsigned char channel, unsigned char i);

/*******************************************************************************
* Function Name		: poly
* Description		: To apply the polynomial calculation
* Input				: x: input data - frequency
					  channel: channel's number
					  offset
* Return			: The value after calculation
*******************************************************************************/
static float Poly(float raw_reading, uint8_t channel, float offset) {
	float real = 0.0;

	real += CheckedSettingsCoefficient(channel, 0);
	
	if (CheckedSettingsCoefficient(channel, 1) != 0) {
		real += CheckedSettingsCoefficient(channel, 1) * raw_reading;
	}
	if (CheckedSettingsCoefficient(channel, 2) != 0) {
		real += CheckedSettingsCoefficient(channel, 2) * raw_reading * raw_reading;
	}
	if (CheckedSettingsCoefficient(channel, 3) != 0) {
		real += CheckedSettingsCoefficient(channel, 3) * raw_reading * raw_reading * raw_reading;
	}
	if (CheckedSettingsCoefficient(channel, 4) != 0) {
		real += CheckedSettingsCoefficient(channel, 4) * raw_reading * raw_reading * raw_reading * raw_reading;
	}
	return real + offset;
}

/*******************************************************************************
* Function Name		: steinhh
* Description		: To apply the Steinhart-Hart
* Input				: x:       input data - resistance
					  channel: channel's number
					  offset:  offset of the channel
* Return			: The value of temperature 
*******************************************************************************/
static float Steinhart(float x, uint8_t channel, float offset) {
	float s, real;
	
	s = log(x);
	real = SettingGetCoefficient(channel,0);
	real += SettingGetCoefficient(channel,1) * s;
	real += SettingGetCoefficient(channel,3) * s * s * s;
	return ((1.0 / real) - 273.15) + offset;

}

/*******************************************************************************
* Function Name		: checked_settings_coefficient
* Description		: Check and return correct coefficient
* Input				: channel and index
* Return			: coefficient
*******************************************************************************/
static float CheckedSettingsCoefficient(uint8_t channel, uint8_t i) {
	if ((i == 0) && SettingGetCompensation(channel)) {
		return 0;
	} else {
		return SettingGetCoefficient(channel, i);
	}
}

/*******************************************************************************
* Function Name		: CalcReadings
* Description		: Function to get the real readings from the raw readings going through the calculation
* Input				: void
* Return			: void
*******************************************************************************/
void CalcReadings(void) {
	float offset;
	float* raw_reading = GetRawReading();
	
	for (uint8_t i = 0; i < NUM_CHANNELS; i++) {
		if (SettingGetCompensation(i) == 0) {
			offset = 0;
			if (SettingGetSensorType(i) != ST_UNUSED) {
				switch (SettingGetSensorEquation(i)) {
					case NO_EQN:
						SetRealReading(i, raw_reading[i]);
						break;
					case POLYNOMIAL_EQN:
						SetRealReading(i, Poly(raw_reading[i], i, offset));
						break;
					case STEINHARTHART_EQN:
						SetRealReading(i, Steinhart(raw_reading[i], i, offset));
						break;
				}
			} else {
				SetRealReading(i, 0);
			}
		}
	}
	
	for (uint8_t i = 0; i < NUM_CHANNELS; i++) {
		if (SettingGetCompensation(i) != 0) {
			offset = GetRealReading()[SettingGetCompensated(i)];
			if (SettingGetSensorType(i) != ST_UNUSED) {
				switch (SettingGetSensorEquation(i)) {
					case NO_EQN:
						SetRealReading(i, raw_reading[i]);
						break;
					case POLYNOMIAL_EQN:
						SetRealReading(i, Poly(raw_reading[i], i, offset));
						break;
					case STEINHARTHART_EQN:
						SetRealReading(i, Steinhart(raw_reading[i], i, offset));
						break;
				}
			} else {
				SetRealReading(i, 0);
			}
		}
	}
}