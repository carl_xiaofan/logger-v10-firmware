/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: sample.h
 * Description: Header file of sample.c
 */ 


#ifndef SAMPLE_H_
#define SAMPLE_H_

#define SWITCHED_5V_PIN_ON()         (PORTA  |=  (_BV(PA1)))
#define SWITCHED_5V_PIN_OFF()        (PORTA  &= ~(_BV(PA1)))

void SampleInit(void);

void sample_run(void);

void SampleSecondTick(void);

void SampleWalk(void);

void SampleReadEnable();

#endif /* SAMPLE_H_ */