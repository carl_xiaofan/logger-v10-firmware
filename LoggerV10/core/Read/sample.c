/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: sample.c
 * Description: Sensor reading control
 */ 

#include <avr/io.h>
#include <stdlib.h>
#include <util/delay.h>
#include "../../common/util.h"
#include "../../common/state.h"
#include "../Serial/serial_packet.h"
#include "../Serial/packetHandler.h"
#include "../Serial/usbuart.h"
#include "../Serial/rs485uart.h"
#include "../Setting/setting.h"
#include "../Display/menu.h"
#include "../Log/log.h"
#include "../RTC/DS3232.h"
#include "../Ram/sram.h"
#include "../../main.h"
#include "sample.h"
#include "read.h"
#include "calculation.h"

static uint8_t sample_state;
static uint8_t read_enable;
static uint8_t current_reading;
static uint16_t sample_interval;
static uint8_t current_channel;

static float last_record[NUM_CHANNELS];

static uint8_t NeedRead(void);
static void SampleIdle(void);
static void SampleRead(void);
static void SampleDone(void);
static void StoreSampleSram(float* data);
static uint8_t CompareValueChange(void);

void SampleInit(void) {
	sample_state = IDLE;
	read_enable = FALSE;
	if (SettingGetEnableFastRead()) {
		read_enable = TRUE;
	}
	sample_interval = SettingGetMeasureInterval();
	current_channel = 0;
}

void SampleSecondTick(void) {
	if (SettingGetEnableRecord()) {
		if (!SettingGetEnableFastRead()) {
			
			// Only master use count down, slave wait for order
			if (SettingGetRole() == ROLE_MASTER || 
			(SettingGetRole() == ROLE_SLAVE && SettingGetEnableSynchronize() == 0)) {
				if (sample_interval) {
					if ((--sample_interval) == 0) {
						read_enable = TRUE;
					}
				}
			}
		}
	}
}

void SampleWalk(void) {
	switch (sample_state) {
		case IDLE:
			SampleIdle();
			break;
		case READ:
			SampleRead();
			break;
		case DONE:
			SampleDone();
			break;
			
	}
}

/*******************************************************************************
* Function Name		: sample_read_enable
* Description		: Used for slave to enable read
* Input				: void
* Return			: void
*******************************************************************************/
void SampleReadEnable() {
	read_enable = TRUE;
}

/*******************************************************************************
* Function Name		: need_read
* Description		: Check if need to read sensor
* Input				: void
* Return			: 1 if need
*					  0 if not
*******************************************************************************/
static uint8_t NeedRead(void) {
	for (uint8_t i = 0; i < NUM_CHANNELS; ++i) {
		if (SettingGetSensorType(i) != ST_UNUSED) {
			current_reading = i;
			return 1;
		}
	}
	return 0;
}

/*******************************************************************************
* Function Name		: sample_idle
* Description		: Idle state, wait for need read
* Input				: void
* Return			: void
*******************************************************************************/
static void SampleIdle(void) {
	if (read_enable) {
		if (NeedRead()) {
			sample_state = READ;	
			SWITCHED_5V_PIN_ON();
			sample_interval = SettingGetMeasureInterval();
		}
	}
}

/*******************************************************************************
* Function Name		: SampleRead
* Description		: Read state, read 8 channel and enter DONE
* Input				: void
* Return			: void
*******************************************************************************/
static void SampleRead(void) {
	if (GetReadStatus() == IDLE) {
		if (SettingGetSensorType(current_reading) != ST_UNUSED) {
			ReadSensor(current_reading);
			current_channel |= (1 << current_reading);
		}
		
		if (current_reading != NUM_CHANNELS - 1) {
			current_reading++;
		} else {
			sample_state = DONE;
		}
	}
}

/*******************************************************************************
* Function Name		: SampleDone
* Description		: Done state, finalize the read including sending RTM and LOG
* Input				: void
* Return			: void
*******************************************************************************/
static void SampleDone(void) {
	CalcReadings();
	
	if (SettingGetEnableFastRead()) {
		if (SettingGetRTM()) {
			for (uint8_t i = 0; i < NUM_CHANNELS; ++i) {
				if (SettingGetSensorType(i) != 0) {
					PacketSendRTMData(i, GetRealReading()[i]);
				}
			}
		}
		read_enable = TRUE;
	} else {
		UploadRead();
		if (SettingGetEnableSynchronize()) {
			PacketSendFOR();
		}
		
		if (CompareValueChange()) {
			if (SettingGetRTM()) {
				if (SettingGetSerialMode() == MODE_RS485) {
					
					RS485UartTXStart();
					for (uint8_t i = 0; i < NUM_CHANNELS; ++i) {
						if (SettingGetSensorType(i) != 0) {
							PacketSendRTMData(i, GetRealReading()[i]);
						}
					}
					RS485UartTXStop();
				} else {
					for (uint8_t i = 0; i < NUM_CHANNELS; ++i) {
						if (SettingGetSensorType(i) != 0) {
							PacketSendRTMData(i, GetRealReading()[i]);
						}
					}
				}
			}
			StoreSampleSram(GetRawReading());
			
			LogRecord();
			
			for (uint8_t i = 0; i < NUM_CHANNELS; i++) {
				last_record[i] = GetRealReading()[i];
			}
		}
		read_enable = FALSE;
	}
	
	ResetRawReading();
	current_channel = 0;
	sample_state = IDLE;
	SWITCHED_5V_PIN_OFF();
}

/*******************************************************************************
* Function Name		: StoreSampleSram
* Description		: Store data into sram, for log purpose
* Input				: void
* Return			: void
*******************************************************************************/
static void StoreSampleSram(float* data) {
	DateTime_t now = RTC_Get();
	
	SramSequentialWriteStart(RECORD_BUFFER);
	SramSequentialWriteByte(U16ToU8(now.Year)[0]);
	SramSequentialWriteByte(U16ToU8(now.Year)[1]);
	SramSequentialWriteByte(now.Month);
	SramSequentialWriteByte(now.Date);
	SramSequentialWriteByte(now.Hour);
	SramSequentialWriteByte(now.Minute);
	SramSequentialWriteByte(now.Second);
	SramSequentialWriteByte(current_channel);
	for (uint8_t i = 0; i < NUM_CHANNELS; ++i)
	{
		uint8_t* f = FloatToBytes(data[i]);
		SramSequentialWriteByte(f[0]);
		SramSequentialWriteByte(f[1]);
		SramSequentialWriteByte(f[2]);
		SramSequentialWriteByte(f[3]);
	}
	SramSequentialEnd();
}

/*******************************************************************************
* Function Name		: compare_value_change
* Description		: Compare record change
* Input				: void
* Return			: return 1 if need to record, 0 otherwise
*******************************************************************************/
static uint8_t CompareValueChange(void) {
	for (uint8_t i = 0; i < NUM_CHANNELS; i ++) {
		if (SettingGetSensorType(i) != 0) {
			if ((GetRealReading()[i] - last_record[i]) >= 0) {
				if ((GetRealReading()[i] - last_record[i]) >= SettingGetSampleChange(i)) {
					return 1;
				}
			} else if ((GetRealReading()[i] - last_record[i]) < 0) {
				if ((last_record[i] - GetRealReading()[i]) >= SettingGetSampleChange(i)) {
					return 1;
				}
			}
		}
	}
	
	return 0;
}