/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: read.h
 * Description: Header file for read.c
 */ 

#ifndef READ_H_
#define READ_H_

// External Analog Voltage V91
#define AUX_ON()                     PORTA  |=  (_BV(PA1))
#define AUX_OFF()                    PORTA  &= ~(_BV(PA1))

// Analog Input and Mux V91
#define MUX_ON()                     PORTF  |=  (_BV(PF2))
#define MUX_OFF()                    PORTF  &= ~(_BV(PF2))
#define MUX_A_HI()                   PORTF  |=  (_BV(PF1))
#define MUX_A_LO()                   PORTF  &= ~(_BV(PF1))
#define MUX_B_HI()                   PORTE  |=  (_BV(PE2))
#define MUX_B_LO()                   PORTE  &= ~(_BV(PE2))
#define MUX_C_HI()                   PORTE  |=  (_BV(PE3))
#define MUX_C_LO()                   PORTE  &= ~(_BV(PE3))

#define RESISTOR_ON()                PORTH  |=  (_BV(PH2))
#define RESISTOR_OFF()               PORTH  &= ~(_BV(PH2))
#define VWLOAD_ON()                  PORTG  |=  (_BV(PG5))
#define VWLOAD_OFF()                 PORTG  &= ~(_BV(PG5))

#define IOPS1_HI()                   PORTE &= ~(1<<PE6)
#define IOPS1_LO()                   PORTE |=  (1<<PE6)
#define IOPE1_OFF()                  PORTE |=  (1<<PE7)
#define IOPE1_ON()                   PORTE &= ~(1<<PE7)

#define IOPS2_HI()                   PORTE &= ~(1<<PE4)
#define IOPS2_LO()                   PORTE |=  (1<<PE4)
#define IOPE2_OFF()                  PORTE |=  (1<<PE5)
#define IOPE2_ON()                   PORTE &= ~(1<<PE5)

#define IOPS3_HI()                   PORTK &= ~(1<<PK5)
#define IOPS3_LO()                   PORTK |=  (1<<PK5)
#define IOPE3_OFF()                  PORTK |=  (1<<PK4)
#define IOPE3_ON()                   PORTK &= ~(1<<PK4)

#define IOPS4_HI()                   PORTK &= ~(1<<PK7)
#define IOPS4_LO()                   PORTK |=  (1<<PK7)
#define IOPE4_OFF()                  PORTK |=  (1<<PK6)
#define IOPE4_ON()                   PORTK &= ~(1<<PK6)

// test lines
#define TEST1_HI()                   PORTB  |=  (_BV(PB0))
#define TEST1_LO()                   PORTB  &= ~(_BV(PB0))
#define TEST2_HI()                   PORTF  |=  (_BV(PF3))
#define TEST2_LO()                   PORTF  &= ~(_BV(PF3))

// Frequency Capture V91
#define MEASURE_CONTROL_B()          TCCR1B |= ((1<<ICNC1) | (1<<ICES1))
#define MEASURE_ON()                 TIMSK1 |=  (_BV(ICIE1))
#define MEASURE_OFF()                TIMSK1 &= ~(_BV(ICIE1))
#define MEASURE_INT_CLR()            TIFR1  |=   ICF1
#define MEASURE_ISR                  ISR(TIMER1_CAPT_vect)

#define ADS8320_CS_HI()              PORTH  |=  (_BV(PH3))
#define ADS8320_CS_LO()              PORTH  &= ~(_BV(PH3))
#define ADS8320_CLK_HI()             PORTD  |=  (_BV(PD5))
#define ADS8320_CLK_LO()             PORTD  &= ~(_BV(PD5))
#define ADS8320_DOUT()               PIND   &   (_BV(PD6))

// Vibrating Wire Chirp V91
#define CHIRP_OCR()                  OCR1A
#define CHIRP_ON()                   TIMSK1 |=  (_BV(OCIE1A))
#define CHIRP_OFF()                  TIMSK1 &= ~(_BV(OCIE1A))
#define CHIRP_INT_CLR()              TIFR1  |=   OCF1A
#define CHIRP_ISR                    ISR(TIMER1_COMPA_vect)

#define APOWER_ON()                  PORTA  |=  (_BV(PA2))
#define APOWER_OFF()                 PORTA  &= ~(_BV(PA2))

#define INPS_MASK                    0x0F
#define INPS_PINS()                  PINK
#define INPS_DIDR()                  DIDR2

#define OUT1_CHAN                    1
#define OUT2_CHAN                    3
#define OUT3_CHAN                    5
#define OUT4_CHAN                    7

#define INP1_BIT                     1 << PK2
#define INP2_BIT                     1 << PK3
#define INP3_BIT                     1 << PK1
#define INP4_BIT                     1 << PK0
  
#define INP1_CHAN                    2
#define INP2_CHAN                    4
#define INP3_CHAN                    6
#define INP4_CHAN                    8

#define MUX_CHANNEL_0                6
#define MUX_CHANNEL_1                2
#define MUX_CHANNEL_2                7
#define MUX_CHANNEL_3                3
#define MUX_CHANNEL_4                5
#define MUX_CHANNEL_5                1
#define MUX_CHANNEL_6                4
#define MUX_CHANNEL_7                0

#define ST_UNUSED                    0
#define ST_VIBRATING_WIRE            1
#define ST_RESISTANCE                2
#define ST_VOLTAGE                   3
#define ST_FREQUENCY                 4
#define ST_COUNTER                   5
#define ST_OUTPUT                    6
#define ST_EXTERNAL                  7
#define ST_INPUT                     8
#define ST_CALCULATION               9
#define ST_ALARMOUTPUT               10
#define ST_UNKNOWNSENSOR             11

#define NO_EQN						 0
#define POLYNOMIAL_EQN               1
#define STEINHARTHART_EQN            2
#define PROCESSA_EQN                 3
#define TIMERA_EQN                   4
#define RATIO_EQN                    5
#define BUTTON_EQN                   6
#define TIMERB_EQN                   7
#define ALARM_EQN                    8
#define SUM_EQN                      9
#define DIFFERENCE_EQN               10
#define BATTERY_VOLTAGE_EQN          11

#define NUM_CHANNELS                 8             // number of sensor channels
#define SWITCH_RESISTANCE			 57 // sw 7, 3 fuse and 47 for infallible resistor
#define ADC_AVERAGES				 256
// output driver states
#define LOW							1
#define HIGH						0
#define HIGHZ						3

void ReadInit(void);

void ReadWalk(void);

void ReadRun(void);

void ReadSensor(uint8_t sensor_channel);

float* GetRawReading(void);

void ResetRawReading(void);

void SetRealReading(uint8_t channel, float reading);

float* GetRealReading(void);

uint8_t GetReadStatus(void);

#endif /* READ_H_ */