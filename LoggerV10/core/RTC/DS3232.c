/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: DS3232.c
 * Description: RTC chips ds3232
 */ 

#include <stdio.h>
#include <stdlib.h>
#include "../../common/util.h"
#include "DS3232.h"
#include "TWI.h"

//----- Auxiliary data ------//
#define __I2C_SLA_W(Address)	(Address<<1)
#define __I2C_SLA_R(Address)	((Address<<1) | (1<<0))
#define __RTC_tmToY2k(Year)		(Year + 2000)
#define __RTC_y2kToTm(Year)		(Year - 2000)

uint8_t __RTC_Buffer[__RTC_Buffer_Length];
#if (RTC_Error_Checking != 0)
	enum RTC_Status_t __RTC_Status;
#endif

//---------------------------//

//----- Prototypes ----------------------------//
static uint8_t Dec2bcd(const uint8_t Value);
static uint8_t Bcd2dec(const uint8_t Value);
//---------------------------------------------//

//----- Functions -------------//
//Setup RTC. 
void RTC_Setup(void)
{
	__I2C_Setup();
	#if (RTC_Error_Checking != 0)
		__RTC_Status = RTC_OK;
	#endif

}

//Get RTC status. 
#if (RTC_Error_Checking != 0)
	enum RTC_Status_t RTC_Status(void)
	{
		return (__RTC_Status);
	}
#endif

//Write byte to RTC. 
void RTC_WriteByte(const uint8_t Address, uint8_t Data)
{
	RTC_WriteBlock(Address, &Data, 1);
}

//Read byte from RTC. 
uint8_t RTC_ReadByte(const uint8_t Address)
{
	uint8_t data;

	RTC_ReadBlock(Address, &data, 1);

	return data;
}

//Write data block to RTC.
void RTC_WriteBlock(const uint8_t StartAddress, uint8_t *Data, const uint8_t Length)
{	
	uint8_t i;
	#if (RTC_Error_Checking != 0)
		uint8_t status;
	#endif

	#if (RTC_Error_Checking != 0)
		do
		{
	#endif
			//Transmit START signal
			__I2C_Start();
			#if (RTC_Error_Checking != 0)
				status = __I2C_Status();
				if ((status != MT_START_TRANSMITTED) && (status != MT_REP_START_TRANSMITTED))
				{
					__RTC_Status = RTC_ERROR;
					break;
				}
			#endif

			//Transmit SLA+W
			__I2C_Transmit(__I2C_SLA_W(__RTC_I2C_Address));
			#if (RTC_Error_Checking != 0)
				status = __I2C_Status();
				if ((status != MT_SLA_W_TRANSMITTED_ACK) && (status != MT_SLA_W_TRANSMITTED_NACK))
				{
					__RTC_Status = RTC_ERROR;
					break;
				}
			#endif

			//Transmit write address
			__I2C_Transmit(StartAddress);
			#if (RTC_Error_Checking != 0)
				status = __I2C_Status();
				if ((status != MT_DATA_TRANSMITTED_ACK) && (status != MT_DATA_TRANSMITTED_NACK))
				{
					__RTC_Status = RTC_ERROR;
					break;
				}
			#endif

			//Transmit DATA
			for (i = 0 ; i < Length ; i++)
			{
				__I2C_Transmit(Data[i]);
				#if (RTC_Error_Checking != 0)
					status = __I2C_Status();
					if ((status != MT_DATA_TRANSMITTED_ACK) && (status != MT_DATA_TRANSMITTED_NACK))
					{
						__RTC_Status = RTC_ERROR;
						break;
					}
				#endif
			}
		
			#if (RTC_Error_Checking != 0)
				__RTC_Status = RTC_OK;
			#endif

	#if (RTC_Error_Checking != 0)
		}
		while (0);
	#endif

	//Transmit STOP signal
	__I2C_Stop();
}

//Read data block from RTC. 
void RTC_ReadBlock(const uint8_t StartAddress, uint8_t *Data, const uint8_t Length)
{
	uint8_t i;
	#if (RTC_Error_Checking != 0)
		uint8_t status;
	#endif

	#if (RTC_Error_Checking != 0)
		do
		{
	#endif
			//Transmit START signal
			__I2C_Start();
			#if (RTC_Error_Checking != 0)
				status = __I2C_Status();
				if ((status != MR_START_TRANSMITTED) && (status != MR_REP_START_TRANSMITTED))
				{
					__RTC_Status = RTC_ERROR;
					break;
				}
			#endif
		
			//Transmit RTC address (write) (SLA+W)
			__I2C_Transmit(__I2C_SLA_W(__RTC_I2C_Address));
			#if (RTC_Error_Checking != 0)
				status = __I2C_Status();
				if ((status != MT_SLA_W_TRANSMITTED_ACK) && (status != MT_SLA_W_TRANSMITTED_NACK))
				{
					__RTC_Status = RTC_ERROR;
					break;
				}
			#endif

			//Transmit read address
			__I2C_Transmit(StartAddress);
			#if (RTC_Error_Checking != 0)
				status = __I2C_Status();
				if ((status != MT_DATA_TRANSMITTED_ACK) && (status != MT_DATA_TRANSMITTED_NACK))
				{
					__RTC_Status = RTC_ERROR;
					break;
				}
			#endif

			//Transmit START signal
			__I2C_Start();
			#if (RTC_Error_Checking != 0)
				status = __I2C_Status();
				if ((status != MR_START_TRANSMITTED) && (status != MR_REP_START_TRANSMITTED))
				{
					__RTC_Status = RTC_ERROR;
					break;
				}
			#endif
				
			//Transmit RTC address (read) (SLA+R)
			__I2C_Transmit(__I2C_SLA_R(__RTC_I2C_Address));
			#if (RTC_Error_Checking != 0)
				status = __I2C_Status();
				if ((status != MR_SLA_R_TRANSMITTED_ACK) && (status != MR_SLA_R_TRANSMITTED_NACK))
				{
					__RTC_Status = RTC_ERROR;
					break;
				}
			#endif

			//Receive DATA
			for (i = 0 ; i < (Length - 1) ; i++)
			{
				Data[i] = __I2C_ReceiveACK();
				#if (RTC_Error_Checking != 0)
					status = __I2C_Status();
					if ((status != MR_DATA_RECEIVED_ACK) && (status != MR_DATA_RECEIVED_NACK))
					{
						__RTC_Status = RTC_ERROR;
						break;
					}
				#endif
			}
			Data[i] = __I2C_ReceiveNACK();
			#if (RTC_Error_Checking != 0)
				status = __I2C_Status();
				if ((status != MR_DATA_RECEIVED_ACK) && (status != MR_DATA_RECEIVED_NACK))
				{
					__RTC_Status = RTC_ERROR;
					break;
				}
			#endif
				

			#if (RTC_Error_Checking != 0)
				__RTC_Status = RTC_OK;
			#endif

	#if (RTC_Error_Checking != 0)
		}
		while (0);
	#endif

	//Transmit STOP signal
	__I2C_Stop();
}

//Set datetime. 
void RTC_Set(DateTime_t Value)
{
	//Prepare transmission buffer
	__RTC_Buffer[0] = Dec2bcd(Value.Second);
	__RTC_Buffer[1] = Dec2bcd(Value.Minute);
	__RTC_Buffer[2] = Dec2bcd(Value.Hour);
	__RTC_Buffer[3] = Dec2bcd(Value.Day);
	__RTC_Buffer[4] = Dec2bcd(Value.Date);
	__RTC_Buffer[5] = Dec2bcd(Value.Month);
	__RTC_Buffer[6] = Dec2bcd(__RTC_y2kToTm(Value.Year));

	//Transmit buffer
	RTC_WriteBlock(__RTC_Address_Seconds, __RTC_Buffer, __RTC_Buffer_Length);
}

//Get datetime. 
DateTime_t RTC_Get(void)
{
	DateTime_t value;

	//Read DATA into buffer
	RTC_ReadBlock(__RTC_Address_Seconds, __RTC_Buffer, __RTC_Buffer_Length);
		
	//Build time_t variable
	value.Second = Bcd2dec(__RTC_Buffer[0]);
	value.Minute = Bcd2dec(__RTC_Buffer[1]);
	value.Hour = Bcd2dec(__RTC_Buffer[2] & 0x3F);
	value.Day = Bcd2dec(__RTC_Buffer[3]);
	value.Date = Bcd2dec(__RTC_Buffer[4]);
	value.Month = Bcd2dec(__RTC_Buffer[5]);
	value.Year = __RTC_tmToY2k(Bcd2dec(__RTC_Buffer[6]));

	return value;
}

static uint8_t Dec2bcd(const uint8_t n)
{
	//Works for 0 -> 98
	return (n + ((n * 26)>>8) * 6);
}

static uint8_t Bcd2dec(const uint8_t n)
{
	//Works for 0 -> 98
	return (n - 6 * (n / 16));
}

/*******************************************************************************
* Function Name		: Get_Date
* Description		: Get Data from RTC
* Input				: datetime RTC
* Return			: Data as string
*******************************************************************************/
char* Get_Date(DateTime_t datetime)
{
	char* date_string;
	date_string = malloc(sizeof(char) * 15);
	sprintf(date_string,"%02d/%02d/%d", datetime.Date, datetime.Month, datetime.Year);
	
	return date_string;	// Remember to free
}

/*******************************************************************************
* Function Name		: Get_Time
* Description		: Get Time from RTC
* Input				: datetime RTC
* Return			: Time as string
*******************************************************************************/
char* Get_Time(DateTime_t datetime)
{
	char* date_string;
	date_string = malloc(sizeof(char) * 15);
	sprintf(date_string,"%02d:%02d:%02d", datetime.Hour, datetime.Minute, datetime.Second);
	
	return date_string;	// Remember to free
}

/*******************************************************************************
* Function Name		: datetime_to_bytes
* Description		: convert datetime to bytes
* Input				: offset: where datetime bytes start
* Return			: void
*******************************************************************************/
void datetime_to_bytes(DateTime_t datetime, uint8_t* str, int offset)
{
	str[offset] = datetime.Year & 0x00ff;
	str[offset + 1] = datetime.Year >> 8 & 0x00ff;
	str[offset + 2] = datetime.Month;
	str[offset + 3] = datetime.Date;
	str[offset + 4] = datetime.Hour;
	str[offset + 5] = datetime.Minute;
	str[offset + 6] = datetime.Second;
}

/*******************************************************************************
* Function Name		: bytes_to_datetime
* Description		: convert datetime to bytes
* Input				: 
* Return			: 
*******************************************************************************/
DateTime_t bytes_to_datetime(uint8_t* str, int offset)
{
	DateTime_t t;
	t.Year = U8ToU16(str[offset], str[offset + 1]);
	t.Month = str[offset + 2];
	t.Date = str[offset + 3];
	t.Hour = str[offset + 4];
	t.Minute = str[offset + 5];
	t.Second = str[offset + 6];
	return t;
}


