/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: process.c
 * Description: The whole firmware is running under Finite State Machine (FSM)
 * This file is the main loop and controlling every section
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include "common/timer.h"
#include "core/RTC/DS3232.h"
#include "core/Serial/usbuart.h"
#include "core/Serial/rs232uart.h"
#include "core/Serial/rs485uart.h"
#include "core/Serial/radio.h"
#include "core/Serial/uart.h"
#include "core/Serial/serial_packet.h"
#include "core/Display/lcd.h"
#include "core/Display/menu.h"
#include "core/Log/log.h"
#include "core/Log/sdhc.h"
#include "core/Read/read.h"
#include "core/Read/sample.h"
#include "core/Power/battery.h"
#include "core/Ram/sram.h"
#include "core/Display/key.h"
#include "process.h"

static volatile unsigned char previous_p1state;
static volatile uint8_t tick_enable;

/*******************************************************************************
* Function Name		: process_init
* Description		: Initialize the whole process
* Input				: void
* Return			: void
*******************************************************************************/
void ProcessInit(void) {
	tick_enable = 0;
	previous_p1state = 0;
	
	TIMER1_PRESCALE();
	TIMER4_PRESCALE();

	TIMER_NEXT();
	TIMER_MASK_ON();
  
	RTU232_PRESCALE();
	RTU485_PRESCALE();
	
	UartInit();
	
	SramInit();
	
	LogInit();
	
	lcd_init();
	
	SerialInit();
	
	KeyInit();
	
	RTC_Setup();
	
	BatteryInit();
	
	MenuInit();
	
	SampleInit();
	
	ReadInit();
	
	RTC_INT_MSK_SET();

	KEYS_INT_MSK_CLR();
	INPS_INT_MSK_CLR();
	
	DMLINK_WAKE_INT_MSK_CLR();
	USBLINK_WAKE_INT_MSK_CLR();
	RS485LINK_WAKE_INT_MSK_CLR();
	RS232LINK_WAKE_INT_MSK_CLR();
	
	PCINT0_INT_CLR();
	PCINT0_INT_ON();  // always on ??
	
	PCINT1_INT_CLR();
	PCINT1_INT_ON();  // always on ??
	
	PCINT2_INT_CLR();
	PCINT2_INT_ON();  // always on ??

	tick_enable = 1;
}

/*******************************************************************************
* Function Name		: process_walk
* Description		: Main loop for the process
* Input				: void
* Return			: void
*******************************************************************************/
void ProcessWalk(void) {
	lcd_walk();
	
	BatteryWalk();
	
	MenuWalk();
	
	SerialWalk();

	SampleWalk();

	ReadWalk();

	LogWalk();

}

/*******************************************************************************
* Function Name		: process_run
* Description		: Run each time TIMER_ISR happens
* Input				: void
* Return			: void
*******************************************************************************/
void ProcessRun(void) {
	lcd_run();
	
	KeyRun();
	
	ReadRun();
	
	LogRun();
	
	SerialRun();
}

/*******************************************************************************
* Function Name		: process_second_tick
* Description		: Run each one second
* Input				: void
* Return			: void
*******************************************************************************/
void ProcessSecondTick(void)
{
	MenuSecondTick();
	
	SampleSecondTick();
	
	BatterySecondTick();
}

/*******************************************************************************
* Function Name		: set_tick_enable
* Description		: Set the enable/disable of tick. By disable the tick, the logger will stop donig anything
* Input				: void
* Return			: void
*******************************************************************************/
void SetTickEnable(uint8_t enable) {
	tick_enable = enable;
}

 //This interrupt runs every 0.5 second
ISR (PCINT1_vect) {
	unsigned char p1state;

	// In V90 we have RS485 on PCINT8, RF on PCINT9, Keys on PCINT11..14 and RTC_INT on PCINT15

	p1state = PINJ; // mostly - missing RS485 int - but does not matter here
	if (((p1state ^ previous_p1state) & RTC_INT & p1state) && (tick_enable))
	{
		ProcessSecondTick();
	}
	
	previous_p1state = p1state;

	KEYS_INT_MSK_CLR();              // during sleep
	INPS_INT_MSK_CLR();              // only on when input switches used and during sleep

	DMLINK_WAKE_INT_MSK_CLR();       // during sleep
	USBLINK_WAKE_INT_MSK_CLR();      // during sleep
	RS485LINK_WAKE_INT_MSK_CLR();    // during sleep
	RS232LINK_WAKE_INT_MSK_CLR();    // during sleep
}

TIMER_ISR {
	TEST1_HI();
	sei();
	TIMER_NEXT();
	if (tick_enable)
	{
		ProcessRun();
	}
	TEST1_LO();
}