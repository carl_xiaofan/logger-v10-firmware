/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: main.h
 * Description: Define global constant here
 */ 

#ifndef MAIN_H_
#define MAIN_H_

#include "common/state.h"

#define TRUE							1
#define FALSE							0

#define ROLE_MASTER						0
#define ROLE_SLAVE						1

#define MODE_USB						0
#define MODE_RS232						1
#define MODE_RS485						2
#define MODE_RADIO						3
#define MODE_DISABLE					4

#define NUM_CHANNELS					8

#define MAX_SYNCSLAVE					4

#endif /* MAIN_H_ */