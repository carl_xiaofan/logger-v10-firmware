/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: util.c
 * Description: Some utility function for firmware
 */ 

#include <avr/io.h>
#include "util.h"

// Can be improved by using extern RAM later
static union Float_Uint8 f_u8;
static union Float_Uint32 f_u32;
static union Uint8_Uint16 u8_u16;
static union Uint8_Uint32 u8_u32;
static union Uint16_Uint32 u16_u32;

/*******************************************************************************
* Function Name		: FloatToBytes
* Description		: Convert a float to uin8 pointer
* Input				: float
* Return			: uint8_t*
*******************************************************************************/
uint8_t* FloatToBytes(float f) {
	f_u8.f = f;
	return f_u8.b;
}

/*******************************************************************************
* Function Name		: BytesToFloat
* Description		: Convert 4 unsigned int8 to float
* Input				: first byte to fourth byte
* Return			: float
*******************************************************************************/
float BytesToFloat(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3) {
	f_u8.b[0] = b0;
	f_u8.b[1] = b1;
	f_u8.b[2] = b2;
	f_u8.b[3] = b3;
	return f_u8.f;
}

/*******************************************************************************
* Function Name		: FloatToU32
* Description		: Convert float to uin32
* Input				: float
* Return			: uint32_t
*******************************************************************************/
uint32_t FloatToU32(float f) {
	f_u32.f = f;
	return f_u32.b;
}

/*******************************************************************************
* Function Name		: U32ToFloat
* Description		: Convert uin32 to float
* Input				: uint32_t
* Return			: float
*******************************************************************************/
float U32ToFloat(uint32_t u32) {
	f_u32.b = u32;
	return f_u32.f;
}

/*******************************************************************************
* Function Name		: U16ToU8
* Description		: Convert u16 to uin8 pointer
* Input				: uint16_t
* Return			: uint8_t*
*******************************************************************************/
uint8_t* U16ToU8(uint16_t u16) {
	u8_u16.u16 = u16;
	return u8_u16.u8;
}

/*******************************************************************************
* Function Name		: U8ToU16
* Description		: Convert two uin8 to uint16
* Input				: first byte, second byte
* Return			: uint16_t
*******************************************************************************/
uint16_t U8ToU16(uint8_t b0, uint8_t b1) {
	u8_u16.u8[0] = b0;
	u8_u16.u8[1] = b1;
	return u8_u16.u16;
}

/*******************************************************************************
* Function Name		: u32_to_u8
* Description		: Convert u32 to 4 unsigned int8
* Input				: uint32_t
* Return			: uint8_t*
*******************************************************************************/
uint8_t* U32ToU8(uint32_t u32) {
	u8_u32.u32 = u32;
	return u8_u32.u8;
}

/*******************************************************************************
* Function Name		: u8_to_u32
* Description		: Convert 4 unsigned int8 to u32
* Input				: first byte to fourth byte
* Return			: uint32_t
*******************************************************************************/
uint32_t U8ToU32(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3) {
	u8_u32.u8[0] = b0;
	u8_u32.u8[1] = b1;
	u8_u32.u8[2] = b2;
	u8_u32.u8[3] = b3;
	return u8_u32.u32;
}

/*******************************************************************************
* Function Name		: u32_to_u16
* Description		: Convert u32 to 2 uint16
* Input				: uint32_t
* Return			: uint16_t*
*******************************************************************************/
uint16_t* U32ToU16(uint32_t u32) {
	u16_u32.u32 = u32;
	return u16_u32.u16;
}

/*******************************************************************************
* Function Name		: u16_to_u32
* Description		: Convert 2 u16 to uint32
* Input				: uint16_t*
* Return			: uint32_t
*******************************************************************************/
uint32_t U16ToU32(uint16_t b0, uint16_t b1) {
	u16_u32.u16[0] = b0;
	u16_u32.u16[1] = b1;
	return u16_u32.u32;
}