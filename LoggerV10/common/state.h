 /* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: state.h
 * Description: Declare states for state machine. States are used in FSM
 */ 

#ifndef STATE_H_
#define STATE_H_


#define INIT                         0
#define OFF                          1
#define ON                           2

#define IDLE                         3
#define WAKE                         4
#define BUSY                         5
#define SEND                         6
#define DONE                         7
#define STOP                         8
#define READ                         9
#define NEXT                         10
#define SAVE                         11
#define EXCITE                       12
#define DISCHARGE					 13
#define MEASURE                      14
#define SETTLE                       15
#define PAGE                         16
#define TIME                         17
#define SETUP                        18
#define FINISH                       19
#define RESET                        20
#define WAIT                         21
#define WAIT2						 22
#define UPDATE						 23
#define DISP						 24
#define CHAN						 25
#define READING						 26
#define CHECK						 27
#define FORWARD						 28

#endif /* STATE_H_ */