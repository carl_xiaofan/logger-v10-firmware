 /* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: timer.c
 * Description: Class for convert delay ms into tick int
 */ 

#include <avr/io.h>
#include "timer.h"

/*******************************************************************************
* Function Name		: milliseconds
* Description		: Convert delay int into real milliseconds (< 255)
* Input				: delay ms
* Return			: tick
*******************************************************************************/
unsigned char Milliseconds(unsigned char delay) {
	unsigned long sec;
	static unsigned char result;

	sec = ((unsigned long) delay * (unsigned long) TICKS_PER_SECOND) / 1000;
	if (sec > 255) {
		result = 255;
	} else {
		result = (unsigned char) sec;
	}
	
	return result;
}

/*******************************************************************************
* Function Name		: big_milliseconds
* Description		: Convert delay int into real milliseconds (< 65535)
* Input				: delay ms
* Return			: tick
*******************************************************************************/
unsigned int  BigMilliseconds(unsigned int delay) {
	unsigned long sec;
	static unsigned int  big_result;

	sec = ((unsigned long) delay * (unsigned long) TICKS_PER_SECOND) / 1000;
	if (sec > 65535) {
		big_result = 65535; 
	} else {
		big_result = (unsigned int) sec;
	}
	
	return big_result;
}