/* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: util.h
 * Description: Header file for util.c
 */ 


#ifndef UTIL_H_
#define UTIL_H_

union Float_Uint8 {
	float f;
	uint8_t b[4];
};

union Float_Uint32 {
	float f;
	uint32_t b;
};

union Uint8_Uint16 {
	uint8_t u8[2];
	uint16_t u16;
};

union Uint8_Uint32 {
	uint8_t u8[4];
	uint32_t u32;
};

union Uint16_Uint32 {
	uint16_t u16[2];
	uint32_t u32;
};

uint8_t* FloatToBytes(float f);

float BytesToFloat(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3);

uint32_t FloatToU32(float f);

float U32ToFloat(uint32_t u32);

uint8_t* U16ToU8(uint16_t u16);

uint16_t U8ToU16(uint8_t b0, uint8_t b1);

uint8_t* U32ToU8(uint32_t u32);

uint32_t U8ToU32(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3);

uint16_t* U32ToU16(uint32_t u32);

uint32_t U16ToU32(uint16_t b0, uint16_t b1);

#endif /* UTIL_H_ */