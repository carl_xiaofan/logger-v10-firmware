 /* Copyright (C) Sigra Pty Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Xiaofan Zhou <xiaofan@sigra.com.au>, May 2020
 * 
 * Sigra Logger Version 10 Firmware
 *
 * File name: timer.h
 * Description: Header file for timer.c
 */ 


#ifndef TIMER_H_
#define TIMER_H_

#define TICKS_PER_SECOND            800UL

unsigned char Milliseconds(unsigned char delay);

unsigned int  BigMilliseconds(unsigned int delay);

#endif /* TIMER_H_ */